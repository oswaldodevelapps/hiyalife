//
//  HLUserView.m
//  hiyalife
//
//  Created by Oswaldo on 04/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLUserView.h"
#import "UIImageView+AFNetworking.h"
 


@interface HLUserView()


@property (nonatomic,strong) UIImageView *avatarImageView;
@property (nonatomic,strong) UILabel *userNameLabel;
@end


@implementation HLUserView

-(UIImageView *)avatarImageView{
    if(!_avatarImageView){
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(1, 1, 19, 19)];
        _avatarImageView.clipsToBounds = YES;
        [_avatarImageView setContentMode:UIViewContentModeScaleAspectFill];
    }
    
    return _avatarImageView;
}

-(UILabel *)userNameLabel{
    if(!_userNameLabel){
     _userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(24, 2, 200, 18)];
        [_userNameLabel setFont:[UIFont fontWithName:@"Tauri" size:10.0]];
        _userNameLabel.textColor = [UIColor whiteColor];
        _userNameLabel.backgroundColor = [UIColor clearColor];
    }
    return _userNameLabel;
}
- (id)init
{
    self = [super initWithFrame:CGRectZero];
    if(self){
     [self setup];   
    }
    
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

-(id)initWithUser:(HLUser*)user {
    self = [super init];
    if(self){
        self.user = user;
        [self setup];
    }
    return self;
}
- (void)setup {
    
    [self setUserInteractionEnabled:YES];
    
    if (!self.user.photoURL) {
        [self.avatarImageView setImage:[UIImage imageNamed:@"testMail.png"]];
    }else{
        [self.avatarImageView setImageWithURL: [NSURL URLWithString: self.user.photoURL]];
    }
    
    [self.userNameLabel setText:self.user.name];
    
    [self addSubview:self.userNameLabel];
    [self addSubview:self.avatarImageView];
  
    CGSize labelSize = [self.user.name sizeWithFont:[UIFont fontWithName:@"Tauri" size:14.0]];
    
    CGRect totalFrame = CGRectMake(0, 0, labelSize.width + self.avatarImageView.bounds.size.width, 20);
    
    
    [self setFrame:totalFrame];
}
 

@end
