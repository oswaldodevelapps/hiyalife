//
//  HLMyLifeViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 17/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLMyLifeViewController.h"
#import "HLUser.h"
#import "HLDataStore.h"

@interface HLMyLifeViewController ()

@end

@implementation HLMyLifeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.currentUser = [HLDataStore sharedInstance].currentUser;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
