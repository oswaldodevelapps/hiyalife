//
//  HLPasswordIndicator.m
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "ORPasswordIndicatorView.h"

@implementation ORPasswordIndicatorView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}



- (void)setup {
    
 
    
    for(int i=0;i<SEGMENT_CNT;i++){
        UIImageView *segment = [[UIImageView alloc]initWithFrame:CGRectMake(i*SEGMENT_WIDTH + (i>0?i:0), 0, SEGMENT_WIDTH, SEGMENT_HEIGHT)];
        segment.tag = i;
        [segment setImage:[UIImage imageNamed:SEGMENT_IMAGE_OFF]];
        [self addSubview:segment];
    }
    
    //NSLog(@"Generating View Password indicator with size %f x %f",self.frame.size.width,self.frame.size.height);
}

@end
