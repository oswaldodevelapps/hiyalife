//
//  HLDataStore.m
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLDataStore.h"
#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"
#import "AFImageRequestOperation.h"

#import "HLMeemo.h"
#import "NSDate+HLDateFormatter.h"
#import "NSData+HLUIImage.h"
#import "HLChapter.h"
#import "HLNotification.h"
#import "HLOtherUser.h"
#import "HLPlace.h"
#import "NSDate+HLDateFormatter.h"
 

@interface HLDataStore()
{
    NSDictionary *loginResponseDictionary;
}


@property (nonatomic,strong) NSDateFormatter *dateFormat;

@property(nonatomic,strong)AFHTTPClient *authClient;
@property(nonatomic,strong)AFHTTPClient *resourceClient;

@end

@implementation HLDataStore
+ (HLDataStore *)sharedInstance
{
    static HLDataStore *_instance;
    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[HLDataStore alloc] init];
        }
        return _instance;
    }
}

-(AFHTTPClient *)authClient
{
    if(!_authClient){
        _authClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:AUTH_BASE_URL]];
        [_authClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [_authClient setDefaultHeader:@"Accept" value:@"application/json"];
    }
    return _authClient;
}
-(AFHTTPClient *)resourceClient
{
    if(!_resourceClient){
        _resourceClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:RES_BASE_URL]];
        [_resourceClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [_resourceClient setDefaultHeader:@"Accept" value:@"application/json"];
        
    }
    return _resourceClient;
}
-(NSMutableArray *)discoverFeedMeemos{
    if(!_discoverFeedMeemos){
        _discoverFeedMeemos = [[NSMutableArray alloc]init];
    }
    return _discoverFeedMeemos;
}

-(NSMutableArray *)myLifeFeedMeemos{
    if(!_myLifeFeedMeemos){
        _myLifeFeedMeemos  = [[NSMutableArray alloc]init];
    }
    return _myLifeFeedMeemos;
}

-(NSMutableArray *)anotherUserFeedMeemos
{
    if(!_anotherUserFeedMeemos){
        _anotherUserFeedMeemos = [[NSMutableArray alloc]init];
    }
    return _anotherUserFeedMeemos;
}

-(NSDateFormatter *)dateFormat{
    if(!_dateFormat){
        _dateFormat = [[NSDateFormatter alloc]init];
        [_dateFormat setDateFormat:K_FORMATER_DATE];
    }
    return _dateFormat;
}

-(HLMeUser *)currentUser{
    if(!_currentUser){
        NSMutableDictionary *userDict = [NSDictionary dictionaryWithContentsOfFile:[self userArchivePath]];
        if(userDict){
            NSLog(@"Restored user from disk with Dictionary %@",userDict);
            _currentUser = [HLMeUser makeUserWithDictionary:userDict];
        }
    }
    return _currentUser;
}



-(HLMeUser *)validatingUser{
    if(!_validatingUser){
        NSMutableDictionary *userDict = [NSDictionary dictionaryWithContentsOfFile:[self validatingUserArchivePath]];
        if(userDict){
            NSLog(@"Restore validating user from disk..");
            _validatingUser = [HLMeUser makeUserWithDictionary:userDict];
        }
    }
    
    return _validatingUser;
}

-(HLEditingMeemo *)editingMeemo{
    if(!_editingMeemo){
        NSMutableDictionary *memoDraftDict = [NSDictionary dictionaryWithContentsOfFile:[self draftMemoArchivePath]];
        if(memoDraftDict){
            _editingMeemo = [HLEditingMeemo editingMeemoWithDictionary: memoDraftDict];
        }
        else{
            _editingMeemo = [[HLEditingMeemo alloc]init];
             
        }
    }
    return _editingMeemo;
}

#pragma mark - API Auth

-(void)getLoginOkWithUser:(NSString *)user andPassword:(NSString *)pwd returnBlock:(loginReturnBlock) block{
     
 
 
    NSDictionary *parameters = @{@"grant_type":@"direct",
                                 @"client_id":API_CLIENT_ID,
                                 @"client_secret":API_CLIENT_SECRET,
                                 @"email":user,
                                 @"password":pwd};
  
    [self.authClient getPath:@"auth"
         parameters:parameters
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"HTTP LOGIN CODE %d",operation.response.statusCode);
                NSLog(@"LOGIN JSON  %@", responseObject);
                if(![responseObject valueForKey:@"error" ]){
                    
                    //set token datetime in dictionary
                    
                    NSMutableDictionary *mutableDict = [(NSDictionary *)responseObject mutableCopy];
                    [mutableDict setValue:[[NSDate alloc]init] forKey:@"access_token_datetime"];
                    self.currentUser = [HLMeUser makeUserWithDictionary:mutableDict];
     
                    [self saveChanges];
                                       if(block)
                                       block(self.currentUser,nil);
                                       
                    }else{
                        if(block)
                            block(nil,nil);
                    }
                                      
                }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"HTTP LOGIN CODE %d",operation.response.statusCode);
                if(block)
                    block(nil,error);
                
            }
     ];

    
    
}
-(void)getRefreshToken:(refreshTokenBlock) block{
   
    NSDictionary *parameters = @{@"grant_type":@"refresh_token",
                                 @"refresh_token":self.currentUser.refresh_token,
                                 @"client_id":API_CLIENT_ID,
                                 @"client_secret":API_CLIENT_SECRET
                                 };
  
    [self.authClient getPath:@"auth"
         parameters:parameters
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"REFRESH TOKEN JSON  %@", responseObject);
                
                if(block)
                    block([responseObject valueForKey:@"access_token"],[responseObject valueForKey:@"refresh_token"]);
                
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                if(block)
                    block(nil,nil);
                
            }
     ];
    
  
}


-(void)getLoginWithFacebookToken:(NSString *)token returnBlock:(loginReturnBlock)block{
    NSDictionary *parameters = @{@"fb_access_token":token};
 
 
    [self.authClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [self.authClient setDefaultHeader:@"Accept" value:@"application/json"];
     
    
    [self.authClient getPath:@"auth"
          parameters:parameters
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSLog(@"LOGIN JSON FB %@", responseObject);
               
                 if(![responseObject valueForKey:@"error"]){
                     self.currentUser = [HLMeUser makeUserWithDictionary:(NSDictionary *)responseObject];
                     [self saveChanges];
                     
                     if(block)
                         block(self.currentUser,nil);
                 }else{
                     if(block)
                         block(nil,nil);
                 }
                 
             
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 if(block)
                     block(nil,error);
                 
             }
     ];
    
 
}

-(void)joinUserWith:(NSString *)email andPassword:(NSString *)pwd returnBlock:(loginReturnBlock) block{
 
    NSDictionary *parameters = @{@"grant_type":@"direct",
                                 @"client_id":API_CLIENT_ID,
                                 @"client_secret":API_CLIENT_SECRET,
                                 @"email":email,
                                 @"password":pwd};
    
    [self.authClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [self.authClient setDefaultHeader:@"Accept" value:@"application/json"];
    
         
    [self.authClient postPath:@"auth"
          parameters:parameters
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSLog(@"JOIN JSON %@", responseObject);
                 if(![responseObject valueForKey:@"error" ]){
                     self.validatingUser = [HLMeUser makeUserWithDictionary:(NSDictionary *)responseObject];
                     self.validatingUser.password1 = pwd;
                     
                     
                     
                     [self saveChanges];
                     
                     if(block)
                         block(self.validatingUser,nil);
                 }
                 else{
                     NSError *error = nil;
                     if([[responseObject valueForKey:@"error"] isEqualToString:@"isauser"]){
                           error = [NSError errorWithDomain:@"join_error" code:101 userInfo:[NSDictionary dictionaryWithObject:@"User already exists" forKey:NSLocalizedDescriptionKey]]; 
                     }
                     
                     if(block)
                         block(nil,error);
                 }
             
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 if(block)
                     block(nil,error);
                 
             }
     ];


}

-(void)joinUserWithFacebookToken:(NSString *)token returnBlock:(loginReturnBlock)block{
 
    NSDictionary *parameters = @{@"fb_access_token":token
                                 };
 
    [self.authClient setParameterEncoding:AFFormURLParameterEncoding];
    
    
    [self.authClient postPath:@"auth"
          parameters:parameters
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSLog(@"JOIN FACEBOOK JSON %@", responseObject);
                 NSError *error = nil;
                 if([responseObject valueForKey:@"error"]){
                     error = [NSError errorWithDomain:@"join_error" code:101 userInfo:[NSDictionary dictionaryWithObject:@"Facebook Join Error" forKey:NSLocalizedDescriptionKey]];
                     if(block)
                         block(nil,error);
                     
                 }else{
                     
                     self.currentUser = [HLMeUser makeUserWithDictionary:(NSDictionary *)responseObject];
                     [self saveChanges];
                     if(block)
                         block(self.currentUser,error);
                 }
         
                 
         
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 if(block)
                     block(nil,error);
                 
             }
     ];
}

-(void)validateJoin:(NSURL *)url returnBlock:(loginReturnBlock)block{
    NSArray *parametersurl = [[url query] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"=&"]];
    NSMutableDictionary *keyValueParm = [[NSMutableDictionary alloc]init];
    for (int i = 0; i < [parametersurl count]; i=i+2) {
        [keyValueParm setObject:[parametersurl objectAtIndex:i+1] forKey:[parametersurl objectAtIndex:i]];
    }
    
    if([keyValueParm valueForKey:@"code"]){
        
        
        HLMeUser *user = self.validatingUser;
        
        
        NSDictionary *parameters = @{@"code":[keyValueParm valueForKey:@"code"],
                                     @"grant_type":@"activation",
                                     @"id":[keyValueParm valueForKey:@"id"]};
        
       
 
      
        [self.authClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",user.access_token]];
        
        
        [self.authClient putPath:@"auth"
              parameters:parameters
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                     NSLog(@"JOIN VALIDATEJSON %@", responseObject);
                     
                     if([[responseObject objectForKey:@"activation"] isEqualToString:@"success"]){
                         
                         //al validar, pasamos el usuario a actual
                         self.currentUser = self.validatingUser;
                         
                         if(block)
                             block(self.currentUser,nil);
                     }else{
                         if(block)
                         block(nil,nil);
                     }
                    
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     if(block)
                         block(nil,error);
                     
                 }
         ];
    }
    //TODO validate with the API
 
}
#pragma mark - API User
-(void)findUserListByKey:(NSString *)keySearch returnBlock:(friendReturnBlock)block{
    NSDictionary *parameters = keySearch ?@{@"key":keySearch} :@{};
   
    [self findUserLisWithDictionary:parameters returnBlock:block];
    
     
}
-(void)findUserListByFacebookID:(NSString *)fb_id returnBlock:(friendReturnBlock)block{
    NSDictionary *parameters = @{@"fb_id":fb_id};
    [self findUserLisWithDictionary:parameters returnBlock:block];
}

-(void)findUserLisWithDictionary:(NSDictionary *)dictionary returnBlock:(friendReturnBlock)block{
 
  
    
    [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
    
    [self.resourceClient getPath:@"user"
                      parameters:dictionary
                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                              NSLog(@"User List %@",responseObject);
                             if([responseObject valueForKey:@"users"]){
                                 NSMutableArray *arrayOfUsers = [[NSMutableArray alloc]init];
                                 id userList = [responseObject valueForKey:@"users"];
                                 for (id userDict in userList) {
                                     if([[userDict allKeys] count] >0){
                                         HLOtherUser *foundUser = [HLOtherUser makeUserWithDictionary:userDict];
                                         [arrayOfUsers addObject:foundUser];
                                     }
                                    
                                 }
                                if(block)block(arrayOfUsers);
                             
                             }
                             else{
                                if(block)block(nil);
                             }
                           
                         }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             NSLog(@"Error %@",error);
                             if(block)block(nil);
                         }
     ];
}


#pragma mark - API Share

-(void)getSharesListForMeemoID:(NSString *)memoID returnBlock:(shareListBlock) block {
 
     
        NSDictionary *parameters = @{@"meemo":  memoID};
  
        [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
        [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
        
        [self.resourceClient getPath:@"share"
             parameters:parameters
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    NSMutableArray *usersArrayResult = [[NSMutableArray alloc]init];
                    
                    if([responseObject valueForKey:@"shares"]){
                        NSDictionary *dictionary = [responseObject valueForKey:@"shares"];
                        
                        NSEnumerator *enumerator = [dictionary keyEnumerator];
                        id key;
                        while ((key = [enumerator nextObject])) {
                            NSDictionary *tmpUserDict = [dictionary objectForKey:key];
                            HLOtherUser *userObject = [HLOtherUser makeUserWithDictionary:tmpUserDict];
                            [usersArrayResult addObject:userObject];
                        }
               
                    }
                    
                    if(block)block(usersArrayResult);
                    
                    
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           
                    if(block)block(nil);
                    
                }
         ];
        }];
  

}
-(void)shareMeemoID:(NSString*)memoID andText:(NSString*)text withArrayUsers: (NSArray*)users withArrayFB:(NSArray*)FBUsers withArrayOfEmails:(NSArray*)emails successBlock:(successBlock)block
{
    
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
        
        NSDictionary *parameters =  @{@"meemo":memoID,
                                               @"email":emails  	,
                                               @"fb_id":FBUsers,
                                               @"user":users,
                                               @"text":text} ;
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSLog(@"JSON DATA: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        NSMutableURLRequest *request = [self.resourceClient multipartFormRequestWithMethod:@"POST" path:@"share" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFormData:[NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil] name:@"data"];
            
            
         
            
        }];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        
      
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            id response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            
            NSLog(@"Response %@",response);
            
            if(![response valueForKey:@"error"]){
             if(block)block(YES);
            }
            else{
                 if(block)block(NO);
            }
         
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Operation FAILED %@",error );
            if(block) block(NO);
        }];
   
        [operation start];
 
    }];
    
    
    
}

#pragma mark - API Notifications
-(void)getNotificationsWithBlock:(notificationsReturnBlock) block{
    if(self.currentUser){
 
        NSDictionary *parameters = @{};
 
        [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
        
        [self.resourceClient getPath:@"notification"
             parameters:parameters
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
                    //NSLog(@"%@",responseObject);
                    if( [responseObject isKindOfClass:[NSArray class]]){
                        
                        NSArray *notifications = responseObject;
                        NSMutableArray *notificationObject = [[NSMutableArray alloc]init];
                        
                        for(int i=0;i<[notifications count];i++){
                            [notificationObject addObject:[HLNotification makeNotificationWithDictionary:[notifications objectAtIndex:i]]];
                        }
                        if(block)
                            block(notificationObject);
                    }
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Error %@",error);
                    if(block)
                        block(nil);
                }
         ];
    }
}

-(void)setNotificationAsRead:(NSArray *)notificationsArray returnBlock:(successBlock)block{
 
    
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
 
    for(HLNotification *notif in notificationsArray){
        
        int notifId = notif.notificationID;
        NSDictionary *parameters = @{@"id": [NSNumber numberWithInt: notifId]};
        NSURLRequest *request =  [self.resourceClient requestWithMethod:@"GET" path:@"notification" parameters:parameters];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            // Print the response body in text
            NSLog(@"Response for %d ID : %@",notifId, [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
 
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
       
        }];
        
        
        [operationQueue addOperation:operation];
        
    }
 
    
    [operationQueue waitUntilAllOperationsAreFinished];
    
    if(block)block(YES);
    
   
}

 


#pragma mark - API Feed

-(void)addToTimeline:(HLMeemo *)memo returnBlock:(successBlock)block{
  
    NSDictionary *parameters = @{@"meemo":memo.idMeemo};
 
    
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
         [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",access_token]];
        
        [self.resourceClient postPath:@"feed"
                           parameters:parameters
                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  NSLog(@"ADD TO TL JSON %@", responseObject);
                                  
                                  if(block){
                                      if([[responseObject valueForKey:@"error"] respondsToSelector:@selector(boolValue)]){
                                          if([[responseObject valueForKey:@"error"] boolValue] == NO){
                                              memo.isInMyTimeLine = YES;
                                              
                                              block(YES);
                                          }
                                          else{
                                              block(NO);
                                          }
                                      }
                                 
                                      else{
                                          block(NO);
                                      }
                                  }
                                  
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                  NSLog(@"Error make it mine: %@",error);
                                  if(block)block(NO);
                              }
         ];
    }];

}
-(void)getFeedForUser:(HLUser *)user withBlock:(feedReturnBlock)block{
 
    NSDictionary *parameters;
    
    if(user){
      parameters = @{@"user":[NSNumber numberWithInt:user.hiyaUserID]};
    }
    else{
      parameters = @{};
    }
 
 
    NSMutableArray *returnArray = [self getFeedForUser:user];
 
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {

        //NSLog(@"User VALID and NEW Token %@",access_token);
        
        [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
        
        
        
        [self.resourceClient getPath:@"feed"
             parameters:parameters
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    if([responseObject valueForKey:@"meemos"]){
                        //NSLog(@"Feed With Parameters %@ =  %@",parameters,responseObject);
                        if([[responseObject valueForKey:@"meemos"] respondsToSelector:@selector(enumerateObjectsUsingBlock:)]){
                            
                            
                                [returnArray removeAllObjects];
                          
                            
                            [[responseObject valueForKey:@"meemos"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                HLMeemo * meemo = [HLMeemo makeMeemoWithDictionary:obj];
                                if(meemo){
                                    
                                    [returnArray addObject:meemo];
                                }
                                
                            }]; 
                            
                            
                            NSLog(@"%@",returnArray);
                            
                            
                            if(block)
                                block(returnArray,nil);
                        }
                        
                    }
                    else{
                        
                        NSError *error = [NSError errorWithDomain:@"feed_error" code:101 userInfo:[NSDictionary dictionaryWithObject:@"Error retrieving Meemos" forKey:NSLocalizedDescriptionKey]];
                        NSLog(@"ERROR getting feed: %@", error);
                        if(block)
                            block(nil,error);
                    }
                    
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"HTTP CODE %d",operation.response.statusCode);
                    
                    if(block)
                        block(nil,error);
                }
         ];
        
        
    }];
 
    
    
   
    
}



#pragma mark - API Meemo
-(void)getMeemoFromID:(NSString *)meemoID forUser:(HLUser *)user withBlock:(meemoDetailsBlock)block{
    
    NSDictionary *parameters;
    
    
    parameters = @{@"meemo": meemoID};
    
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
        
        
        
        [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
        
        
        
        [self.resourceClient getPath:@"meemo"
                          parameters:parameters
                             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                 id meemoResponse = [responseObject valueForKey:@"meemo"];
                                 if(meemoResponse){
                                     HLMeemo *newMeemo = [HLMeemo makeMeemoWithDictionary:meemoResponse];
                                     if(block) block(newMeemo);
                                 }
                                 
                                 
                             }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                 if(block)block(nil);
                             }
         ];
        
        
    }];
    
    
 
    
    
    
    
}

-(void)getMeemoDetails:(HLMeemo *)meemo forUser:(HLUser *)user withBlock:(meemoDetailsBlock)block {
 NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
  
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
        
        //get details operations
        [operationQueue addOperation:[self getMemoDetailsRequestOperationForMemo:meemo forUser:user]];
        
        //get share operation
        [operationQueue addOperation:[self getShareListRequestOperationForMemo:meemo]];
     
        
        [operationQueue waitUntilAllOperationsAreFinished];
 
        
        if(block)block(meemo);
               
        
    }];
 
}

-(AFHTTPRequestOperation *)getMemoDetailsRequestOperationForMemo:(HLMeemo *)meemo forUser:(HLUser *)user{
    NSDictionary *parameters = @{@"meemo":  meemo.idMeemo,
                                @"user":[NSNumber numberWithInt: user.hiyaUserID]};
    
    NSURLRequest *request =  [self.resourceClient requestWithMethod:@"GET" path:@"meemo" parameters:parameters];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *meemoResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        if(meemoResponse){
            if([[meemoResponse valueForKey:@"isadded"] class] != [NSNull class]){
                meemo.isInMyTimeLine = [[meemoResponse valueForKey:@"isadded"] intValue];
            }else{
                meemo.isInMyTimeLine = NO;
            }
            
            if([[meemoResponse valueForKey:@"title"] class]!= [NSNull class]){
                meemo.title = [meemoResponse valueForKey:@"title"];
            }
            
  
        }
        NSLog(@"Operation %@ finished",operation);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@",error);
    }];
    
    return operation;
}

-(AFHTTPRequestOperation *)getShareListRequestOperationForMemo:(HLMeemo *)memo{
    NSDictionary *parameters = @{@"meemo":  memo.idMeemo};
    NSURLRequest *request =  [self.resourceClient requestWithMethod:@"GET" path:@"share" parameters:parameters];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        if([responseDict valueForKey:@"shares"]){
            NSDictionary *dictionary = [responseDict valueForKey:@"shares"];
             NSMutableArray *usersArrayResult = [[NSMutableArray alloc]init];
            
            NSEnumerator *enumerator = [dictionary keyEnumerator];
            id key;
            while ((key = [enumerator nextObject])) {
                NSDictionary *tmpUserDict = [dictionary objectForKey:key];
                HLOtherUser *userObject = [HLOtherUser makeUserWithDictionary:tmpUserDict];
                [usersArrayResult addObject:userObject];
                
            }
            
            
            memo.shareList = usersArrayResult;
        }
        NSLog(@"Operation %@ finished",operation);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
    return operation;
     
}


-(void)publishMemo:(HLEditingMeemo *)newMeemo returnBlock:(meemoPublishBlock) block{
 
    NSMutableArray *mutableCaptions = [[NSMutableArray alloc]init];
    NSUInteger imageIndex = 0;
    for(NSString *captionPhoto in newMeemo.editingCaptions){
        [mutableCaptions addObject:@{@"caption":captionPhoto,@"name":[NSString stringWithFormat:@"photo%d.jpg",imageIndex]}];
        imageIndex++;
    }
    
    
    
    NSDictionary *parameters = @{@"title":newMeemo.title?newMeemo.title:@"",
                                 @"text":newMeemo.text?newMeemo.text:@"",
                                 @"photosinfo":mutableCaptions,
                                 @"date": @{@"ini":@{@"y":newMeemo.fromDateYear,@"m":[NSString stringWithFormat:@"%02d", [NSDate getIndexForMonthName:newMeemo.fromDateMonth]+1],@"d":newMeemo.fromDateDay},
                                            @"end":@{@"y":newMeemo.toDateYear,@"m":[NSString stringWithFormat:@"%02d", [NSDate getIndexForMonthName:newMeemo.toDateMonth]+1],@"d":newMeemo.toDateDay}}
                                 }  ;
 
    [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
    
    NSMutableURLRequest *request = [self.resourceClient multipartFormRequestWithMethod:@"POST" path:@"meemo" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
 
        id json = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
        NSLog(@"JSON SENT: %@",[[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
      
        
         [formData appendPartWithFormData:json name:@"data"];
        
        
        
        for(int i=0;i<[newMeemo.editingPhotos count];i++){
            UIImage *image = [newMeemo.editingPhotos objectAtIndex:i];
            NSData * dataImage  = UIImageJPEGRepresentation(image,0.7f);
             [formData appendPartWithFileData:dataImage name:@"photos" fileName:[NSString stringWithFormat:@"photo%d.jpg",i] mimeType:@"image/jpg"];
        }
  
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
 
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *responseDict  =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"Operation Finished %@ ",responseDict );
        
        
        
        if(block){
            if([responseDict valueForKey:@"idmeemo"]){
                block([responseDict valueForKey:@"idmeemo"]);
            }
            else{
             block(nil);   
            }
             
        }
           
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Operation FAILED " );
        if(block)
            block(nil);
    }];
    
    
    
    
    [operation start];

    
}


-(void)editMemo:(HLMeemo*)editedMeemo onlyForParameters:(NSDictionary *)changedParamsDict returnBlock:(successBlock)block{
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
        
        
        
        NSDictionary *parameters =  @{@"meemo":editedMeemo.idMeemo,
                                       } ;
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSLog(@"JSON DATA: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        NSMutableURLRequest *request = [self.resourceClient multipartFormRequestWithMethod:@"PUT" path:@"meemo" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFormData:[NSJSONSerialization dataWithJSONObject:changedParamsDict options:NSJSONWritingPrettyPrinted error:nil] name:@"data"];
            
            
        }];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        
        
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            id response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            
            NSLog(@"Response %@",response);
            
            if(block){
                
                if([response valueForKey:@"error"])
                    block(NO);
                else
                    block(YES);
            }
       
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Operation FAILED %@",error );
            if(block) block(NO);
        }];
        
        [operation start];
    }];
}

-(void)editMemo:(HLMeemo *)editedMeemo returnBlock:(meemoDetailsBlock)block{
    
}

#pragma mark - API Chapters
-(void)getChaptersforUser:(HLUser *)user returnBlock:(chapterReturnBlock) block{
 
    NSDictionary *parameters;
 
    parameters = @{ };
 
   
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
        
        
        
        [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
        
        
        
        [self.resourceClient getPath:@"chapter"
             parameters:parameters
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    NSLog(@"Chapters details %@",responseObject);
                    if([responseObject valueForKey:@"chapters"]){
                        NSArray *chapters = [responseObject valueForKey:@"chapters"];
                        NSMutableArray *chaptersObject = [[NSMutableArray alloc]init];
                       
                        for(int i=0;i<[chapters count];i++){
                            [chaptersObject addObject:[HLChapter makeChapterWithDictionary:[chapters objectAtIndex:i]]];
                        }
                        if(block)
                            block(chaptersObject);
         
                    }
                    else{
                        if(block)
                            block(nil);
                    }
                    
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    if(block)
                        block(nil);
                }
         ];
        
        
    }];
}



#pragma mark - API ME
-(void)updateMeProfileDataForUser:(HLMeUser *)user returnBlock:(loginReturnBlock) block{
 
 
    NSDictionary *parameters = @{};
 
    
  
    [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",user.access_token]];
    
    [self.resourceClient getPath:@"me"
         parameters:parameters
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"ME JSON %@", responseObject);
   
                if(responseObject){
                    [self.currentUser updateUserInfoWith:(NSDictionary *)responseObject];
                    
                    [self getChaptersforUser:self.currentUser returnBlock:^(NSArray *arrayOfChapters) {
                    
                        
                        self.currentUser.userChapters = arrayOfChapters;
                        [self saveChanges];
                  
                        if(block)
                            block(self.currentUser,nil);
                    }];
                    
                
             
                }else{
                    if(block)
                        block(nil,nil);
                }
                
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error Get Profile: %@",error);
                if(block)
                    block(self.currentUser,error);
            }
     ];
    
  
    
    
}

-(void)setMeProfileDataForUser:(HLMeUser *)user succesBlock:(successBlock) block{
  
    NSAssert(user, @"SET PROFILE. USER CANNOT BE NIL");
 
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:@{@"name":user.name ? user.name : @"name",
                
                                           @"surname":user.surname ? user.surname : @"surname",
                                           @"birthday": [user.birthday dateString] ? [user.birthday dateString]: @"1950-01-01",
                            
                                            @"gender":user.gender ? ([user.gender isEqualToString:@"Male"] ? @"m":@"f"): @"m",
                                            
                                           }  ];
    if(user.first_login){
        [parameters setValue:user.password1 forKey:@"password"];
    }
    
      
    NSLog(@"User Parameters Sent %@",parameters);
    
    [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",user.access_token]];
   
    NSMutableURLRequest *request = [self.resourceClient multipartFormRequestWithMethod:@"PUT" path:@"me" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
     
        [formData appendPartWithFormData:[NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil] name:@"data"];
      
        
        if(user.editingPhoto){
 
            NSData * dataImage  = UIImagePNGRepresentation(user.editingPhoto);
            
            [formData appendPartWithFileData:dataImage name:@"photo" fileName:@"avatar.png" mimeType:@"image/png"];
        }
   
    }]; 
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request]; 
    
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Bytes written %d",bytesWritten);
    }];
    
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
 
            id response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            
            NSLog(@"Response %@",response);
            
            if(block){
                if(![response valueForKey:@"error"]){
                    
                    block(YES);
                }
                else{
                    block(NO);
                }
            }
       
                    
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Operation FAILED %@",error );
            if(block)
                block(NO);
        }];
    
    
    
    
    [operation start];
  

}

#pragma mark - API Places
-(void)getPlacesInBlock:(placesBlock) block{
    
    NSDictionary *parameters;
    
    parameters = @{ };
    
    
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
 
        [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
 
        [self.resourceClient getPath:@"place"
                          parameters:parameters
                             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                  //NSLog(@"Places Response %@",responseObject);
                                 
                                 if([responseObject valueForKey:@"places"]){
                                     NSMutableArray *placesList = [[NSMutableArray alloc]init];
                                     NSArray *places = [responseObject valueForKey:@"places"];
                                     for(NSDictionary * placeDict in places){
                                         [placesList addObject:[HLPlace placeWithDictionary:placeDict]];
                                     }
                                    
                                     if(block)block(placesList);
                                 }
                                 else{
                                    if(block)block(nil); 
                                 }
                                
                                 
                             }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                 if(block)block(nil);
                             }
         ];
        
        
    }];
}
-(void)createPlace:(HLPlace *)place successblock:(successBlock)block{
    NSDictionary *parameters;
    
  
    
    parameters = @{ @"caption":place.caption,
                    @"coordinates":@[[NSNumber numberWithDouble:place.longitude],[NSNumber numberWithDouble:place.latitude]],
                    @"id_ext":@121323,
                    @"source":@"iosmap"};
    
    
    [self.currentUser accessTokenRefreshed:^(NSString *access_token) {
        
        [self.resourceClient setDefaultHeader:@"Authentication" value:[NSString stringWithFormat:@"Bearer %@",self.currentUser.access_token]];
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSLog(@"JSON DATA: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        NSMutableURLRequest *request = [self.resourceClient multipartFormRequestWithMethod:@"POST" path:@"place" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFormData:[NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil] name:@"data"];
         
            
        }];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
   
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            id response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            
            NSLog(@"Response %@",response);
            
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Operation FAILED %@",error );
            
        }];
    
        
        [operation start];
        
        
    }];
}
#pragma mark - Validation

+(BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; //  return 0;
    return [emailTest evaluateWithObject:candidate];
}

#pragma mark - Utilities 
-(NSDate *)translateStringToDate:(NSString *)dateStr{
    
    NSDate *date = [self.dateFormat dateFromString:dateStr];
    return date;
}
-(NSMutableArray *)getFeedForUser:(HLUser *)user{
    if(!user){
        return self.discoverFeedMeemos;
    }
    else{
        if([user class] == [HLMeUser class]){
            return self.myLifeFeedMeemos;
        }
        else {
            return self.anotherUserFeedMeemos;
        }
    }
}
-(NSString *)translateDateToString:(NSDate *)date{
    return [self.dateFormat stringFromDate:date];
}

#pragma mark - Images
+(void)downloadImageWithURL:(NSString *)urlString success:(void (^)(UIImage *image))success{
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    AFImageRequestOperation *operation = [AFImageRequestOperation imageRequestOperationWithRequest:request success:^(UIImage *image) {
       
        if(success)
            success(image);
    }];
    
    [operation start];
}


#pragma mark - Archiving 



- (NSString *)meemosArchivePath
{
    NSArray *documentDirectories =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    
    // Get one and only document directory from that list
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    
    return [documentDirectory stringByAppendingPathComponent:@"meemos.json"];
}
- (NSString *)userArchivePath
{
    NSArray *documentDirectories =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    
    // Get one and only document directory from that list
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    
    return [documentDirectory stringByAppendingPathComponent:@"user.json"];
}
-(NSString *)draftMemoArchivePath{
    NSArray *documentDirectories =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    
    // Get one and only document directory from that list
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    
    return [documentDirectory stringByAppendingPathComponent:@"memoDraft.json"];
}
-(NSString *)draftPhotoArchivePathForIndex:(NSUInteger )index{
    NSArray *documentDirectories =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
   
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    
    return [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat: @"memoDraft_photo%d.png",index]];
}

-(NSString *)validatingUserArchivePath{
    NSArray *documentDirectories =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    
    // Get one and only document directory from that list
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    
    return [documentDirectory stringByAppendingPathComponent:@"validatinguser.json"];
}

-(BOOL)saveDraftMeemo{
    NSLog(@"Saving draft memo to disk...");
     
    
    NSString *draftMeemoPath = [self draftMemoArchivePath];
    return [self.editingMeemo.json writeToFile:draftMeemoPath atomically:YES];
    
}
-(NSArray *)saveDraftPhotos:(NSArray *)imageArray{
    NSUInteger imageIndex = 0;
    NSMutableArray *arrayOfPaths = [[NSMutableArray alloc]init];
    for(UIImage *image in imageArray){
        NSData *d = UIImagePNGRepresentation(image);
        NSString *photoPath = [self draftPhotoArchivePathForIndex:imageIndex];
        [d writeToFile:photoPath atomically:YES];
        [arrayOfPaths addObject:photoPath];
        imageIndex++;
    }
    return arrayOfPaths;
}
-(BOOL)deleteDraftMemo{
    
    self.editingMeemo = [[HLEditingMeemo alloc]init];
    
    NSString *draftMeemoPath = [self meemosArchivePath];
    NSFileManager *fm  = [[NSFileManager alloc]init];
    return [fm removeItemAtPath:draftMeemoPath error:nil];
  
}

- (BOOL)saveChanges
{
    
    
    //user
    NSString * userPath = [self userArchivePath];
#warning SAVE another user info like Chapters, Places
    BOOL success = [self.currentUser.json writeToFile:userPath atomically:YES];
    
    NSLog(@"Success: %d, Saving changes in disk: %@",success,self.currentUser.json);
    
    NSString *validatingUserPath = [self validatingUserArchivePath];
    
    [self.validatingUser.json writeToFile:validatingUserPath atomically:YES];
    
    
    
    
    return YES;
    
}

#pragma mark - Session
-(void)logout
{
    self.currentUser = nil;
    NSError *error;
    NSString *userPath = [self userArchivePath];
    
    [[NSFileManager defaultManager] removeItemAtPath:userPath error:&error];
    
}

@end
