//
//  UIImageView+FadeIn.m
//  Snaply
//
//  Created by Rafa Barberá Córdoba on 09/07/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import "UIImageView+FadeIn.h"
#import "UIImageView+AFNetworking.h"

@implementation UIImageView (FadeIn)

- (void)fadeInImage:(UIImage *)image
{
    if (image!=self.image) {
        UIImageView *overlay = [[UIImageView alloc] initWithImage:image];
        overlay.contentMode = self.contentMode;
        overlay.frame = self.frame;
        overlay.alpha = 0;
        [self.superview insertSubview:overlay aboveSubview:self];
        [UIView animateWithDuration:0.25f
                         animations:^{
                             overlay.alpha = 1;
                         }
                         completion:^(BOOL finished) {
                             self.image = image;
                             [overlay removeFromSuperview];
                         }];
    }
}

- (void)fadeInFromURL:(NSURL *)url
{
    [self fadeInFromURL:url placeholder:nil];
}

- (void)fadeInFromURL:(NSURL *)url placeholder:(UIImage *)placeholder
{
    __weak UIImageView *weakSelf = self;
    [self setImageWithURLRequest:[NSURLRequest requestWithURL:url]
                placeholderImage:placeholder
                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                             if (response) {
                                 [weakSelf fadeInImage:image];
                             } else {
                                 weakSelf.image = image;
                             }
                         }
                         failure:nil];
}

@end
