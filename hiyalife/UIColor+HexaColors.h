//
//  UIColor+HexaColors.h
//  hiyalife
//
//  Created by Juan Hontanilla on 23/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexaColors)
+ (UIColor *)colorWithHexString:(NSString *)str;
+ (UIColor *)colorWithHex:(UInt32)col;
@end
