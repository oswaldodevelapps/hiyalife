//
//  HLViewMeemoViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 04/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLViewMeemoViewController.h"
#import "HLDataStore.h"
#import "HLHeaderView.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+HLImage.h"
#import "NSAttributedString+HLMeemo.h"
#import "RBHorizontalImageScrollerView.h"
#import "HLMainPhotoAndThumbsWithScrollverView.h"
#import "NSString+HLHTMLContent.h"
#import "SVProgressHUD.h"
#import "HLSharedMeemoViewController.h"
#import "HLShowPhotosInFullScreenVCViewController.h"

@interface HLViewMeemoViewController ()<HLPhotoThumbsDelegate>{
    CGRect prevFrame;
}

@property (weak, nonatomic) IBOutlet HLHeaderView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (weak, nonatomic) IBOutlet UILabel *whereandWhen;
@property (weak, nonatomic) IBOutlet UIImageView *privacyView;
 

@property (weak, nonatomic) IBOutlet UITextView *extendedTitleTextview;
 
@property (weak, nonatomic) IBOutlet UIButton *editButton;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (weak, nonatomic) IBOutlet UIButton *makeItMineButton;
@property (weak, nonatomic) IBOutlet UIButton *addContentButton;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollContentView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet HLMainPhotoAndThumbsWithScrollverView *mainPhotoAndThumbs; 



@property (weak, nonatomic) IBOutlet UIView *headerOwnerView;

 
@property(nonatomic) BOOL isFullScreen;


@end

@implementation HLViewMeemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.isFullScreen = NO;
      self.navigationItem.titleView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    
    
    [self showButtonActions];
 
    
    [[HLDataStore sharedInstance] getSharesListForMeemoID:self.meemo.idMeemo returnBlock:^(NSArray *arrayOfOtherUser) {
        NSLog(@"Share Users %@",arrayOfOtherUser);
    }];
    
    
    self.webView.allowsInlineMediaPlayback = YES;
 
 
    self.headerView.label.text = self.meemo.title;
    //self.extendedTitleTextview.text = self.meemo.text;
    
    [self.webView loadHTMLString: [self.meemo.text embedStringInHTMLBody] baseURL:nil];
    
    
    if([self.meemo.owner valueForKey:@"photo"]){
        
        [self.avatarView setImageWithURL:[NSURL URLWithString:[self.meemo.owner valueForKey:@"photo"]] placeholderImage:[UIImage imageNamed:@"genuser"]];
    }
    self.ownerLabel.text = [self.meemo.owner valueForKey:@"name"];
    
    
    self.privacyView.image = [UIImage imageForPrivacy:self.meemo.privacy blackOrWhite:YES] ;
    
    self.whereandWhen.attributedText = [NSAttributedString dateAndWhereAttributedDescriptionWithMeemo:self.meemo];
  
    
    [self.mainPhotoAndThumbs setImagesThumnails:[self.meemo getAllPhotosNSURL]];
    
    self.mainPhotoAndThumbs.delegate = self;
 
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupContainerViews];
}
#define PHOTO_VIEW_HEIGHT 300
-(void)setupContainerViews{
    NSLog(@"Memo TExt %@",self.meemo.text);
    
    if(!self.meemo.text){
        //ONLY PHOTOS
  
        self.mainPhotoAndThumbs.frame = CGRectMake(0, CGRectGetMaxY(self.headerOwnerView.frame), self.scrollContentView.bounds.size.width, PHOTO_VIEW_HEIGHT);
        
        //self.webView.frame = CGRectZero;
    }
    else{
        if([self.meemo.photos count]>0){
                //PHOTOS AND TEXT
            self.mainPhotoAndThumbs.frame  = CGRectMake(0, CGRectGetMaxY(self.headerOwnerView.frame), self.scrollContentView.bounds.size.width, PHOTO_VIEW_HEIGHT);
            
            self.webView.frame =
            CGRectInset(CGRectMake(0, CGRectGetMaxY(self.mainPhotoAndThumbs.frame), self.scrollContentView.bounds.size.width, self.scrollContentView.frame.size.height - CGRectGetMaxY(self.mainPhotoAndThumbs.frame)), 6, 6);
            
            
        }else{
            //ONLY TEXT
            self.mainPhotoAndThumbs.frame = CGRectZero;
            self.webView.frame =  
            CGRectInset(CGRectMake(0, CGRectGetMaxY(self.headerOwnerView.frame), self.scrollContentView.bounds.size.width, self.scrollContentView.frame.size.height - CGRectGetMaxY(self.headerOwnerView.frame)), 6, 6);
            
        }
    }
    
}

-(void)showButtonActions{
    self.makeItMineButton.alpha = 0;self.editButton.alpha = 0; self.shareButton.alpha = 0
    
    ;self.addContentButton.alpha = 0;
    
    if(![self.meemo isInMyTimeLine]){
        
        [UIView animateWithDuration:0.5f animations:^{
          self.makeItMineButton.alpha = 1;
        }];
        
        
  
    }else{
        if([self.meemo isOwnedByMe]){
            [UIView animateWithDuration:0.5f animations:^{
                self.editButton.alpha = 1;
                self.shareButton.alpha = 1;
            }];
           
        }
        else{
            [UIView animateWithDuration:0.5f animations:^{
                self.addContentButton.alpha = 1;
            }];
         
        }
     
    }
}
 
-(void)dealloc
{
    NSLog(@"Deallocating %@",NSStringFromClass([self class]));
}


#pragma mark - Actions 

- (IBAction)makeItMineAction:(id)sender {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    
    
    [[HLDataStore sharedInstance] addToTimeline:self.meemo returnBlock:^(BOOL success) {
        if(success){
            [SVProgressHUD showSuccessWithStatus:@"The memo has been added!"];
            
            [self showButtonActions];
        }
        else{
            [SVProgressHUD showErrorWithStatus:@"Error. Not Allowed"];
        }
        
        
        
        
    }];
    
}

- (IBAction)editMemoAction:(id)sender {
}
- (IBAction)addContentAction:(id)sender {
    [SVProgressHUD showErrorWithStatus:@"Not implemented yet"];
    
    
}

- (IBAction)shareAction:(id)sender {
    
    [self performSegueWithIdentifier:@"showShareMemoFromViewMeemo" sender:self.meemo];
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showShareMemoFromViewMeemo"])
    {
       HLSharedMeemoViewController *vc =  segue.destinationViewController ;
        vc.meemo = self.meemo;
    }
    
    else if([segue.identifier isEqualToString:@"showPhotoFullScreenSegue"]){
        HLShowPhotosInFullScreenVCViewController *vc = segue.destinationViewController;
        vc.photoImageArray = [self.meemo getAllPhotosOriginalSizeNSURL];
        vc.selectedPhotoIndex = [sender integerValue];
    }
}


- (IBAction)unwindFromShowPhotoInFullScreen:(UIStoryboardSegue *)segue {
    
 
    
    
}
#pragma mark - UIWevViewDelegate
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    CGRect frame = self.webView.frame;
    frame.size.height = 1;
    self.webView.frame = frame;
    CGSize fittingSize = [self.webView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    self.webView.frame = frame;
 
    [self.scrollContentView setContentSize:CGSizeMake(fittingSize.width, self.webView.frame.origin.y+fittingSize.height)];
}
#pragma mark - HLPhotoThumbsDelegate
-(void)photoWasSelected:(NSUInteger)
selectedImageViewIndex{
    NSLog(@"Show photo");
     
    [self performSegueWithIdentifier:@"showPhotoFullScreenSegue" sender:[NSNumber numberWithInt:selectedImageViewIndex ]];
         
}
@end
