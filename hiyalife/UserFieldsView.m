//
//  UserFieldsView.m
//  hiyalife
//
//  Created by Juan Hontanilla on 20/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "UserFieldsView.h"

@interface UserFieldsView()
 
    @property (nonatomic,strong) UIView *mainView;

 

@end

@implementation UserFieldsView


- (id)init
{
    self = [super initWithFrame:CGRectZero];
    [self loadFromNib];
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self loadFromNib];
    return self;
}

- (void)loadFromNib
{
    self.mainView = [[[NSBundle mainBundle] loadNibNamed:@"UserFieldsView" owner:self options:nil] lastObject];
    //self.frame = self.mainView.bounds;
    [self addSubview:self.mainView];
}


@end
