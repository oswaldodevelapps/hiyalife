//
//  HLTextViewMini.m
//  hiyalife
//
//  Created by Pablo on 08/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLTextViewMini.h"
#import <QuartzCore/QuartzCore.h>

@implementation HLTextViewMini

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

- (void)setup {
    
    self.font = [UIFont fontWithName:@"Tauri" size:14.0];
    self.textColor= [UIColor lightGrayColor];
    
    // textView bordes redondeados.
    [self.layer setBackgroundColor: [[UIColor colorWithRed:212/255.0f green:223/255.0f blue:234/255.0f alpha:1]CGColor]];
    [self.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.layer setBorderWidth: 1.0];
    [self.layer setCornerRadius:8.0f];
    [self.layer setMasksToBounds:YES];
    self.alpha = 1.0;
}

@end
