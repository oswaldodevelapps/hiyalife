//
//  HLLocationHelper.m
//  hiyalife
//
//  Created by Oswaldo on 08/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLLocationHelper.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "HLPlace.h"
@interface HLLocationHelper()
@property (nonatomic,strong) MKLocalSearch *localSearch;

@end

@implementation HLLocationHelper
+ (HLLocationHelper *)sharedInstance
{
    static HLLocationHelper *_instance;
    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[HLLocationHelper alloc] init];
        }
        return _instance;
    }
}
-(MKLocalSearch *)localSearch{
    if(!_localSearch) _localSearch = [[MKLocalSearch alloc] init];
    return _localSearch;
}
-(void)searchPlaceWithQuery:(NSString *)queryString inLocation:(CLLocationCoordinate2D)location aroundDistanceInKms:(NSUInteger)kmsAround additionalPlacesArray:(NSArray *)hlPlaceArray returnBlock:(placesBlock)block{
    NSMutableArray *resultsArray = [[NSMutableArray alloc ]init];
    
    for(HLPlace *localPlace in hlPlaceArray ){
        if([localPlace.caption rangeOfString:queryString options:NSCaseInsensitiveSearch].location != NSNotFound ){
            [resultsArray addObject:localPlace];
        }
    }
    
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
     
    CLRegion *region = nil;
    if(CLLocationCoordinate2DIsValid(location)){
          region = [[CLRegion alloc] initCircularRegionWithCenter:location radius:kmsAround*1000 identifier:@"Hint Region"] ;
        
        
    }
    
    
    [geocoder geocodeAddressString:queryString  inRegion:region completionHandler:^(NSArray *placemarks, NSError *error)
     {
        // NSLog(@"PlaceMarks for %@ =  %@",queryString, placemarks);
         for(CLPlacemark *placemark in placemarks){
       
             NSLog(@"Placemark Info: %@",placemark);
             
             NSString *name = placemark.name ? placemark.name:(placemark.locality?placemark.locality: placemark.subAdministrativeArea);
             
             NSLog(@"Placemark name: %@",name);
             if(name){
                 HLPlace *place = [[HLPlace alloc]init];
                 place.caption = [NSString stringWithFormat:@"%@,%@", name
                                  ,placemark.country] ;
                 
                 place.longitude = placemark.location.coordinate.longitude;
                 place.latitude = placemark.location.coordinate.latitude;
                 
                 [resultsArray addObject:place];
             }
         
            
         }
         
         if(block)block(resultsArray);
     }];
         
    /*
    [self.localSearch cancel];
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];

    request.naturalLanguageQuery = queryString;
    if(CLLocationCoordinate2DIsValid(location)){
        request.region =  MKCoordinateRegionMakeWithDistance(location, kmsAround*1000, kmsAround*1000);
    }
    
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    [self.localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        for(MKMapItem *item in response.mapItems){
 
            
            NSLog(@"Item name %@",item.name);
     
           
            if(placeMark.locality){
                
                NSLog(@"%@",placeMark.locality);
            }
             
        }
        
        if(block)block(resultsArray);
    }];*/
    
    
}
@end
