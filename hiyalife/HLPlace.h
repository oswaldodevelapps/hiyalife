//
//  HLPlace.h
//  hiyalife
//
//  Created by Oswaldo on 08/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLPlace : NSObject
@property (nonatomic) NSUInteger placeID;
@property (nonatomic,strong) NSString *caption;
@property (nonatomic) double longitude;
@property (nonatomic)double latitude;
+(HLPlace *)placeWithDictionary:(NSDictionary *)dict;
@end
