//
//  TextFieldConnector.h
//  KVOField
//
//  Created by Rafa Barberá Córdoba on 02/05/13.
//  Copyright (c) 2013 Rafa Barbera. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RBTextFieldConnectorDelegate <NSObject>

@optional
- (void)textFieldConnector:(UITextField *)textField isValid:(BOOL)valid withError:(NSError *)error;

@end

@interface RBTextFieldConnector : NSObject <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, readonly, getter = isValid) BOOL valid;
@property (nonatomic, weak) id<UITextFieldDelegate> delegate;
@property (nonatomic, weak) id<RBTextFieldConnectorDelegate> validationDelegate;

+ (RBTextFieldConnector *)fieldWithTextField:(UITextField *)textField
                           observingObject:(NSObject *)object forKey:(NSString *)key;

- (id)initWithTextField:(UITextField *)textField
                           observingObject:(NSObject *)object forKey:(NSString *)key;

- (void)changeModelObject:(NSObject *)newModelObject;
- (void)cancelEdit;

@end
