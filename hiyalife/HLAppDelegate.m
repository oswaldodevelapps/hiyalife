//
//  HLAppDelegate.m
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLAppDelegate.h"
#import "HLDataStore.h"
#import "HLJoinCompleteDataNormalAccountViewController.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "HLFacebookHelper.h"
#import "HLRootViewController.h"
#import "SVProgressHUD.h"

@interface HLAppDelegate()
{
    HLRootViewController *hlRootVC;
}
@end

@implementation HLAppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
    [self customizeAppearance];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
 
   
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
 
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
     hlRootVC = [storyboard instantiateViewControllerWithIdentifier:@"HLRootViewController"];
    
    [self.window setRootViewController:hlRootVC];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
 
    if([launchOptions valueForKey:@"UIApplicationLaunchOptionsURLKey"]){
        NSURL *url = [launchOptions valueForKey:@"UIApplicationLaunchOptionsURLKey"];
        [self handleURlOpen:url];
        
    }
     
    return YES;
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[HLDataStore sharedInstance] saveChanges];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    [self handleURlOpen:url];
        
 
    return YES;
}

#pragma mark - Custom Functions


- (void)customizeAppearance
{
    // Create resizable images
    UIImage *headerImage = [[UIImage imageNamed:@"header"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    [[UINavigationBar appearance] setBackgroundImage:headerImage
                                       forBarMetrics:UIBarMetricsDefault];
    
    [[UIToolbar appearance] setBackgroundImage:headerImage
                            forToolbarPosition:UIToolbarPositionAny
                                    barMetrics:UIBarMetricsDefault];
    
    /*
    UIImage *buttonBack = [UIImage imageNamed:@"backButton"];
    
 
  
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:buttonBack
                                                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
     
    */
    [[UIBarButtonItem appearance] setTintColor:[UIColor darkGrayColor]];
    
}
-(void)handleURlOpen:(NSURL *)url{
    
  
    
    [[HLDataStore sharedInstance] validateJoin:url returnBlock:^(HLMeUser *user, NSError *error) {
     
        if(user){
            
            [SVProgressHUD showSuccessWithStatus:@""];
         
            hlRootVC.skipAutoValidation = YES;
            
            [hlRootVC dismissViewControllerAnimated:NO
        completion:^{            
            [hlRootVC performSegueWithIdentifier:@"completeValidationSegue" sender:self];
        }]; 
 
        }
        else{
            [SVProgressHUD showErrorWithStatus:@"User validation failed, or used already valid."];
        }
    
    }];
 
}

 
#pragma mark - TESTS
-(void)testPutMeAPI{
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    
    [HLDataStore sharedInstance].currentUser = [[HLMeUser alloc]init];
    [HLDataStore sharedInstance].currentUser.access_token = @"bdea0fdfa73d2a626b1193e0f3a4a0897ea070ca";
    
    [[HLDataStore sharedInstance].currentUser setAvatarImage:[UIImage imageNamed:@"genuser.png"]];
    
    //push view manually
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    HLJoinCompleteDataNormalAccountViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"HLJoin3ViewController"];
    
    [(UINavigationController*)self.window.rootViewController pushViewController:ivc animated:NO];
}
-(void)testGetFacebookToken{
    [[HLFacebookHelper sharedInstance] getFacebookToken:^(NSString *token){
        NSLog(@"Facebook Token %@",token);
    }];
}
-(void)testLoginFacebook
{
    NSString *token = @"CAAF6uqDipYYBAHDgMg9fiHvPpimhSC9gPpn8epd3hZAcaZCqodPOqClaSYQBbQqGFE5UKKu3ha48HiQjbKOl3KDdqJX42sFoeWdNr38qEyogUci1aPUl4xANSE8fZCeCxeXnXXL90uNviZAKPUNR7pvkX7qxQY3qfisW9Mawd19ntnZBcUyZCyfkFRsbIdza3ACqXNorXlZAE3azCVlh3tp0yJ7m57puOYZD";
    
    
    [[HLDataStore sharedInstance] getLoginWithFacebookToken:token returnBlock:^(HLMeUser *user, NSError *error) {
        NSLog(@"Login Facebook Response : User %@ / Error %@",user,error);
    
    }];

}
-(void)testJoinFacebook{
    
      NSString *token = @"CAAF6uqDipYYBAHDgMg9fiHvPpimhSC9gPpn8epd3hZAcaZCqodPOqClaSYQBbQqGFE5UKKu3ha48HiQjbKOl3KDdqJX42sFoeWdNr38qEyogUci1aPUl4xANSE8fZCeCxeXnXXL90uNviZAKPUNR7pvkX7qxQY3qfisW9Mawd19ntnZBcUyZCyfkFRsbIdza3ACqXNorXlZAE3azCVlh3tp0yJ7m57puOYZD";
    
    [[HLDataStore sharedInstance] joinUserWithFacebookToken:token returnBlock:^(HLMeUser *user, NSError *error) {
         NSLog(@"JOIN Facebook Response : User %@ / Error %@",user,error);
        
    }];
}


@end
