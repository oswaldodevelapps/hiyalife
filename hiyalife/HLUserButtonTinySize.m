//
//  HLUserButtonTinySize.m
//  hiyalife
//
//  Created by Oswaldo on 04/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLUserButtonTinySize.h"
#import <QuartzCore/QuartzCore.h>

@implementation HLUserButtonTinySize

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
- (void)setup {
    self.titleLabel.font = [UIFont fontWithName:@"Tauri" size:13.0];
    self.titleLabel.textColor = [UIColor darkGrayColor];
    self.showsTouchWhenHighlighted = YES;
    [self.layer setCornerRadius:2.0f];
    [self.layer setBorderWidth:2.0f];
    [self.layer setBorderColor:[UIColor darkGrayColor].CGColor];
}


@end
