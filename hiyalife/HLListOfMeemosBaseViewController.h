//
//  HLListOfMeemosBaseViewController.h
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLUser.h"


@interface HLListOfMeemosBaseViewController : UIViewController
- (IBAction)unwindFromPublish:(UIStoryboardSegue *)segue;
@property (nonatomic,strong) HLUser *currentUser;
@end
