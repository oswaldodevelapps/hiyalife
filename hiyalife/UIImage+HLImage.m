//
//  UIImage+HLPrivacy.m
//  hiyalife
//
//  Created by Juan Hontanilla on 29/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "UIImage+HLImage.h"

@implementation UIImage (HLImage)
+(UIImage *)imageForPrivacy:(HLMeemoPrivacyType)type blackOrWhite:(BOOL)black{
    switch (type) {
        case HLprivacyLimited:
            
            return [UIImage imageNamed:black ? @"limited_black": @"limited"];
            break;
        case HLprivacyPrivate:
            return [UIImage imageNamed:black ? @"private_black": @"private"];
        case HLprivacyPublic:
            return [UIImage imageNamed:black ? @"public_black": @"public"];
        default:
            break;
    }
}

+(UIImage *)paintImage:(UIImage *)image withColor:(UIColor *)color
{
    UIImage *img;
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContext(image.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];
    CGContextFillRect(context, rect);
    [image drawAtPoint:CGPointZero blendMode:kCGBlendModeDestinationIn alpha:0.4];
    img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
@end
