//
//  HLUserView.h
//  hiyalife
//
//  Created by Oswaldo on 04/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLUser.h"

@interface HLUserView : UIView
@property(nonatomic,strong) HLUser *user;
-(id)initWithUser:(HLUser*)user  ;

@end
