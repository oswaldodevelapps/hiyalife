//
//  RBTextViewConnector.h
//  MeetQute
//
//  Created by Rafa Barberá on 18/05/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol RBTextViewConnectorDelegate <NSObject>

@optional
- (void)textViewConnector:(UITextView *)textView isValid:(BOOL)valid;

@end

@interface RBTextViewConnector : NSObject <UITextViewDelegate>

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, readonly, getter = isValid) BOOL valid;
@property (nonatomic, weak) id<UITextViewDelegate> delegate;
@property (nonatomic, weak) id<RBTextViewConnectorDelegate> validationDelegate;

+ (RBTextViewConnector *)fieldWithTextView:(UITextView *)textView
                             observingObject:(NSObject *)object forKey:(NSString *)key;

- (id)initWithTextView:(UITextView *)textView
        observingObject:(NSObject *)object forKey:(NSString *)key;

- (void)changeModelObject:(NSObject *)newModelObject;
- (void)cancelEdit;

@end
