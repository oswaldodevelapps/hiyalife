//
//  HLHeaderView.m
//  hiyalife
//
//  Created by Juan Hontanilla on 11/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLHeaderView.h"

@implementation HLHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
-(HLLabelMediumSizeDark *)label{
    if(!_label){
      _label = [[HLLabelMediumSizeDark alloc]initWithFrame:CGRectMake(68,4, 190, 35)];
        _label.adjustsFontSizeToFitWidth = YES;
     
        _label.minimumScaleFactor = 0.5f;
        _label.textAlignment = NSTextAlignmentCenter;
        _label.numberOfLines = 2;
    }
        
    
    return _label;
}
- (void)setup {
    [self setBackgroundColor:[UIColor clearColor]];
    UIImageView *sepizq = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"grey_line"]];
    UIImageView *sepder = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"grey_line"]];
  
    
    sepizq.frame = CGRectMake(20,16, 42, 15);
    sepizq.contentMode = UIViewContentModeScaleAspectFit;
    sepder.frame = CGRectMake(258,16,42,15);
    sepder.contentMode = UIViewContentModeScaleAspectFit;
    
 
    
    
    self.label.backgroundColor = [UIColor clearColor];
    self.label.text = @"";
    
    
    [self addSubview:sepizq];
    [self addSubview:sepder];
 
    [self addSubview:self.label];
}
@end
