//
//  HLCustomNotificationCell.h
//  hiyalife
//
//  Created by Oswaldo on 03/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLCustomNotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *textNotificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (assign,nonatomic)  int typeNotification;

@end
