//
//  HLNavigationLoginViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 07/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLNavigationLoginViewController.h"

@interface HLNavigationLoginViewController ()

@end

@implementation HLNavigationLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
