//
//  HLLabelNormalSize.m
//  hiyalife
//
//  Created by Juan Hontanilla on 09/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLLabelNormalSize.h"

@interface HLLabelNormalSize()

 
@end

@implementation HLLabelNormalSize

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
- (void)setup {
 
    self.font= [UIFont fontWithName:@"Tauri" size:18.0];
    self.textColor  = [UIColor lightGrayColor];
}

 

 
@end
