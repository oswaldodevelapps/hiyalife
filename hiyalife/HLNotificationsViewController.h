//
//  HLNotificationsViewController.h
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLNotificationsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
