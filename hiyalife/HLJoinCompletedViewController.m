//
//  HLJoin2ViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 13/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLJoinCompletedViewController.h"

@interface HLJoinCompletedViewController ()
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end

@implementation HLJoinCompletedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.messageLabel.text = [NSString stringWithFormat:@"Validate your account by clicking on the link we've sent to\r%@",self.currentUser.email];
}

 

@end
