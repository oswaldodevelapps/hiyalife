//
//  UIImage+ImageResizer.m
//  hiyalife
//
//  Created by Oswaldo on 12/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "UIImage+ImageResizer.h"

@implementation UIImage (ImageResizer)
+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(UIImage *)imageWithImage:(UIImage *)image scaledProportionalToMaxWidth:(CGFloat) width{
    
    return [self imageWithImage:image scaledToSize:CGSizeMake(width, (width/image.size.width)*image.size.height)];
  
}
@end
