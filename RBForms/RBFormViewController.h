//
//  RBFormViewController.h
//  KVOField
//
//  Created by Rafa Barberá Córdoba on 02/05/13.
//  Copyright (c) 2013 Rafa Barbera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RBFormViewController : UIViewController

// The model that is connected with fields
@property (nonatomic, strong) NSObject *modelObject;

// Are there any view with input?
@property (weak, nonatomic) UIView *activeView;

// ScrollView for textFieldViews
@property (nonatomic, strong) IBOutlet UIScrollView *fieldsScrollerView;

// Connect fields to model
- (void)connectTextField:(UITextField *)textField forKey:(NSString *)key;
- (void)connectTextView:(UITextView *)textView forKey:(NSString *)key;
- (void)connectSimplePickerWithOptions:(NSArray *)options toTextField:(UITextField *)textField forKey:(NSString *)key;
- (void)connectDatePickerWithFormatandMaxDate:(NSString *)format withMaxDate:(NSDate *)maximumDate toTextField:(UITextField *)textField forKey:(NSString *)key;
- (void)connectDatePickerWithFormat:(NSString *)format toTextField:(UITextField *)textField forKey:(NSString *)key;
-(void)voidMethod;	

// Present/Dismiss some custom inputView
- (void)presentInputView:(UIView *)inputView animated:(BOOL)animated completion:(void(^)(BOOL finished))completionBlock;
- (void)dismissInputViewAnimated:(BOOL)animated completion:(void(^)(BOOL finished))completionBlock;

// Cancel any input in form
- (void)cancelInput;

// Validate UITextFields & UITextViews agains the model
- (BOOL)validateTextFields;

// Called everytime the users changes the active
- (void)changedActiveView:(UIView *)view;

// Helpers functions
- (UIButton *)overlayButtonWithAction:(SEL)action onTopOfView:(UIView *)view;
- (void)moveToStartPositionView:(UIView *)view;
- (CGPoint)computeBottomPointForView:(UIView *)view;



@end
