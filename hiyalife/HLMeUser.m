//
//  HLUser.m
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLMeUser.h"
#import "HLDataStore.h"
#import "NSDate+HLDateFormatter.h"
#import "HLChapter.h"

@implementation HLMeUser
+(HLMeUser *)makeUserWithDictionary:(NSDictionary *)dict{
    if(dict){
    
        HLMeUser * user = [[HLMeUser alloc]init];
     
        [user updateUserInfoWith:dict];
 
        return user;
    }
    return nil;
}

-(void)updateUserInfoWith:(NSDictionary *)dict{
    
    
    if([dict valueForKey:@"access_token_datetime"])
        self.access_token_datetime = [dict valueForKey:@"access_token_datetime"];

    if([dict valueForKey:@"access_token"])
        self.access_token = [dict valueForKey:@"access_token"];
    
    if([[dict valueForKey:@"expires_in"] integerValue])
    self.expires_in = [[dict valueForKey:@"expires_in"] integerValue] ;
    
    if([dict valueForKey:@"refresh_token"])
        self.refresh_token = [dict valueForKey:@"refresh_token"];
    
    if([dict valueForKey:@"token_type"])
        self.token_type = [dict valueForKey:@"token_type"];
    
    if([dict valueForKey:@"id"])
        self.hiyaUserID = [[dict valueForKey:@"id"] intValue];
    
    
    if([dict valueForKey:@"birthday"]){
        if([[dict valueForKey:@"birthday"] class] != [NSNull class])
        self.birthday = [NSDate dateFromString:[dict valueForKey:@"birthday"]]   ;
    }
    if([dict valueForKey:@"email"]){
        self.email = [dict valueForKey:@"email"] ;
    }
    if([dict valueForKey:@"gender"]){
        NSString *gender =[dict valueForKey:@"gender"];
        if(gender){
            if([gender isEqualToString:@"m"])
                self.gender = @"Male";
            else
                self.gender = @"Female";
        }
        else
            self.gender = @"";
    }
    if([dict valueForKey:@"name"]){
        self.name = [dict valueForKey:@"name"] ;
    }

    if([dict valueForKey:@"surname"]){
        self.surname = [dict valueForKey:@"surname"] ;
    }
    if([dict valueForKey:@"photo"]){
        self.photoURL = [dict valueForKey:@"photo"] ;
        
        [HLDataStore downloadImageWithURL:self.photoURL success:^(UIImage *image) {
            [self setAvatarImage:image];
        } ] ;
        
    }
    
    NSMutableDictionary *mutableDict = [dict mutableCopy];
    for(NSString *key in dict){
        id obj = [dict objectForKey:key];
        if([obj class]== [NSNull class]){
            NSLog(@"obj Key %@ : class %@" , key, [obj class]);
            [mutableDict removeObjectForKey:key];
        }
    }
 
    
    if(self.json){
        NSMutableDictionary *mutableJson = [self.json mutableCopy];
        [mutableJson addEntriesFromDictionary:mutableDict];
        self.json = mutableJson;
    }else{
        
        self.json = mutableDict;
    }


}
-(NSString *)description{
    return [NSString stringWithFormat:@"HLUser: (access_token=%@, expires_in=%d, name=%@, email=%@, birthdate=%@, gender=%@",self.access_token, self.expires_in, self.name,self.email,self.birthday,self.gender ];
}
-(BOOL)isMe{
     return YES;
}
 

-(NSArray * )getChaptersNames{
    NSMutableArray *names = [[NSMutableArray alloc]init];
    for (HLChapter *chapter in self.userChapters){
        [names addObject:chapter.name];
    }
    return names;
}

-(void)setAvatarImage:(UIImage *)avatarImage
{
    
    
    NSData *d = UIImageJPEGRepresentation(avatarImage, 0.8);

    [d writeToFile:[self userImageArchivePath] atomically:YES];
    
}
- (NSString *)userImageArchivePath
{
    NSArray *documentDirectories =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    
    // Get one and only document directory from that list
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    
    return [documentDirectory stringByAppendingPathComponent:@"userPhoto.jpg"];
}
-(UIImage *)avatarUIImage{
    
    UIImage *image = [UIImage imageNamed:[self userImageArchivePath] ];
    if(!image)
        image = [UIImage imageNamed:@"genuser"];
    
    return image;
}

-(void)accessTokenRefreshed:(TokenBlock) token{
    if(![self isTokenExpired]){
        if(token)
            token(self.access_token);
    }
    else{
      
        
        [[HLDataStore sharedInstance] getRefreshToken:^(NSString *access_token, NSString *refresh_token) {
            self.access_token = access_token;
            self.refresh_token = refresh_token;
            self.access_token_datetime = [[NSDate alloc]init];
            [[HLDataStore sharedInstance] saveChanges];
            
            
                if(token)
                    token(access_token);
        }];
        
    }
}



-(BOOL)isTokenExpired{
 
    NSTimeInterval interval = [self.access_token_datetime  timeIntervalSinceNow];
 
    return -interval > self.expires_in;
    
}


#pragma mark - KVO Validation Methods

-(BOOL)validateUsername:(id *)ioValue error:(NSError * __autoreleasing *)outError{
    
    if(*ioValue == nil ){
        
        if(outError!= NULL){
            NSString *errorString = @"Email empty";
            NSDictionary *userInfoDict = @{ NSLocalizedDescriptionKey : errorString };
            *outError = [[NSError alloc] initWithDomain:USER_ERROR_DOMAIN code:USER_EMAIL_INVALID userInfo:userInfoDict];
            
        }
        return NO;
    }
    else{
        if(![HLDataStore validateEmail:*ioValue]){
            if(outError!= NULL){
                NSString *errorString = @"Email address not valid";
                NSDictionary *userInfoDict = @{ NSLocalizedDescriptionKey : errorString };
                *outError = [[NSError alloc] initWithDomain:USER_ERROR_DOMAIN code:USER_EMAIL_INVALID userInfo:userInfoDict];
                
            }
            return NO;
        }
        else
            return YES;
    }
  
    NSLog(@"Is valid email? %@",*ioValue);
    return YES;
}

#define MINLENGTHPASSWORD 4
-(BOOL)validatePassword1:(id *)ioValue error:(NSError * __autoreleasing *)outError{
    return [self validatePassword:ioValue error:outError];
}
-(BOOL)validatePassword2:(id *)ioValue error:(NSError * __autoreleasing *)outError{
    return [self validatePassword:ioValue error:outError];
}
-(BOOL)validatePassword:(id *)ioValue error:(NSError * __autoreleasing *)outError{
    
    if(*ioValue == nil){
        if(outError!= NULL){
            NSString *errorString = @"Empty Password";
            NSDictionary *userInfoDict = @{ NSLocalizedDescriptionKey : errorString };
            *outError = [[NSError alloc] initWithDomain:USER_ERROR_DOMAIN code:0 userInfo:userInfoDict];
            
        }
        return NO;
    }else{
        NSString *value = *ioValue;
        if([value length]<MINLENGTHPASSWORD){
            if(outError!= NULL){
                NSString *errorString = @"Password Too short";
                NSDictionary *userInfoDict = @{ NSLocalizedDescriptionKey : errorString };
                *outError = [[NSError alloc] initWithDomain:USER_ERROR_DOMAIN code:0 userInfo:userInfoDict];
                
            }
            return NO;
        }
    }
    return YES;
}

@end
