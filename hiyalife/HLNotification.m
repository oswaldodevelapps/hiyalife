//
//  HLNotification.m
//  hiyalife
//
//  Created by Oswaldo on 02/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLNotification.h"
#import "HLDataStore.h"

@implementation HLNotification

+(HLNotification *)makeNotificationWithDictionary:(NSDictionary *)dict;{
    
    HLNotification *notification = [[HLNotification alloc] init];
    notification.notificationID = [[dict valueForKey:@"id"] integerValue];
    notification.isReaded =  ([[dict valueForKey:@"readed"]integerValue] == 0) ? NO:YES;
    notification.typeNotification = [[dict valueForKey:@"type"] integerValue];
    
    //---Text---
    NSString *strText   = [dict valueForKey:@"text"];
    NSRange range       = [strText rangeOfString:@":"];
    NSString *substring = [[strText substringFromIndex:NSMaxRange(range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    notification.text = substring;
    
    
    //---typeString---
    NSString *substring2 = [[strText substringToIndex:NSMaxRange(range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    notification.typeString = substring2;

    notification.ideMeemo     = [dict valueForKey:@"meemo"];
    
    NSDictionary *dictUser     = [dict valueForKey:@"user"];
    notification.userName      = [dictUser valueForKey:@"name"];
    notification.userPhotoURL  = [[NSURL alloc]initWithString:[dictUser valueForKey:@"photo"]];
    
    if([[dict valueForKey:@"date"] class] != [NSNull class]  ){
        notification.date  = [[HLDataStore sharedInstance] translateStringToDate:[dict valueForKey:@"date"]];
    }else{
        notification.date  = [[NSDate alloc] init];
    }
    
    return notification;
}

-(NSString *)description{
    return [NSString stringWithFormat:@"Notification Type %@ - %@",self.typeString,self.text];
}


@end
