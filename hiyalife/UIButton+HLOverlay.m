//
//  UIButton+HLOverlay.m
//  hiyalife
//
//  Created by Juan Hontanilla on 13/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "UIButton+HLOverlay.h"

@implementation UIButton (HLOverlay)
+ (UIButton *)overlayButtonWithAction:(SEL)action onTarget:(id)target onTopOfView:(UIView *)view
{
    UIButton *overlay = [UIButton buttonWithType:UIButtonTypeCustom];
    overlay.frame = view.frame;
    
    [overlay addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [view.superview insertSubview:overlay aboveSubview:view];
    return overlay;
}
@end
