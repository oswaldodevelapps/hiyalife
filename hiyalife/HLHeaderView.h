//
//  HLHeaderView.h
//  hiyalife
//
//  Created by Juan Hontanilla on 11/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLLabelMediumSizeDark.h"
@interface HLHeaderView : UIView
@property (nonatomic,strong) HLLabelMediumSizeDark *label;
@end
