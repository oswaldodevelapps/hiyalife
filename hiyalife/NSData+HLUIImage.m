//
//  UIImage+HLUIImage.m
//  hiyalife
//
//  Created by Juan Hontanilla on 05/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "NSData+HLUIImage.h"

@implementation NSData (HLUIImage)
+ (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
            
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}
@end
