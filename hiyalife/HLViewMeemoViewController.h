//
//  HLViewMeemoViewController.h
//  hiyalife
//
//  Created by Juan Hontanilla on 04/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLMeemo.h"


@interface HLViewMeemoViewController : UIViewController
@property (nonatomic,strong) HLMeemo *meemo;
- (IBAction)unwindFromShowPhotoInFullScreen:(UIStoryboardSegue *)segue ;
@end
