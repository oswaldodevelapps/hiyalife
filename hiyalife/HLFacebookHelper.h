//
//  HLFacebookHelper.h
//  hiyalife
//
//  Created by Juan Hontanilla on 08/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^getTokenBlock)(NSString *token );

@interface HLFacebookHelper : NSObject
+(HLFacebookHelper *)sharedInstance;
-(void)getFacebookToken:(getTokenBlock)block;

@end
