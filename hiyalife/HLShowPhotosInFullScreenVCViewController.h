//
//  HLShowPhotosInFullScreenVCViewController.h
//  hiyalife
//
//  Created by Oswaldo on 15/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLShowPhotosInFullScreenVCViewController : UIViewController
@property(nonatomic,strong) NSArray *photoImageArray;
@property (nonatomic) NSUInteger selectedPhotoIndex;
@end
