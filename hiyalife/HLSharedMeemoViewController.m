//
//  HLSharedMeemoViewController.m
//  hiyalife
//
//  Created by Pablo on 03/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLSharedMeemoViewController.h"
#import "UIImageView+AFNetworking.h"
#import "HLDataStore.h"

#import "HLOtherUser.h"
#import "ORSelectableViewScroller.h"
#import "HLUserButtonTinySize.h"
#import "HLUserView.h"
#import "HLEmailUser.h"
#import "SVProgressHUD.h"

#import<Social/Social.h>

#import <QuartzCore/QuartzCore.h>

@interface HLSharedMeemoViewController ()<ORSelectableViewScrollerDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedPrivacy;
@property (weak, nonatomic) IBOutlet UIView *optionsView;
@property (weak, nonatomic) IBOutlet UIView *privateView;
@property (weak, nonatomic) IBOutlet UIView *limitedView;
@property (weak, nonatomic) IBOutlet UIView *publicView;
@property (weak, nonatomic) IBOutlet UILabel *labelPrivacy;
//@property (assign) int  currentPrivacy;
//@property (assign) BOOL showPrivacyPanel;

@property (weak, nonatomic) IBOutlet UITextField *searchFriendTextField;
@property (weak, nonatomic) IBOutlet UITextView  *sharedTextView;

@property(strong,nonatomic) NSArray *resultFriendsArray;
@property(strong,nonatomic) NSArray *selectedFriendsArray;

@property(strong,nonatomic) ORSelectableViewScroller* userListToolBar;
@property (weak, nonatomic) IBOutlet ORSelectableViewScroller *destinationView;

@end

@implementation HLSharedMeemoViewController

#define MAX_LENGTH 140  //maximo caracteres twitter

#define PADD_INTERVIEWS 10
#define VIEWS_ORIGIN_X  15
#define VIEWS_WIDTH     290
#define OPTIONS_VIEW_HEIGHT 75
#define PRIVATE_VIEW_HEIGHT 44
#define LIMITED_VIEW_HEIGHT 233
#define PUBLIC_VIEW_HEIGHT  54

@synthesize selectedFriendsArray = _selectedFriendsArray;

-(void)setResultFriendsArray:(NSArray *)resultFriendsArray
{
    _resultFriendsArray = resultFriendsArray;
    
    [self setTheseUsers:_resultFriendsArray inThatSelectableView:self.userListToolBar];
}


-(void)setSelectedFriendsArray:(NSArray *)selectedFriendsArray
{
    _selectedFriendsArray = selectedFriendsArray;
    [self setTheseUsers:_selectedFriendsArray inThatSelectableView:self.destinationView];
}

-(NSArray *)selectedFriendsArray{
    if(!_selectedFriendsArray){
        _selectedFriendsArray = [[NSArray alloc]init];
    }
    return _selectedFriendsArray;
}


#define HEIGHT_SCROLLER 30
-(ORSelectableViewScroller *)userListToolBar{
    if(!_userListToolBar){
        _userListToolBar = [[ORSelectableViewScroller alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_SCROLLER)];
        _userListToolBar.delegate = self;
    }
    return _userListToolBar;
}


-(void)setTheseUsers:(NSArray *)users inThatSelectableView:(ORViewScroller *)view{
    NSMutableArray *userItemArray = [[NSMutableArray alloc]init];
    
    NSLog(@"Send %d users to view %@",[users count],view);
    
    for (HLOtherUser * user in users) {
        
        HLUserView *userView = [[HLUserView alloc]initWithUser:user ];
        
        [userItemArray addObject:userView];
    }
    
    
    [view setViewElements:userItemArray];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)customView{
    
    // destinationView bordes redondeados.
    [self.destinationView.layer setBackgroundColor: [[UIColor grayColor]CGColor]];
    [self.destinationView.layer setBorderColor:[[[UIColor whiteColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.destinationView.layer setBorderWidth: 1.0];
    [self.destinationView.layer setCornerRadius:8.0f];
    [self.destinationView.layer setMasksToBounds:YES];
    self.destinationView.alpha = 1.0;
    
    self.destinationView.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self customView];
    
	// Do any additional setup after loading the view.
    self.navigationItem.titleView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    self.searchFriendTextField.inputAccessoryView = self.userListToolBar;
    
 
    if (![self.meemo isOwnedByMe]){
        [self.optionsView setHidden:YES];
        [self.privateView setHidden:NO];
    }
    
    [self setPrivacyViewLevel:self.meemo.privacy];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    [[HLDataStore sharedInstance] findUserListByKey:substring
                                        returnBlock:^(NSArray *friends) {
                                            NSLog(@"Found Friends %@", friends);
                                            self.resultFriendsArray = friends;
                                            
                                        }];
}


-(void)setPrivacyViewLevel:(int)level{
    
  
    
    switch (level) {
        case 0:
        
            self.labelPrivacy.text = @"Private (Not shared)";
            [self showPrivateView];
            break;
        case 1:
             
            self.labelPrivacy.text = @"Limited (Shared with a few)";
            BOOL animated = [self.limitedView isHidden];
            [self showLimitedViewAnimated:animated];
            break;
        case 2:
            
            self.labelPrivacy.text = @"Public (Open to everyone)";
            [self showPublicView];
            break;
        default:
            break;
    }
    
    [self.segmentedPrivacy setSelectedSegmentIndex:level];
}




#pragma mark -distributionViews-
-(void)showPrivateView{
    
    [self.privateView setHidden:YES];
    
    [self.limitedView setHidden:YES];
    [self.publicView setHidden:YES];
}

-(void)showLimitedViewAnimated :(BOOL)animated{
    
    [self.privateView setHidden:YES];
    [self.publicView setHidden:YES];
    [self.limitedView setHidden:NO];
    
    if ([self.meemo isOwnedByMe]) {
        CGRect correctFrame = CGRectMake(VIEWS_ORIGIN_X, self.optionsView.frame.origin.y + OPTIONS_VIEW_HEIGHT + PADD_INTERVIEWS , VIEWS_WIDTH, LIMITED_VIEW_HEIGHT);
        if (animated) [self animateFrameWithView:self.limitedView toFrame:correctFrame];
        
    }else{
        CGRect correctFrame = CGRectMake(VIEWS_ORIGIN_X, PADD_INTERVIEWS, VIEWS_WIDTH, LIMITED_VIEW_HEIGHT);
        [self animateFrameWithView:self.limitedView toFrame:correctFrame];
        
        if (animated) [self animateFrameWithView:self.limitedView toFrame:correctFrame];
    }
}

-(void)showPublicView{
    
    [self.privateView setHidden:YES];
    [self showLimitedViewAnimated:NO];
    [self.publicView  setHidden:NO];
                                       
    //---Public view frame ---
    CGRect correctFrame = CGRectMake(VIEWS_ORIGIN_X, self.limitedView.frame.origin.y + LIMITED_VIEW_HEIGHT + PADD_INTERVIEWS , VIEWS_WIDTH, PUBLIC_VIEW_HEIGHT);
    [self animateFrameWithView:self.publicView toFrame:correctFrame];
}

-(void)animateFrameWithView:(UIView*)animateView toFrame: (CGRect)correctFrame{
    
    CGRect initialRect = animateView.frame;
    if (animateView.tag == 2)
        initialRect.origin.y += 200;
    
    animateView.frame = CGRectMake(VIEWS_ORIGIN_X, initialRect.origin.y , VIEWS_WIDTH, 0);
    
    animateView.contentMode = UIViewContentModeRedraw;
    [UIView animateWithDuration:0.4 animations:^{
        animateView.frame = correctFrame;
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark -Actions-

- (IBAction)actionSegmented:(id)sender {
   
    
    if (self.segmentedPrivacy.selectedSegmentIndex != self.meemo.privacy){
        [SVProgressHUD show];
        
        NSDictionary *dict = @{@"privacy":[NSNumber numberWithInt:self.segmentedPrivacy.selectedSegmentIndex]};
        
        NSLog(@"Privacy Dict %@",dict);
        
        [[HLDataStore sharedInstance] editMemo:self.meemo onlyForParameters:dict returnBlock:^(BOOL success) {
            [SVProgressHUD dismiss];
            if(success){
                
                self.meemo.privacy = self.segmentedPrivacy.selectedSegmentIndex;
                [self setPrivacyViewLevel:self.segmentedPrivacy.selectedSegmentIndex]; 
            }
            else{
                self.segmentedPrivacy.selectedSegmentIndex = self.meemo.privacy;
                [SVProgressHUD showErrorWithStatus:@"Error changing permissions"];
            }
        }];
        
       
    
    }
}
- (IBAction)actionDismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionAdd:(id)sender {
    
    HLEmailUser *emailUser = [[HLEmailUser alloc]init];
    emailUser.email = ([HLDataStore validateEmail:self.searchFriendTextField.text]) ? self.searchFriendTextField.text : nil;
    
    /*
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"shareTwitter" ofType:@"png"];
    emailUser.photoURL = resourcePath;
     */
    
    if (emailUser.email) {
        NSMutableArray *mutableUsersArray = [self.selectedFriendsArray mutableCopy];
        [mutableUsersArray addObject:emailUser];
        
        self.selectedFriendsArray = mutableUsersArray;
        
        self.searchFriendTextField.text = @"";
    }
    
}

- (IBAction)actionShare:(id)sender {
    
    if([self.selectedFriendsArray count]==0){
        [SVProgressHUD showErrorWithStatus:@"You didn't select any users for share."];
        return;
    }else{
        NSMutableArray *arr_Users    = [[NSMutableArray alloc]init];
        NSMutableArray *arr_Emails   = [[NSMutableArray alloc]init];
        NSMutableArray *arr_FBUsers  = [[NSMutableArray alloc]init];
        
        
        for (HLUser *user in self.selectedFriendsArray) {
            if ([user isKindOfClass:[HLEmailUser class]])
                [arr_Emails addObject:user.email];
            else
                [arr_Users addObject:[NSNumber numberWithInt:user.hiyaUserID]];
        }
        
        
        [SVProgressHUD showWithStatus:@"Sharing memo..." maskType:SVProgressHUDMaskTypeClear];
        
        [[HLDataStore sharedInstance]shareMeemoID:self.meemo.idMeemo andText:self.sharedTextView.text withArrayUsers:arr_Users withArrayFB:arr_FBUsers withArrayOfEmails:arr_Emails successBlock:^(BOOL success) {
            
            if(success)
                [SVProgressHUD showSuccessWithStatus:@"Your memo has been shared!"];
            else
                [SVProgressHUD showErrorWithStatus:@"Error sharing memo."];
        }];
    }
 
}

- (IBAction)shareWithTwitter:(id)sender {
    [self.searchFriendTextField resignFirstResponder];
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *twitterComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        //add initial text
        [twitterComposer setInitialText:self.sharedTextView.text];
        
        /*
        //add image to post
        UIImageView *img = [[UIImageView alloc]init];
        [img setImageWithURL:[self.meemo getPhotoThumbUrl]];
        [twitterComposer addImage:img.image];
         */
        
        //present composer
        [self presentViewController:twitterComposer animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"twitter Error"
                                  message:@"You may not have set up twitter service on your device or\n                                  You may not connected to internent.\nPlease check ..."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles: nil];
        [alertView show];
    }
}

- (IBAction)shareWithFacebook:(id)sender {
    [self.searchFriendTextField resignFirstResponder];
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *fbComposer = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeFacebook];
        //set the initial text message
        [fbComposer setInitialText:self.sharedTextView.text];
        //add url
        if ([fbComposer addURL:[NSURL URLWithString:@"www.hiyalife.com"]]) {
            NSLog(@"url added");
        }
        
        // you can remove all added URLs as follows
        //[fbComposer removeAllURLs];
        
        
        //add image to post
        /*
        UIImageView *img = [[UIImageView alloc]init];
        [img setImageWithURL:[self.meemo getPhotoThumbUrl]];
    
        if ([fbComposer addImage:img.image]) {
            NSLog(@"Photo added to the post");
        }*/
        
        //remove all added images
        //[fbComposer removeAllImages];
        
        //present the composer to the user
        [self presentViewController:fbComposer animated:YES completion:nil];
        
    }else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Facebook Error"
                                  message:@"You may not have set up facebook service on your device or\n                                  You may not connected to internent.\nPlease check ..."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles: nil];
        [alertView show];
    }
}


#pragma  mark - ORSelectableViewScrollerDelegate
-(void)selectableViewScroller:(ORViewScroller *)scroller viewWasSelected:(UIView *)view{
    
    if(scroller == self.userListToolBar){
        HLUserView *userView = (HLUserView *)view;
        [UIView animateWithDuration:0.3 animations:^{
            userView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            NSLog(@"user %@ was selected",userView.user);
            self.searchFriendTextField.text = @"";
            
            NSMutableArray *mutableUsersArray = [self.selectedFriendsArray mutableCopy];
            [mutableUsersArray addObject:userView.user];
            
            NSMutableArray *mutableSearchArray = [self.resultFriendsArray mutableCopy];
            [mutableSearchArray removeObject:userView.user];
            
            self.resultFriendsArray = mutableSearchArray;
            
            self.selectedFriendsArray = mutableUsersArray;
        }];
    }
    
    
    if(scroller == self.destinationView){
        
        
        HLUserView *userView = (HLUserView *)view;
        
        [UIView animateWithDuration:0.3 animations:^{
            userView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            NSLog(@"Remove from list user %@",userView.user);
            NSMutableArray *mutableUsersArray = [self.selectedFriendsArray mutableCopy];
            [mutableUsersArray removeObject:userView.user];
            self.selectedFriendsArray = mutableUsersArray;
        }];
    }
}


#pragma mark UITextFieldDelegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    
    if([substring length] > 1){
        [self searchAutocompleteEntriesWithSubstring:substring];
        //NSLog(@"Search friends with String: %@",substring);
    }else{
        self.resultFriendsArray = nil;
    }

    return YES;
}

#pragma mark -UITextViewDelegate-
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    
    if(newLength <= MAX_LENGTH)
    {
        return YES;
    } else {
        NSUInteger emptySpace = MAX_LENGTH - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        
        return NO;
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int   movementDistance = 100;
    const float movementDuration = 0.3f;
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}




@end
