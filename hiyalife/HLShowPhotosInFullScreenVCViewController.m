//
//  HLShowPhotosInFullScreenVCViewController.m
//  hiyalife
//
//  Created by Oswaldo on 15/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLShowPhotosInFullScreenVCViewController.h"
#import "RBHorizontalImageScrollerView.h"


@interface HLShowPhotosInFullScreenVCViewController ()<RBHorizontalImageScrollerViewDelegate>
@property (strong, nonatomic) IBOutlet RBHorizontalImageScrollerView *fullScreenScrollView;


@end

@implementation HLShowPhotosInFullScreenVCViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.fullScreenScrollView.delegate = self;
    [self.fullScreenScrollView setImagesURL:self.photoImageArray withGap:0.0f withVerticalGap:0.0f inGroupsOf:1 withContentMode:UIViewContentModeScaleAspectFit];
    if(self.selectedPhotoIndex > 0 ){
        [self.fullScreenScrollView navigateToImageIndex:self.selectedPhotoIndex animated:NO];
    }
  
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
  
}

#pragma mark - RBHorizontalImageScrollerViewDelegate
-(void)imageWasSelected:(NSUInteger)selectedIndex
{
    [self performSegueWithIdentifier:@"unwindSegueFromFullPhoto" sender:nil];
}
@end
