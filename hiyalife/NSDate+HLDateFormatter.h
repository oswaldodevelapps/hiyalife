//
//  NSDate+HLDateFormatter.h
//  hiyalife
//
//  Created by Juan Hontanilla on 31/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (HLDateFormatter)
+(NSArray *)getDaysForMonth:(NSUInteger)month andYear:(NSUInteger)year;
+(NSArray *)getYearsArrayFrom:(NSUInteger)fromYear;
+(NSArray *)getMonthNames;
+(NSUInteger)getIndexForMonthName:(NSString *)monthName;
+(NSDate *)getDateForSomeYearsAgo:(NSUInteger)years;
-(NSString *)dateString;
-(NSString *)dateStringSecondFormat;
+(NSDate *)dateFromString:(NSString *)dateString;
@end
