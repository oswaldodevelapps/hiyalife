//
//  HLPlace.m
//  hiyalife
//
//  Created by Oswaldo on 08/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLPlace.h"

@implementation HLPlace
+(HLPlace *)placeWithDictionary:(NSDictionary *)dict{
    HLPlace *place = [[HLPlace alloc]init];
    place.placeID = [[dict valueForKey:@"id"] intValue];
    place.caption = [dict valueForKey:@"caption"];
    place.latitude = [[dict valueForKey:@"lat"] doubleValue];
    place.longitude  = [[dict valueForKey:@"lng"] doubleValue];
    return place;

}

-(NSString *)description{
    return [NSString stringWithFormat:@"PLACE (id=%d,caption=%@,lat=%f,long=%f",self.placeID,self.caption,self.latitude,self.longitude];
}
@end
