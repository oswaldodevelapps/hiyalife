//
//  HLButton.m
//  hiyalife
//
//  Created by Juan Hontanilla on 09/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLButton.h"

@implementation HLButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
- (void)setup {
    self.titleLabel.font = [UIFont fontWithName:@"Tauri" size:24.0];
    self.titleLabel.textColor = [UIColor darkGrayColor];
}


@end
