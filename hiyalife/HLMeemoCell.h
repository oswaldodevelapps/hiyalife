//
//  HLMeemoCell.h
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLLabelTinySizeWhite.h"
#import "HLLabelNormalSize.h"

typedef enum{
    HLMeemoLayoutFull,
    HLMeemoLayoutFullWithPhoto,
    HLMeemoLayoutThumb
}HLMeeMoCellLayout;


@interface HLMeemoCell : UICollectionViewCell
@property (strong, nonatomic)  UIImageView *imageView;
@property (strong, nonatomic)  UIView *thumbView;


 
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;

@property (strong, nonatomic)  HLLabelTinySizeWhite *whereAndWhenView;
@property (strong,nonatomic) HLLabelNormalSize *titleText;



@property (weak, nonatomic) IBOutlet UIImageView *privacyImageView;

@property (weak, nonatomic) IBOutlet UILabel *ownerNameLabel;

-(void)clearContentsWithLayout:(HLMeeMoCellLayout)layout;
-(void)setBottomPhotos:(NSArray *)photosNSURLs;
@end
