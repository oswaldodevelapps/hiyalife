//
//  ORSelectableViewScroller.h
//  hiyalife
//
//  Created by Oswaldo on 04/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "ORViewScroller.h"

@protocol ORSelectableViewScrollerDelegate <NSObject>
 
-(void) selectableViewScroller:(ORViewScroller *)scroller viewWasSelected:(UIView *)view;

@end


@interface ORSelectableViewScroller : ORViewScroller

@property (nonatomic, weak) id<ORSelectableViewScrollerDelegate> delegate;

@end
