//
//  TextFieldConnector.m
//  KVOField
//
//  Created by Rafa Barberá Córdoba on 02/05/13.
//  Copyright (c) 2013 Rafa Barbera. All rights reserved.
//

#import "RBTextFieldConnector.h"
#import <QuartzCore/QuartzCore.h>

@interface RBTextFieldConnector() 

@property (nonatomic, strong) NSObject *observedObject;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, readwrite) BOOL valid;
@property (nonatomic) BOOL hasBeenValidated;


@end

@implementation RBTextFieldConnector


+ (RBTextFieldConnector *)fieldWithTextField:(UITextField *)textField
                           observingObject:(NSObject *)object forKey:(NSString *)key;
{
    return [[RBTextFieldConnector alloc] initWithTextField:textField observingObject:object forKey:key];
}

- (id)init
{
    NSAssert(YES, @"use initTextField:observingObject:forKey:");
    return nil;
}

- (id)initWithTextField:(UITextField *)textField
          observingObject:(NSObject *)object forKey:(NSString *)key;
{
    self = [super init];
    if (self) {
        _hasBeenValidated = NO;
        _observedObject = object;
        _key = key;
        [_observedObject addObserver:self forKeyPath:_key options:NSKeyValueObservingOptionNew context:nil];
        
        _textField = textField;
        _textField.delegate = self;
        _textField.text = [_observedObject valueForKey:_key];
    }
    return self;
}

- (void)setTextField:(UITextField *)textField
{
    if (_textField == textField) return;
    
    _textField.delegate = nil;
    _textField = textField;
    _textField.delegate = self;
    _textField.text = [self.observedObject valueForKey:self.key];
}

- (void)dealloc
{
    [self.observedObject removeObserver:self forKeyPath:self.key];
}

- (void)changeModelObject:(NSObject *)newModelObject
{
    if (newModelObject == self.observedObject) return;
    
    [self.observedObject removeObserver:self forKeyPath:self.key];
    self.observedObject = newModelObject;
    self.textField.text = [self.observedObject valueForKey:self.key];
    [self.observedObject addObserver:self forKeyPath:self.key options:NSKeyValueObservingOptionNew context:nil];
    self.hasBeenValidated = NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ((keyPath == self.key)&&(object == self.observedObject)) {
        NSString *value = [self.observedObject valueForKey:self.key];
        self.textField.text = value;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldDidBeginEditing:)])
        [self.delegate textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSError *error;
    id validatedValue = [textField.text copy];
    
    self.valid = [self.observedObject validateValue:&validatedValue
                                             forKey:self.key
                                              error:&error];
    if (self.isValid) {
        textField.text = validatedValue;
        [self.observedObject setValue:validatedValue forKey:self.key];
    }
    [self setValidationFeedback:self.isValid withError:error];
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldDidEndEditing:)])
        [self.delegate textFieldDidEndEditing:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.textField resignFirstResponder];
    return YES;
}

- (BOOL)isValid
{
    if (!self.hasBeenValidated) {
        NSError *error;
        id validatedValue = [self.textField.text copy];
        
        _valid = [self.observedObject validateValue:&validatedValue
                                             forKey:self.key
                                              error:&error];
        if (_valid) {
            self.textField.text = validatedValue;
            [self.observedObject setValue:validatedValue forKey:self.key];
        }
        [self setValidationFeedback:_valid withError:error];
        self.hasBeenValidated = YES;
    }
    return _valid;
}


- (void)setValidationFeedback:(BOOL)isValid withError:(NSError *)error
{
    if (self.validationDelegate && [self.validationDelegate respondsToSelector:@selector(textFieldConnector:isValid:withError:)]) {
        [self.validationDelegate textFieldConnector:self.textField isValid:isValid withError:error];
    } else {
        if (isValid) {
            self.textField.layer.borderWidth = 0;
        } else {
            self.textField.layer.borderWidth = 1;
            self.textField.layer.borderColor = [UIColor redColor].CGColor;
        }        
    }
}

- (void)cancelEdit
{
    [self.textField resignFirstResponder];
}

@end
