//
//  RBSimplePickerConnector.m
//  MeetQute
//
//  Created by Rafa Barberá Córdoba on 09/05/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import "RBSimplePickerConnector.h"


@interface RBSimplePickerConnector() <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSObject *observedObject;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSArray *options;
@property (nonatomic, strong) UIPickerView *picker;

@end

@implementation RBSimplePickerConnector

+ (RBSimplePickerConnector *) simplePickerConnectorWithOptions:(NSArray *)options targetTextField:(UITextField *)targetTextField observingObject:(NSObject *)object forKey:(NSString *)key
{
    return [[RBSimplePickerConnector alloc] initWithOptions:options targetTextField:targetTextField observingObject:object forKey:key];
}

// TODO: KVO for object
- (id)initWithOptions:(NSArray *)options targetTextField:(UITextField *)targetTextField observingObject:(NSObject *)object forKey:(NSString *)key
{
    self = [super init];
    if (self) {
        self.observedObject = object;
        self.key = key;
        self.options = options;
        self.textField = targetTextField;
        [self updateWithModel];
    }
    return self;
}

- (UIPickerView *)picker
{
    if (!_picker)
    {
        _picker = [[UIPickerView alloc] init];
        _picker.delegate = self;
        _picker.dataSource = self;
        _picker.showsSelectionIndicator = YES;
    }
    return _picker;
}

- (void)changeModelObject:(NSObject *)newModelObject
{
    if (newModelObject == self.observedObject) return;
    self.observedObject = newModelObject;
    [self updateWithModel];
}


#pragma mark - UIPickerViewDataSource

- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == self.picker) {
        return 1;
    }
    return 0;
}

- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == self.picker) {
        return [self.options count];
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == self.picker) {
        return [[self.options objectAtIndex:row] description];
    }
    return 0;
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == self.picker) {
        NSString *value = [[self.options objectAtIndex:row] description];
        self.textField.text = value;
        [self.observedObject setValue:value forKey:self.key];
    }
}

#pragma mark - RBBasePickerConnector

- (id)getPicker
{
    return self.picker;
}

- (void)updateWithModel
{
    int row = [self.options indexOfObject:[self.observedObject valueForKey:self.key]];
    row = (row==NSNotFound)?0:row;
    [self.picker reloadAllComponents];
    [self.picker selectRow:row inComponent:0 animated:YES];
    self.textField.text = [self.observedObject valueForKey:self.key];
}


@end
