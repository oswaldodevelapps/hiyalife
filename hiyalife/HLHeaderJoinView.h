//
//  HLHeaderJoinView.h
//  hiyalife
//
//  Created by Juan Hontanilla on 23/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLLabelMediumSizeDark.h"

@interface HLHeaderJoinView : UIView
@property (nonatomic,strong) HLLabelMediumSizeDark *label;
@end
