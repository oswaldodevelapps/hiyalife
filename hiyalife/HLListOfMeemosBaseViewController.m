//
//  HLListOfMeemosBaseViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLListOfMeemosBaseViewController.h"
#import "HLDataStore.h"
#import "HLMeemoCell.h"
#import "UIImageView+AFNetworking.h" 
#import "HLMeemo.h"
#import "SVProgressHUD.h"
#import "UIImage+HLImage.h"
#import "HLViewMeemoViewController.h"
#import "NSAttributedString+HLMeemo.h"
#import "UIButton+HLOverlay.h"
#import "HLOtherUser.h"
#import "HLAnotherUserFeedViewController.h"
#import "HLTapGestureRecognizerForMemo.h"
#import "UIImageView+FadeIn.h"
@interface HLListOfMeemosBaseViewController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    BOOL firstInRow;
    UIRefreshControl * refreshControl;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
 
@property (weak, nonatomic) IBOutlet UILabel *nodataLabel;
@property(nonatomic,strong) NSArray *meemos;

 



@end

@implementation HLListOfMeemosBaseViewController

 
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(NSArray *)meemos{
    if(!_meemos){
        _meemos = [[NSArray alloc]init];
    }
    return _meemos;
}
 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
      self.navigationItem.titleView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    
    //add refresh control
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [ refreshControl addTarget:self action:@selector(refreshControlAction:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview: refreshControl];
    
    [self reloadData];
 
    self.collectionView.alwaysBounceVertical = YES; 
}
 
-(void)reloadData{
    firstInRow = YES;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm:ss a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"Last updated on %@", [formatter stringFromDate:[NSDate date]]];
    
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    
    [UIView animateWithDuration:0.1f animations:^{
        self.nodataLabel.alpha = 0.0f;
    }];
    
    [[HLDataStore sharedInstance] getFeedForUser:self.currentUser withBlock:^(NSArray *meemos, NSError *error) {
  
    
        if(meemos){
            self.nodataLabel.text = @"";
            self.meemos = meemos;
            [self.collectionView reloadData];
            
        }
        else{
           self.nodataLabel.alpha = 1.0f;
            self.nodataLabel.text = @"No data found...";
        }
        
        if(error){
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        }
        
        
        [refreshControl endRefreshing];
        
    } ];
}


#pragma mark - UICollectionView Datasource
 
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
 
    return [self.meemos count];
}
 
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
 
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
 
    HLMeemoCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"HLCell" forIndexPath:indexPath];
 
    
    if(indexPath.row > ([self.meemos count]-1)) {
        NSLog(@"FATAL ERROR with indexpath in Cell Feed %d",indexPath.row);
        return cell ;
        
    }
    
    HLMeemo *meemo = [self.meemos objectAtIndex:indexPath.row];
    
    
    if(meemo){
   
        [cell setBackgroundColor:meemo.backgroundColor];
        
        if([meemo.photos count] == 0){
            [cell clearContentsWithLayout:HLMeemoLayoutFull];
        }
        else if([meemo.photos count]< 5){
                [cell clearContentsWithLayout:HLMeemoLayoutFullWithPhoto];
        
        }else{
            [cell clearContentsWithLayout:HLMeemoLayoutThumb];
            [cell setBottomPhotos:[meemo getNextFivePhotoThumbUrl]];
        } 
       
        
        NSURL * thumbUrl = [meemo getPhotoThumbUrl];
        if(thumbUrl){
            
            [cell.imageView fadeInFromURL:thumbUrl placeholder:[UIImage imageNamed:@"placeholder"]];
   
        }
        
        if([meemo.owner valueForKey:@"photo"]){
            [cell.avatarView fadeInFromURL:[NSURL URLWithString:[meemo.owner valueForKey:@"photo"]] placeholder:[UIImage imageNamed:@"genuser"]];
             
        }
        
        HLTapGestureRecognizerForMemo *tapOwnerAction =  [[HLTapGestureRecognizerForMemo alloc]
                                             initWithTarget:self
                                             action:@selector(showAnotherUserFeed:)];
        
        tapOwnerAction.memo = meemo;
         
        
        cell.avatarView.userInteractionEnabled  = YES;
        
        
        [cell.avatarView addGestureRecognizer:tapOwnerAction];
                
        cell.ownerNameLabel.text = [meemo.owner valueForKey:@"name"];
       
          cell.ownerNameLabel.textColor = [UIColor whiteColor];
 
         cell.whereAndWhenView.attributedText = [NSAttributedString dateAndWhereAttributedDescriptionWithMeemo:meemo];
        
        cell.titleText.text = meemo.text ? [NSString stringWithFormat:@"“%@”", meemo.text] : @"";
                                            
                                             
        cell.privacyImageView.image =  [UIImage imageForPrivacy:meemo.privacy blackOrWhite:NO];
 
        
    }
 
    return cell;


}

#pragma  mark - Actions

 

-(void)showAnotherUserFeed:(HLTapGestureRecognizerForMemo *)sender{
    
  
    HLMeemo * meemo = sender.memo;
    if (meemo)
    {
 
        
         HLOtherUser *user = [HLOtherUser makeUserWithDictionary:meemo.owner];
        
        if(self.currentUser){
            
            if([self.currentUser class] == [HLOtherUser class]){
                //from Another user Feed, we stay in another user feed. Only change the current user
                
                if(self.currentUser.hiyaUserID != user.hiyaUserID){
                    self.currentUser = user;
                    [self reloadData];
                }
               
            }else{
                //from My Life
                [self performSegueWithIdentifier:@"showAnotherUserFeed" sender:user];
                
            }
        }
        else{
            //from discover
            [self performSegueWithIdentifier:@"showAnotherUserFeed" sender:user];
        }
        
        
        
        
        
    }
}
 

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
   
    
    HLMeemo *meemo = [self.meemos objectAtIndex:indexPath.row];
    if(meemo){
         NSLog(@"Show meemo %@",meemo);
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        [[HLDataStore sharedInstance] getMeemoDetails: meemo forUser:nil withBlock:^(HLMeemo *meemo) {
            
            [SVProgressHUD dismiss];
            if(meemo){
                NSLog(@"Show Meemo details in VC  ");
                NSLog(@"Is Meemo in My TL? %d", meemo.isInMyTimeLine);
                NSLog(@"Is Meemo owned by Me? %d",[ meemo isOwnedByMe]);
                [self performSegueWithIdentifier:@"ViewMemoSegue" sender:meemo];
            }
            else{
                NSLog(@"Error getting memo details for meemo %@",meemo);
                
                [SVProgressHUD showErrorWithStatus:@"Error obtaining details"];
            }
            
            
        }];

        
       
        
        
    }
    
    
    
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
}

#pragma mark – UICollectionViewDelegateFlowLayout

#define CELL_HEIGHT 240

 
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
 
    
    CGSize retval;
    CGSize retValWide = CGSizeMake(310, CELL_HEIGHT);
    CGSize retValShort = CGSizeMake(152.5f, CELL_HEIGHT);
    HLMeemo *nextMeemo = nil;
    
   HLMeemo *currentMeemo = [self.meemos objectAtIndex:indexPath.row];
    
    if(indexPath.row +1 < [self.meemos count]){
          nextMeemo = [self.meemos objectAtIndex:indexPath.row+1]; 
    }
 
    HLMeemoPreferredLayout prefLayout= [currentMeemo preferredLayout];
    
    if(firstInRow){
        if(prefLayout == HLPreferredWide){
             retval = retValWide;
             firstInRow = YES;
        }           
        else{
            if(nextMeemo && [nextMeemo preferredLayout] == HLPreferredWide){
                retval = retValWide;
                 firstInRow = YES;
            }else{
                if(indexPath.row == ([self.meemos count] -1)){
                    retval = retValWide;
                    firstInRow = YES;
                }else{
                    
                    retval = retValShort;
                    firstInRow = NO;
                }
            
            }
        }
  

    }else{
        retval = retValShort;
        firstInRow = YES;
    }
    
    return retval;
  	
}

 

#pragma mark - UIRefreshControl Action
 
-(void)refreshControlAction:(UIRefreshControl *)sender{
    [self reloadData];
  
}

#pragma mark - Outlet Actions

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
 if([segue.identifier isEqualToString:@"ViewMemoSegue"])
 {
 
     
     HLViewMeemoViewController *vmvc = segue.destinationViewController;
     vmvc.meemo = sender;
 }
    
 
    if([segue.identifier isEqualToString:@"showAnotherUserFeed"]){
        HLAnotherUserFeedViewController *otherUservc =segue.destinationViewController;
        otherUservc.currentUser = sender;
    }
 
    
}

- (IBAction)unwindFromProfile:(UIStoryboardSegue *)segue {
 
    
 
}

- (IBAction)unwindFromPublish:(UIStoryboardSegue *)segue {
    NSLog(@"User has published");
}



-(void)dealloc
{
    NSLog(@"Deallocating %@",NSStringFromClass([self class]));
}
@end
