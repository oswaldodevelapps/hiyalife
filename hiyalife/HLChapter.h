//
//  HLChapter.h
//  hiyalife
//
//  Created by Juan Hontanilla on 18/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLChapter : NSObject
@property (nonatomic,strong) NSString *colorHexString;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDate *initialDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic) int idChapter; 

+(HLChapter *)makeChapterWithDictionary:(NSDictionary *)dict;
@end
