//
//  ORPasswordIndicator.h
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ORPasswordIndicator : NSObject
+(void)updatePasswordIndicatorView:(UIView *)view andPassword:(NSString *)password;
@end
