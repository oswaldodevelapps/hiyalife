//
//  UIImage+ImageResizer.h
//  hiyalife
//
//  Created by Oswaldo on 12/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageResizer)

+(UIImage *)imageWithImage:(UIImage *)image scaledProportionalToMaxWidth:(CGFloat) width;
+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
@end
