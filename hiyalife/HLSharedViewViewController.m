//
//  HLSharedViewViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 25/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLSharedViewViewController.h"
#import "HLShareCustomCell.h"
#import "UIImageView+AFNetworking.h"

@interface HLSharedViewViewController ()

@end

@implementation HLSharedViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSArray *)shareListOfOtherUser{
    if(!_shareListOfOtherUser){
        _shareListOfOtherUser = @[
                                  @{@"name":@"paco",@"photo":@"http://prepro.hiyalife.com/images/user/91d8d21d21d64d8890aae88a01537e2496/50x50.jpg?e"},
                                  @{@"name":@"pepe",@"photo":@"http://prepro.hiyalife.com/images/user/91d8d21d21d64d8890aae88a01537e2496/50x50.jpg?e"}
                                  ];
    }
    return _shareListOfOtherUser;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    NSLog(@"Num of Share %d",[self.shareListOfOtherUser count]);
    return [self.shareListOfOtherUser count];
 
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
     HLShareCustomCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"HLShareCell" forIndexPath:indexPath];
    
    NSURL *url = [[self.shareListOfOtherUser objectAtIndex:indexPath.row] valueForKey:@"photo"];
    
    [cell.avatarImage setImageWithURL: url];
    
    cell.nameLabel.text = [[self.shareListOfOtherUser objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    
    return cell;
}


@end
