//
//  HLMeemo.m
//  hiyalife
//
//  Created by Juan Hontanilla on 10/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLMeemo.h"
#import "HLDataStore.h"


@interface HLMeemo()

@end

@implementation HLMeemo

+(UIColor *)getRandomBackgroundColor{
    NSArray *colors = [HLMeemo availableBackgroundColors];
    return [colors objectAtIndex:arc4random() % [colors count]];
}
+(NSArray *)availableBackgroundColors{
    return @[[UIColor colorWithHexString:HL_BLUE_COLOR],[UIColor colorWithHexString:HL_GREEN_COLOR],[UIColor colorWithHexString:HL_PURPLE_COLOR],[UIColor colorWithHexString:HL_YELLO_COLOR]];
}

-(NSMutableDictionary *)photosUImages{
    if(!_photosUImages)
        _photosUImages = [[NSMutableDictionary alloc]init];
    
    return _photosUImages;
}

 

+(HLMeemo *)makeMeemoWithDictionary:(NSDictionary *)dict{
    HLMeemo *meemo = [[HLMeemo alloc] init];
    meemo.json = dict;
    meemo.idMeemo = [dict valueForKey:@"id_meemo"];
 
    
    if([[[dict valueForKey:@"date"] valueForKey:@"mid_date"] class] != [NSNull class] ){
          meemo.date = [[HLDataStore sharedInstance] translateStringToDate:[[dict valueForKey:@"date"] valueForKey:@"mid_date"]];  
    }

    if( [[[dict valueForKey:@"date"] valueForKey:@"text"] class] != [NSNull class]){
         meemo.dateString = [[dict valueForKey:@"date"] valueForKey:@"text"] ;
    }else{
        meemo.dateString = @"";
    }
   
    
    meemo.photos = [dict valueForKey:@"photos"];
 
    
     meemo.backgroundColor = [HLMeemo getRandomBackgroundColor];
     
 
    if([[dict valueForKey:@"text"] class] != [NSNull class]){
 
        meemo.text = [dict valueForKey:@"text"];
    }
    else{
        meemo.text = nil;
    }

    if([[dict valueForKey:@"title"]class] != [NSNull class]){
         meemo.title = [dict valueForKey:@"title"];
    }
   
    
    meemo.owner = [dict valueForKey:@"owner"];
    
    meemo.place = [[[dict valueForKey:@"place"] valueForKey:@"text"] class] == [NSNull class] ? nil: [[dict valueForKey:@"place"] valueForKey:@"text"] ;
    
    meemo.privacy = [[dict valueForKey:@"privacy"] intValue];
    
    
    return meemo;
}
-(NSString *)description{
    return [NSString stringWithFormat:@"Meemo Id:%@\rDate String: %@\rDate:%@\rLocation: %@\rPhotos #: %d\rText: %@\rTitle: %@\rOwner:%@\rOwner URL:%@\rPrivacy: %d\rThumb URL %@ \r\r",self.idMeemo,self.dateString,self.date,self.place , [self.photos count],  self.text,self.title,[self.owner valueForKey:@"name"],[self.owner valueForKey:@""] ,self.privacy,[self getPhotoThumbUrl]];
}


-(NSURL *)getPhotoThumbUrl{
    if([self.photos count]>0){
        //NSString *strUrl = [[self.photos objectAtIndex:0] valueForKey:@"original"];
        NSString *strUrl = [[self.photos objectAtIndex:0] valueForKey:@"view"];        
        return [NSURL URLWithString:strUrl];
    }
    return nil;
}

#define MAXINDEXNEXTFIVE 4
-(NSArray *)getNextFivePhotoThumbUrl{
    
    if([[self photos] count]>2){
        NSMutableArray *array = [[NSMutableArray alloc]init];
        [self.photos  enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            if(idx > MAXINDEXNEXTFIVE)
                *stop = YES;
            else if(idx >0){
                
             [array addObject:[NSURL URLWithString:[obj valueForKey:@"thumb"]]];
            }
       
        }];
      
        return array;
    }
    return nil;
}

-(NSArray *)getAllPhotosUIImage{
    return [self.photosUImages allValues];
}

-(NSArray *)getAllPhotosNSURL{
    NSMutableArray *returnValue = [[NSMutableArray alloc ]init];
    [self.photos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        [returnValue addObject:[NSURL URLWithString:[obj valueForKey:@"view"]]];
    }];
        
    return returnValue;
}

-(NSArray *)getAllPhotosOriginalSizeNSURL{
    NSMutableArray *returnValue = [[NSMutableArray alloc ]init];
    [self.photos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        [returnValue addObject:[NSURL URLWithString:[obj valueForKey:@"original"]]];
    }];
    
    return returnValue;
}
-(NSString *)dateDescription{
    
    return self.dateString;
    
     //meemo.dateString ? meemo.dateString : [[HLDataStore sharedInstance]translateDateToString:meemo.date]
}

-(NSString *)dateAndWhereDescription{
 
    return [[[self dateDescription] stringByAppendingString:@" ● \ue724"] stringByAppendingString:(self.place ? self.place:@"Unknow Place") ];
}

 

-(HLMeemoPreferredLayout)preferredLayout{
    if([self.photos count]>4 ){
        return HLPreferredWide;
    }else{
        if([self.text length]> 30){
            return HLPreferredWide;
        }
        else return HLPreferredShort;
    }
}

-(BOOL)isInMyTimeLine{
    if([self isOwnedByMe]) return YES;
    else return _isInMyTimeLine;
}
-(BOOL)isOwnedByMe{
    return [[self.owner valueForKey:@"id"] intValue] == [HLDataStore sharedInstance].currentUser.hiyaUserID;
}
 
@end