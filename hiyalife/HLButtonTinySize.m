//
//  HLButtonTinySize.m
//  hiyalife
//
//  Created by Pablo on 03/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLButtonTinySize.h"

@implementation HLButtonTinySize

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
- (void)setup {
    self.titleLabel.font = [UIFont fontWithName:@"Tauri" size:12.0];
    self.titleLabel.textColor = [UIColor darkGrayColor];
}

@end
