//
//  HLChapter.m
//  hiyalife
//
//  Created by Juan Hontanilla on 18/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLChapter.h"
#import "HLDataStore.h"

@implementation HLChapter
+(HLChapter *)makeChapterWithDictionary:(NSDictionary *)dict{
    HLChapter *chapter = [[HLChapter alloc] init];
    chapter.idChapter = [[dict valueForKey:@"id_chapter"] intValue];
    chapter.name  = [dict valueForKey:@"name"];
    chapter.colorHexString = [dict valueForKey:@"color"];
    chapter.initialDate  = [[HLDataStore sharedInstance] translateStringToDate:[dict valueForKey:@"ini_date"]];
    
    
    if([[dict valueForKey:@"end_date"] class] != [NSNull class]  ){
          chapter.endDate  = [[HLDataStore sharedInstance] translateStringToDate:[dict valueForKey:@"end_date"]];
    }else{
        chapter.endDate  = [[NSDate alloc] init];
    }
  
    
    return chapter;
}

-(NSString *)description{
    return [NSString stringWithFormat:@"Chapter Id %d - %@",self.idChapter,self.name];
}
@end
