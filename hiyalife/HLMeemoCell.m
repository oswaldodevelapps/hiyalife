//
//  HLMeemoCell.m
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLMeemoCell.h"
#import "UIImageView+AFNetworking.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+FadeIn.h"


@interface HLMeemoCell()

@end
@implementation HLMeemoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    
    }
    return self;
}

 
-(void)clearContentsWithLayout:(HLMeeMoCellLayout)layout{

    
    
    [self clearViews];
 
   
    
    if(layout == HLMeemoLayoutFull){
        self.imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height*0.8f);
        
        
        
    }
    else if(layout == HLMeemoLayoutFullWithPhoto) {
            self.imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
     
        
    }
    

    else if (layout == HLMeemoLayoutThumb)
    {
        
        
        self.imageView.frame = CGRectMake(0, 20, self.bounds.size.width, self.bounds.size.height*0.50);
        
        self.imageView.frame = CGRectInset(self.imageView.frame, 4.0, 4.0);
       
      
        
        self.thumbView.frame = CGRectMake(0,self.imageView.frame.size.height+self.imageView.frame.origin.y-2,self.frame.size.width,self.frame.size.height*0.30f);
        
        [self setBackgroundColor:[UIColor colorWithHexString:HL_DARL_GREY_COLOR]];
    }
    
    
}

-(void)clearViews
{
    self.imageView.image = nil;
    
    for(UIImageView *subview in [self.thumbView subviews] ) {
        subview.image = nil ;
    }
    
    
    self.whereAndWhenView.text = @"";
    self.whereAndWhenView = nil;
    self.titleText.text = @"";
    self.titleText = nil;
 
    [self.avatarView.gestureRecognizers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.avatarView removeGestureRecognizer:obj];
    }];
}

-(UIImageView *)imageView
{
    if(!_imageView){
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height*0.8f)];
        //NSLog(@"Image Frame %@",_imageView);
        _imageView.clipsToBounds = YES;
        
        
        
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [self addSubview:_imageView];
        [self sendSubviewToBack:_imageView];
    
    }
    
    return _imageView;
}

-(UIView *)thumbView
{
    if(!_thumbView){
        _thumbView = [[UIView alloc]init];
        
        
        [self addSubview:_thumbView];
    }
    
    
    
    return  _thumbView;

}
-(HLLabelNormalSize *)titleText
{
    if(!_titleText){
        _titleText = [[HLLabelNormalSize alloc]initWithFrame:CGRectMake(5, 0, self.imageView.frame.size.width-10, 50)];
        _titleText.center = self.imageView.center;
        _titleText.textAlignment = NSTextAlignmentCenter;
        _titleText.backgroundColor = [UIColor clearColor];
        _titleText.textColor = [UIColor whiteColor];
    
        _titleText.adjustsFontSizeToFitWidth = YES;
        [_titleText setMinimumScaleFactor:0.5f];
        _titleText.numberOfLines = 2;
        _titleText.shadowColor = [UIColor darkGrayColor];
        _titleText.shadowOffset = CGSizeMake(0.5f, 0.5f);
 
        
        [self.imageView addSubview:_titleText];
        
    }
    return _titleText;
}

#define FIRST_ROW_HEIGHT_PROP 0.10f
-(HLLabelTinySizeWhite *) whereAndWhenView{
    if(!_whereAndWhenView){
        _whereAndWhenView = [[HLLabelTinySizeWhite alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width-5, self.bounds.size.height*FIRST_ROW_HEIGHT_PROP)];
        
        _whereAndWhenView.textAlignment = NSTextAlignmentRight;
        _whereAndWhenView.backgroundColor = [UIColor clearColor];
        [_whereAndWhenView setMinimumScaleFactor:0.6f];
        _whereAndWhenView.adjustsFontSizeToFitWidth = YES;
    
        
        [self addSubview:_whereAndWhenView];
    }
    return _whereAndWhenView; 
}
 
#define INTERSPACE_THUMB_VIEWS 2
  
-(void)setBottomPhotos:(NSArray *)photosNSURLs
{
    
 
    for(UIView *subview in [self.thumbView subviews] ) {
        [subview removeFromSuperview];
    }
    
    self.thumbView.frame = CGRectInset(self.thumbView.frame, 4.0, 4.0);
     
    
    if([photosNSURLs count]>0){
        
 
        
        CGFloat widthPerBox = self.thumbView.bounds.size.width/[photosNSURLs count];
        
        [photosNSURLs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            float borderLeft = (idx ==0 ? 0 :INTERSPACE_THUMB_VIEWS );
            float borderRight = (idx == [photosNSURLs count] -1 ? 0: INTERSPACE_THUMB_VIEWS);
            
            UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(idx*widthPerBox + borderLeft, INTERSPACE_THUMB_VIEWS*2, widthPerBox - (borderRight+borderLeft), self.thumbView.bounds.size.height-INTERSPACE_THUMB_VIEWS*4)];
            
       
            imgV.clipsToBounds = YES;
            
            [imgV setContentMode:UIViewContentModeScaleAspectFill];
            
            [imgV fadeInFromURL:obj  placeholder:[UIImage imageNamed:@"placeholder"]];             
            
            
            [self.thumbView addSubview:imgV];
        }];
    }

  
    
    
}


@end
