//
//  HLJoin2ViewController.h
//  hiyalife
//
//  Created by Juan Hontanilla on 13/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLMeUser.h"

@interface HLJoinCompletedViewController : UIViewController
@property (nonatomic,strong) HLMeUser *currentUser;
@end
