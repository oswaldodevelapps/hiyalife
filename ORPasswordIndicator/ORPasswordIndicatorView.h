//
//  HLPasswordIndicator.h
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

//Setup your custom values and Images 

#define SEGMENT_VALUE 20

#define SEGMENT_WIDTH 26
#define SEGMENT_HEIGHT 10
#define SEGMENT_CNT  5

#define SEGMENT_IMAGE_OFF @"strong_grey.png"
#define SEGMENT_IMAGE_ON @"strong_green.png"


@interface ORPasswordIndicatorView : UIView

@end
