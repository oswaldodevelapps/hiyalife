//
//  UIImageView+FadeIn.h
//  Snaply
//
//  Created by Rafa Barberá Córdoba on 09/07/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (FadeIn)

- (void)fadeInFromURL:(NSURL *)url;
- (void)fadeInFromURL:(NSURL *)url placeholder:(UIImage *)placeholder;

@end
