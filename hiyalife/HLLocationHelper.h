//
//  HLLocationHelper.h
//  hiyalife
//
//  Created by Oswaldo on 08/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^placesBlock)(NSArray *placesArray);


@interface HLLocationHelper : NSObject
+ (HLLocationHelper *)sharedInstance;
-(void)searchPlaceWithQuery:(NSString *)queryString inLocation:(CLLocationCoordinate2D)location aroundDistanceInKms:(NSUInteger)kmsAround additionalPlacesArray:(NSArray *)hlPlaceArray returnBlock:(placesBlock)block;


@end
