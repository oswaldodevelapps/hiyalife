//
//  ORSelectableViewScroller.m
//  hiyalife
//
//  Created by Oswaldo on 04/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "ORSelectableViewScroller.h"

@implementation ORSelectableViewScroller

-(void)setViewElements:(NSArray *)elements
{
    [super setViewElements:elements];
   

    
    for (UIView *v1  in elements) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(handleSingleTap:)];
        [v1 addGestureRecognizer:tap];
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender{
    if([self.delegate respondsToSelector:@selector(selectableViewScroller:viewWasSelected:)]){
        [self.delegate selectableViewScroller:self viewWasSelected:sender.view];
    }
}

@end
