//
//  HLTextFieldMiniLocked.m
//  hiyalife
//
//  Created by Oswaldo on 02/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLTextFieldMiniLocked.h"

@implementation HLTextFieldMiniLocked

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
- (void)setup {
    
    self.font = [UIFont fontWithName:@"Tauri" size:14.0];
    self.textColor= [UIColor darkGrayColor];
}


@end
