//
//  HLJoinCompleteDataFacebookAccountViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 19/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLJoinCompleteDataFacebookAccountViewController.h"
#import "UserFieldsView.h"
#import "HLMeUser.h"
#import "HLDataStore.h"
#import "ORPasswordIndicator.h"
#import "ORPasswordIndicatorView.h"
#import "SVProgressHUD.h"
#import "NSDate+HLDateFormatter.h"
#import "HLTakeSelectPhotoHelper.h"


@interface HLJoinCompleteDataFacebookAccountViewController ()<UITextFieldDelegate,HLTakeSelectPhotoHelperDelegate>
 
@property (weak, nonatomic) IBOutlet UIScrollView *scrollerView;
@property (weak, nonatomic) IBOutlet UserFieldsView *userDetailsView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *password1TextField;
@property (weak, nonatomic) IBOutlet UITextField *password2TextField;
 
@property (weak, nonatomic) IBOutlet ORPasswordIndicatorView *passwordIndicatorView;

@property (strong, nonatomic) HLMeUser *user;

@property (strong,nonatomic) HLTakeSelectPhotoHelper *photoHelper;

@end

@implementation HLJoinCompleteDataFacebookAccountViewController
-(HLTakeSelectPhotoHelper *)photoHelper
{
    if(!_photoHelper){
        _photoHelper = [[HLTakeSelectPhotoHelper alloc]init];
        _photoHelper.delegate = self;
    }
    
    return _photoHelper;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSingleTap:)];
    
    [self.view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tapPhoto =  [[UITapGestureRecognizer alloc]
                                         initWithTarget:self
                                         action:@selector(handleTapPhoto:)];
    
    [self.userDetailsView.avatarPhoto addGestureRecognizer:tapPhoto];
    
    self.user = [HLDataStore sharedInstance].currentUser;
    
    NSLog(@"Complete Profile for user: %@",self.user);
    
 
    self.userDetailsView.nameTextField.delegate = self;
    self.userDetailsView.surnameTextField.delegate = self;
 
    self.modelObject = self.user;
    
    
    [self connectTextField:self.emailTextField forKey:@"email"];
    
    
    [self connectTextField:self.password1TextField forKey:@"password1"];
    [self connectTextField:self.password2TextField forKey:@"password2"];
    [self connectTextField:self.userDetailsView.nameTextField forKey:@"name"];
    [self connectTextField:self.userDetailsView.surnameTextField forKey:@"surname"];
 
    [self connectDatePickerWithFormatandMaxDate:@"dd/MM/yyyy" withMaxDate:[NSDate getDateForSomeYearsAgo:8] toTextField:self.userDetailsView.birthdayTextField forKey:@"birthday"];
   
  
    [self connectSimplePickerWithOptions:@[@"",@"Male",@"Female"] toTextField:self.userDetailsView.genderTextField forKey:@"gender"];
 
     
    
    self.userDetailsView.avatarPhoto.image  = [self.user avatarUIImage];
    
    
}
- (void)textFieldConnector:(UITextField *)textField isValid:(BOOL)valid withError:(NSError *)error
{
    
    if(!valid){
         NSLog(@"Textfield %@ is NOT valid",textField);
    }
 
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.scrollerView.contentSize = CGSizeMake(self.scrollerView.contentSize.width, self.userDetailsView.frame.size.height+self.userDetailsView.frame.origin.y);
    self.scrollerView.scrollEnabled = YES;
  
}


- (CGPoint)computeBottomPointForView:(UIView *)view
{
    NSLog(@"Compute bottom for view %@",view);
    if([view isDescendantOfView:self.userDetailsView]){
        CGPoint bottomPoint = view.frame.origin;
        bottomPoint.y += self.userDetailsView.frame.origin.y;
        return bottomPoint;
    }
    else{
           return [super computeBottomPointForView:view];  
    }

}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    
    [self cancelInput];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - HLTakeSelectPhotoHelperDelegate
-(void)imageWasTaken:(UIImage *)image
{
    NSLog(@"We have the new image %@",image);
    self.user.editingPhoto = image;
    [self.user setAvatarImage:image];
    [self.userDetailsView.avatarPhoto setImage:image];
    
}
#pragma mark - Actions

-(void)handleTapPhoto:(UITapGestureRecognizer *)sender{
    
    [self.photoHelper takeOrSelectPhotoFromViewController:self];
}

 

- (IBAction)passwordChanged:(UITextField *)sender {
    [ORPasswordIndicator updatePasswordIndicatorView:self.passwordIndicatorView andPassword:sender.text];
}

- (IBAction)finishAction:(id)sender {
    
    BOOL allFieldsAreOk = [self validateTextFields];
 
         if(allFieldsAreOk){
        
            self.user.first_login = YES; //send password
             
        [[HLDataStore sharedInstance] setMeProfileDataForUser:self.user succesBlock:^(BOOL success) {
            
            if(success){
   
                [self performSegueWithIdentifier:@"unwindFromFacebookJoinSegue" sender:self];
                
            }
            else{
                [SVProgressHUD showErrorWithStatus:@"Error setting Profile User Data"];
            }
            
        }];
        
        
        
    }else{
        
   
        [SVProgressHUD showErrorWithStatus:@"Complete the mandatory fields"];
    }
    
}


@end
