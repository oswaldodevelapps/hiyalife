//
//  NSString+HLHTMLContent.h
//  hiyalife
//
//  Created by Juan Hontanilla on 24/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HLHTMLContent)
-(NSString *)embedStringInHTMLBody;
@end
