//
//  RBImageHorizontalScroller.m
//  TutorialWizard
//
//  Created by Rafa Barberá Córdoba on 04/06/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import "RBHorizontalImageScrollerView.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton+HLOverlay.h"

@interface RBHorizontalImageScrollerView()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSArray *urls;
@property (nonatomic, strong) UIImage *placeholder;
@property (nonatomic) CGFloat gapWidth;
@property (nonatomic) CGFloat gapHeight;
@property (nonatomic) int inGroupsOf;
@property (nonatomic) int pagesCount;
@property (nonatomic) CGFloat imageWidthMax;
@property (nonatomic)  UIViewContentMode contentModeImageView;
@end

@implementation RBHorizontalImageScrollerView

@synthesize stepIndicator = _stepIndicator;

- (void)awakeFromNib
{
    self.allowsInteraction = YES;
    if (self.urls || self.images) {
        
        [self setupScroller];
    }
}
 

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _scrollView.delegate = self;
        _scrollView.scrollEnabled = YES;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.directionalLockEnabled = YES;
        _scrollView.alwaysBounceVertical = NO;
        
        [self addSubview:_scrollView];
    }
    return _scrollView;
}
-(void)setActivePage:(int)index{
    
    [self.scrollView setContentOffset:CGPointMake(self.imageWidthMax *self.inGroupsOf * index, 0.0f) animated:YES];
}
- (UIPageControl *)stepIndicator
{
    if (!_stepIndicator) {
        CGRect stepRect, rest;
        CGRectDivide(self.scrollView.bounds, &stepRect, &rest, 20, CGRectMaxYEdge);
        _stepIndicator = [[UIPageControl alloc] initWithFrame:CGRectInset(stepRect, 20, 0)];
        _stepIndicator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _stepIndicator.currentPageIndicatorTintColor = [UIColor grayColor];
        _stepIndicator.pageIndicatorTintColor = [UIColor whiteColor];
        [self addSubview:_stepIndicator];
    }
    return _stepIndicator;
}
-(void)setStepIndicator:(UIPageControl *)stepIndicator
{
    if (_stepIndicator){
        [_stepIndicator removeFromSuperview];
        
    }
    _stepIndicator = stepIndicator;
    
    
}
- (void)setImages:(NSArray *)images withGap:(CGFloat)gapWidth withVerticalGap:(CGFloat)gapHeight inGroupsOf:(int)count {
 
    _inGroupsOf = count;
    _images = images;
    _urls = nil;
    _gapWidth = gapWidth;
    _gapHeight = gapHeight;
    _contentModeImageView = UIViewContentModeScaleAspectFill;
    [self setupScroller ];
}
- (void)setImagesURL:(NSArray *)URLs withGap:(CGFloat)gapWidth withVerticalGap:(CGFloat)gapHeight inGroupsOf:(int)count {
 
    _inGroupsOf = count;
    _urls = URLs;
    _images = nil;
    _gapWidth = gapWidth;
    _gapHeight = gapHeight;
    _contentModeImageView = UIViewContentModeScaleAspectFill;
    [self setupScroller ];
}

-(void)setImagesURL:(NSArray *)URLs withGap:(CGFloat)gapWidth withVerticalGap:(CGFloat)gapHeight inGroupsOf:(int)count withContentMode:(UIViewContentMode)contentMode{
 
    _inGroupsOf = count;
    _urls = URLs;
    _images = nil;
    _gapWidth = gapWidth;
    _gapHeight = gapHeight;
    _contentModeImageView = contentMode;
    [self setupScroller ];
}
- (void)setImages:(NSArray *)images withGap:(CGFloat)gapWidth
{
   
    _inGroupsOf = 1;
    _images = images;
    _gapWidth = gapWidth;
    _urls = nil;
    _contentModeImageView = UIViewContentModeScaleAspectFill;
    [self setupScroller];
}

- (void)setImagesURL:(NSArray *)URLs placeholder:(UIImage *)placeholder withGap:(CGFloat)gapWidth 
{
    
    _urls = URLs;
    _images = nil;
    _gapWidth = gapWidth;
    _placeholder = placeholder;
    _contentModeImageView = UIViewContentModeScaleAspectFill;
    [self setupScroller];
}

-(void)navigateToImageIndex:(NSUInteger)index animated:(BOOL)animated{
    CGPoint targetPoint = CGPointMake(index*self.imageWidthMax, 0);
    if(animated){
        [UIView animateWithDuration:0.5f animations:^{
            
            self.scrollView.contentOffset = targetPoint;
        }];
    }
    else{
        self.scrollView.contentOffset = targetPoint;
    }
}
- (void)setupScroller 
{
    int count_elements = MAX([self.images count],[self.urls count]);
    self.pagesCount = ceil(count_elements / (float)_inGroupsOf);
    
    [[self.scrollView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    self.imageWidthMax =self.scrollView.frame.size.width / MIN(_inGroupsOf, count_elements);
    
    
    CGFloat scrollWidthAdjusted = self.imageWidthMax * _inGroupsOf * self.pagesCount;
    
    self.scrollView.contentSize = CGSizeMake(scrollWidthAdjusted,self.scrollView.frame.size.height);
    
    
    
    
    for (int index=0; index < count_elements; index++) {
   
        UIImageView *imageView;
        
        if(self.urls){
            imageView = [[UIImageView alloc] init];
            [imageView setImageWithURL:[self.urls objectAtIndex:index] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        }else if(self.images){
            imageView = [[UIImageView alloc] initWithImage:[self.images objectAtIndex:index]]; 
        }
    
        
        
        
   
        CGRect boundsRect = CGRectMake(0, 0, self.imageWidthMax, self.scrollView.bounds.size.height);
 
        imageView.frame = CGRectInset(boundsRect, self.gapWidth  , self.gapHeight);
  
       
        
        if(_inGroupsOf > 1){
            //first of group
            if((index % _inGroupsOf) == 0){
                CGRect frame = imageView.frame;
                frame.origin.x = 0;
                frame.size.width +=self.gapWidth;
                imageView.frame =frame;
 
            }
            //last of group
            if((index % _inGroupsOf ) == (_inGroupsOf-1)){
                CGRect frame = imageView.frame;                 
                frame.size.width +=self.gapWidth;
                imageView.frame =frame;
  
            }
        }
      
        
        imageView.frame = CGRectOffset(imageView.frame, index*self.self.imageWidthMax, 0);
        
        
        imageView.contentMode = self.contentModeImageView;
        imageView.clipsToBounds = YES;
        
        [self.scrollView addSubview:imageView];
        
        if(self.allowsInteraction){
            [UIButton overlayButtonWithAction:@selector(showPhoto:) onTarget:self onTopOfView:imageView];
        }
        
        
    }
   
    
    self.stepIndicator.numberOfPages = ceil(count_elements / (float)_inGroupsOf);
 
    self.stepIndicator.hidesForSinglePage = YES;
    
   
}

- (void)showPhoto:(UIButton *)sender{
    NSUInteger selectedIndex = 0;
    for (UIView * view in self.scrollView.subviews){
        if([view isKindOfClass:[UIImageView class]]){
            if(CGRectEqualToRect(view.frame, sender.frame)){
                NSLog(@"Photo in thumb scroller was selected: %@",view);
                if([self.delegate respondsToSelector:@selector(imageWasSelected:)]){
                    [self.delegate imageWasSelected:selectedIndex];
                }
            }
            selectedIndex++;
        }
        
    }
    
}
/*
 DEPRECATED
- (void)setupScrollerWithURLs
{
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width*[self.urls count],
                                             self.scrollView.frame.size.height);
    for (int index=0; index < [self.urls count]; index++) {
        NSURL *url = [self.urls objectAtIndex:index];
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        imageView.frame = CGRectInset(self.scrollView.bounds, self.gapWidth, 0);
        imageView.frame = CGRectOffset(imageView.frame, index*self.scrollView.bounds.size.width, 0);
        [imageView setImageWithURL:url placeholderImage:self.placeholder];
        [self.scrollView addSubview:imageView];
    }
    self.stepIndicator.numberOfPages = [self.urls count];
}
 */

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.stepIndicator.currentPage = round(self.scrollView.contentOffset.x/self.scrollView.bounds.size.width);
}


@end
