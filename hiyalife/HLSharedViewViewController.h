//
//  HLSharedViewViewController.h
//  hiyalife
//
//  Created by Juan Hontanilla on 25/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLSharedViewViewController : UIViewController
@property (nonatomic,strong) NSArray *shareListOfOtherUser;
@end
