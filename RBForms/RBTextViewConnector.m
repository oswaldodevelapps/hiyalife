//
//  RBTextViewConnector.m
//  MeetQute
//
//  Created by Rafa Barberá on 18/05/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import "RBTextViewConnector.h"
#import <QuartzCore/QuartzCore.h>

@interface RBTextViewConnector()

@property (nonatomic, strong) NSObject *observedObject;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, readwrite) BOOL valid;
@property (nonatomic) BOOL hasBeenValidated;


@end

@implementation RBTextViewConnector


+ (RBTextViewConnector *)fieldWithTextView:(UITextView *)textView observingObject:(NSObject *)object forKey:(NSString *)key
{
    return [[RBTextViewConnector alloc] initWithTextView:textView observingObject:object forKey:key];
}

- (id)init
{
    NSAssert(YES, @"use initTextField:observingObject:forKey:");
    return nil;
}

- (id)initWithTextView:(UITextView *)textView observingObject:(NSObject *)object forKey:(NSString *)key
{
    self = [super init];
    if (self) {
        _hasBeenValidated = NO;
        _observedObject = object;
        _key = key;
        [_observedObject addObserver:self forKeyPath:_key options:NSKeyValueObservingOptionNew context:nil];
        
        _textView = textView;
        _textView.delegate = self;
        _textView.text = [_observedObject valueForKey:_key];
    }
    return self;
}

- (void)setTextView:(UITextView *)textView
{
    if (_textView == textView) return;
    
    _textView.delegate = nil;
    _textView = textView;
    _textView.delegate = self;
    _textView.text = [self.observedObject valueForKey:self.key];
}

- (void)dealloc
{
    [self.observedObject removeObserver:self forKeyPath:self.key];
}

- (void)changeModelObject:(NSObject *)newModelObject
{
    if (newModelObject == self.observedObject) return;
    
    [self.observedObject removeObserver:self forKeyPath:self.key];
    self.observedObject = newModelObject;
    self.textView.text = [self.observedObject valueForKey:self.key];
    [self.observedObject addObserver:self forKeyPath:self.key options:NSKeyValueObservingOptionNew context:nil];
    self.hasBeenValidated = NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ((keyPath == self.key)&&(object == self.observedObject)) {
        NSString *value = [self.observedObject valueForKey:self.key];
        self.textView.text = value;
    }
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewDidBeginEditing:)])
        [self.delegate textViewDidBeginEditing:textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSError *error;
    id validatedValue = [textView.text copy];
    
    self.valid = [self.observedObject validateValue:&validatedValue
                                             forKey:self.key
                                              error:&error];
    if (self.isValid) {
        textView.text = validatedValue;
        [self.observedObject setValue:validatedValue forKey:self.key];
    }
    [self setValidationFeedback:self.isValid];
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewDidEndEditing:)])
        [self.delegate textViewDidEndEditing:textView];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
        BOOL acceptInput = [self.delegate textView:textView shouldChangeTextInRange:range replacementText:text];
        if (acceptInput) self.hasBeenValidated = NO;
        return acceptInput;
    } else return YES;
}

- (BOOL)isValid
{
    if (!self.hasBeenValidated) {
        NSError *error;
        id validatedValue = [self.textView.text copy];
        
        _valid = [self.observedObject validateValue:&validatedValue
                                             forKey:self.key
                                              error:&error];
        if (_valid) {
            self.textView.text = validatedValue;
            [self.observedObject setValue:validatedValue forKey:self.key];
        }
        [self setValidationFeedback:_valid];
        self.hasBeenValidated = YES;
    }
    return _valid;
}


- (void)setValidationFeedback:(BOOL)isValid
{
    if (self.validationDelegate && [self.validationDelegate respondsToSelector:@selector(textViewConnector:isValid:)]) {
        [self.validationDelegate textViewConnector:self.textView isValid:isValid];
    } else {
        if (isValid) {
            self.textView.layer.borderWidth = 0;
        } else {
            self.textView.layer.borderWidth = 1;
            self.textView.layer.borderColor = [UIColor redColor].CGColor;
        }
    }
}

- (void)cancelEdit
{
    [self.textView resignFirstResponder];
}
@end