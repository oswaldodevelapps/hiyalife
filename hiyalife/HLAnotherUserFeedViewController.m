//
//  HLAnotherUserFeedViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 13/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLAnotherUserFeedViewController.h"
#import "UIImageView+AFNetworking.h"
#import "HLOtherUser.h"
#import "HLDataStore.h"

@interface HLAnotherUserFeedViewController ()
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIButton *userButton;

@end

@implementation HLAnotherUserFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setCurrentUser:(HLUser *)currentUser
{
    [super setCurrentUser:currentUser];
  
    [self updateAnotherUserPhoto];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    

  
  
}
-(void)updateAnotherUserPhoto{
     HLOtherUser *user = (HLOtherUser *)self.currentUser;
    [HLDataStore downloadImageWithURL:user.photoURL success:^(UIImage *image) {
        [self.userButton setImage:image forState:UIControlStateNormal];
    } ] ;
}

 

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
     
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    NSLog(@"Deallocating %@",NSStringFromClass([self class]));
 
    
  
}

@end
