//
//  RBFormViewController.m
//  KVOField
//
//  Created by Rafa Barberá Córdoba on 02/05/13.
//  Copyright (c) 2013 Rafa Barbera. All rights reserved.
//

#import "RBFormViewController.h"
#import "RBTextFieldConnector.h"
#import "RBTextViewConnector.h"
#import "RBSimplePickerConnector.h"
#import "RBDatePickerConnector.h"
#import "RBBasePickerConnector.h"

@interface RBFormViewController () <UITextFieldDelegate, UITextViewDelegate, RBTextFieldConnectorDelegate, RBTextViewConnectorDelegate>

// TextField managment
@property (nonatomic,strong) NSMutableArray *textFieldConnectors;

// TextView managment
@property (nonatomic,strong) NSMutableArray *textViewConnectors;

// Picker managment
@property (nonatomic,strong) NSMutableDictionary *pickerConnectors;
@property (nonatomic,weak) UIPickerView *activePicker;
@property (nonatomic,strong) UIDatePicker *datePicker;

// Keyboard managment
@property (nonatomic) BOOL isKeyboardVisible;
@property (nonatomic) CGFloat inputViewHeight;

// Other inputView management
@property (nonatomic) BOOL isInputViewVisible;
@property (weak, nonatomic) UIView *inputView;

@end

@implementation RBFormViewController

- (void)viewDidLoad
{
    [self registerForKeyboardNotifications];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.fieldsScrollerView.contentSize = self.fieldsScrollerView.bounds.size;
    self.fieldsScrollerView.scrollEnabled = NO;

}

#pragma mark - UITextField managment

- (void)connectTextField:(UITextField *)textField forKey:(NSString *)key
{
    NSAssert(self.modelObject, @"You need to connect a modelObject before you can connect the fields");
    RBTextFieldConnector *connector = [RBTextFieldConnector fieldWithTextField:textField observingObject:self.modelObject forKey:key];
    connector.delegate = self;
    connector.validationDelegate = self;
    [self.textFieldConnectors addObject:connector];
}


- (NSMutableArray *)textFieldConnectors
{
    if (!_textFieldConnectors) _textFieldConnectors = [NSMutableArray array];
    return _textFieldConnectors;
}


- (void)cancelInput
{
    if (self.isKeyboardVisible) [self.view endEditing:YES];
    if (self.isInputViewVisible) [self dismissInputViewAnimated:YES completion:nil];
}

- (BOOL)validateTextFields
{
    __block BOOL isValid = YES;
    [self.textFieldConnectors enumerateObjectsUsingBlock:^(RBTextFieldConnector *field, NSUInteger idx, BOOL *stop) {
        NSLog(@"valid textfield %@  %d -> %@",field,field.isValid,field.textField.text);
        isValid &= field.isValid;
    }];
    [self.textViewConnectors enumerateObjectsUsingBlock:^(RBTextViewConnector *view, NSUInteger idx, BOOL *stop) {
        isValid &= view.isValid;
    }];
    return isValid;
}

#pragma mark - UITextView managment

- (void)connectTextView:(UITextView *)textView forKey:(NSString *)key
{
    NSAssert(self.modelObject, @"You need to connect a modelObject before you can connect the fields");
    RBTextViewConnector *connector = [RBTextViewConnector fieldWithTextView:textView observingObject:self.modelObject forKey:key];
    connector.delegate = self;
    connector.validationDelegate = self;
    [self.textViewConnectors addObject:connector];
}

- (NSMutableArray *)textViewConnectors
{
    if (!_textViewConnectors) _textViewConnectors = [NSMutableArray array];
    return _textViewConnectors;
}


#pragma mark - Pickers managment

- (void)connectSimplePickerWithOptions:(NSArray *)options toTextField:(UITextField *)textField forKey:(NSString *)key
{
    NSAssert(self.modelObject, @"You need to connect a modelObject before you can connect the fields");
    RBSimplePickerConnector *connector = [RBSimplePickerConnector simplePickerConnectorWithOptions:options
                                                                                   targetTextField:textField
                                                                                   observingObject:self.modelObject
                                                                                            forKey:key];
    UIButton *fireButton = [self overlayButtonWithAction:@selector(showPickerWithButton:) onTopOfView:textField];
    [self.pickerConnectors setObject:connector forKey:[NSValue valueWithPointer:(__bridge const void *)(fireButton)]];
}


- (void)connectDatePickerWithFormat:(NSString *)format toTextField:(UITextField *)textField forKey:(NSString *)key
{
    NSAssert(self.modelObject, @"You need to connect a modelObject before you can connect the fields");
    RBDatePickerConnector *connector = [RBDatePickerConnector datePickerConnectorWithFormat:format
                                                                            targetTextField:textField
                                                                            observingObject:self.modelObject
                                                                                     forKey:key];
    UIButton *fireButton = [self overlayButtonWithAction:@selector(showPickerWithButton:) onTopOfView:textField];
    [self.pickerConnectors setObject:connector forKey:[NSValue valueWithPointer:(__bridge const void *)(fireButton)]];
}
-(void)voidMethod{
    
}
- (void)connectDatePickerWithFormatandMaxDate:(NSString *)format withMaxDate:(NSDate *)maximumDate toTextField:(UITextField *)textField forKey:(NSString *)key{
    NSAssert(self.modelObject, @"You need to connect a modelObject before you can connect the fields");
    RBDatePickerConnector *connector = [RBDatePickerConnector datePickerConnectorWithFormat:format
                                            withMaxDate:maximumDate 
                                                                            targetTextField:textField
                                                                            observingObject:self.modelObject
                                                                                     forKey:key];
    UIButton *fireButton = [self overlayButtonWithAction:@selector(showPickerWithButton:) onTopOfView:textField];
    [self.pickerConnectors setObject:connector forKey:[NSValue valueWithPointer:(__bridge const void *)(fireButton)]];
}


- (void)showPickerWithButton:(UIButton *)sender
{
    id<RBBasePickerConnector> connector = [self.pickerConnectors objectForKey:[NSValue valueWithPointer:(__bridge const void *)(sender)]];
    if (connector)
    {
        self.activeView = sender;
        self.activePicker = [connector getPicker];
        [connector updateWithModel];
        [self presentInputView:self.activePicker animated:YES completion:nil];
    }
}

- (void)setActivePicker:(UIPickerView *)activePicker
{
    if (_activePicker!=activePicker) {
        _activePicker = activePicker;
        if ([[self.view subviews] indexOfObject:_activePicker]==NSNotFound) {
            [self.view addSubview:_activePicker];
        }
    }
    [self moveToStartPositionView:_activePicker];
}

- (NSMutableDictionary *)pickerConnectors
{
    if (!_pickerConnectors) _pickerConnectors = [NSMutableDictionary dictionary];
    return _pickerConnectors;
}


#pragma mark - Inicialization

- (void)setModelObject:(NSObject *)modelObject
{
    if(modelObject == _modelObject) return;
    
    _modelObject = modelObject;
    [self.textFieldConnectors enumerateObjectsUsingBlock:^(RBTextFieldConnector *fld, NSUInteger idx, BOOL *stop) {
        [fld changeModelObject:modelObject];
    }];
    
    [self.textViewConnectors enumerateObjectsUsingBlock:^(RBTextViewConnector *tView, NSUInteger idx, BOOL *stop) {
        [tView changeModelObject:modelObject];
    }];
    
    [self.pickerConnectors enumerateKeysAndObjectsUsingBlock:^(id key, id<RBBasePickerConnector> picker, BOOL *stop) {
        [picker changeModelObject:modelObject];
    }];
}

#pragma mark - UITextFieldViewDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeView = textField;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.activeView = textView;
}

#pragma mark - Keyboard notifications
// Call this method somewhere in your view controller setup code.

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if (self.isInputViewVisible) [self dismissInputViewAnimated:YES completion:nil];
    
    self.isKeyboardVisible = YES;
    self.inputViewHeight = kbSize.height;
    self.fieldsScrollerView.scrollEnabled = YES;
    [self scrollContentToEludeInputView];
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self changedActiveView:nil];
    if (!self.inputView) {
        [UIView animateWithDuration:0.3f animations:^{
            UIEdgeInsets contentInsets = UIEdgeInsetsZero;
            self.fieldsScrollerView.contentInset = contentInsets;
            self.fieldsScrollerView.scrollIndicatorInsets = contentInsets;
            self.fieldsScrollerView.scrollEnabled = NO;
        }];
    }
    self.isKeyboardVisible = NO;
}

- (void)scrollContentToEludeInputView
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, self.inputViewHeight, 0.0);
    self.fieldsScrollerView.contentInset = contentInsets;
    self.fieldsScrollerView.scrollIndicatorInsets = contentInsets;
    self.fieldsScrollerView.indicatorStyle = UIScrollViewIndicatorStyleWhite;

    CGPoint bottomPoint = [self computeBottomPointForView:self.activeView];

    CGFloat delta = MAX(0,bottomPoint.y-(self.fieldsScrollerView.bounds.size.height-self.inputViewHeight));
    [self.fieldsScrollerView setContentOffset:CGPointMake(0, delta) animated:YES];
}

- (CGPoint)computeBottomPointForView:(UIView *)view
{
    CGPoint bottomPoint = view.frame.origin;
    bottomPoint.y += view.bounds.size.height;
    return bottomPoint;
}

- (void)setActiveView:(UIView *)activeView
{
    NSAssert(activeView!=nil, @"You shouldent assign nil to activeView");
    _activeView = activeView;
    [self changedActiveView:_activeView];
    if (_activeView && self.isKeyboardVisible) [self scrollContentToEludeInputView];
}

- (void)changedActiveView:(UIView *)view
{
    // You can use this method to see if the form is valid for summision and show/hide a button
}


#pragma mark - Input Views

- (void)presentInputView:(UIView *)inputView animated:(BOOL)animated completion:(void(^)(BOOL finished))completionBlock
{
   if (self.inputView!=inputView) {
       if (self.isInputViewVisible) [self visualDismissInputViewAnimated:animated completion:completionBlock];

       self.inputView = inputView;
       if (self.isKeyboardVisible) [self.view endEditing:YES];
       

       self.inputView.hidden = NO;
       self.inputViewHeight = self.inputView.bounds.size.height;
       [UIView animateWithDuration:(animated)?0.3f:0.0
                        animations:^{
                             self.inputView.frame = CGRectOffset(self.inputView.frame, 0, -self.inputView.frame.size.height);
                             [self scrollContentToEludeInputView];
                         }
                        completion:^(BOOL finished) {
                            self.isInputViewVisible = YES;
                            if (completionBlock) completionBlock(finished);
        }];
    }
}

- (void)dismissInputViewAnimated:(BOOL)animated completion:(void(^)(BOOL finished))completionBlock
{
    if (!self.isInputViewVisible) return;
    [self changedActiveView:nil];
    [self visualDismissInputViewAnimated:animated
                              completion:^(BOOL finished) {
                                  self.isInputViewVisible = NO;
                                  self.inputView.hidden = YES;
                                  self.inputView = nil;
                              }];
}

// Only animation, no dataflow values touched
- (void)visualDismissInputViewAnimated:(BOOL)animated completion:(void(^)(BOOL finished))completionBlock
{
    [UIView animateWithDuration:(animated)?0.3f:0
                     animations:^{
                         UIEdgeInsets contentInsets = UIEdgeInsetsZero;
                         self.fieldsScrollerView.contentInset = contentInsets;
                         self.fieldsScrollerView.scrollIndicatorInsets = contentInsets;
                         self.inputView.frame = CGRectOffset(self.inputView.frame, 0, self.inputView.frame.size.height);
                     }
                     completion:completionBlock];
}

#pragma mark - Utilities

- (UIButton *)overlayButtonWithAction:(SEL)action onTopOfView:(UIView *)view
{
    UIButton *overlay = [UIButton buttonWithType:UIButtonTypeCustom];
    overlay.frame = view.frame;
    [overlay addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [view.superview insertSubview:overlay aboveSubview:view];
    return overlay;
}


- (void)moveToStartPositionView:(UIView *)view
{
    CGSize controllerSize = self.view.bounds.size;
    CGSize viewSize = view.bounds.size;
    
    view.frame = CGRectMake(0, controllerSize.height, viewSize.width, viewSize.height);
    view.hidden = YES;
}


@end
