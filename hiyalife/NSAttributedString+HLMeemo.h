//
//  NSAttributedString+HLMeemo.h
//  hiyalife
//
//  Created by Juan Hontanilla on 11/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HLMeemo.h"

@interface NSAttributedString (HLMeemo)
+(NSAttributedString *)dateAndWhereAttributedDescriptionWithMeemo:(HLMeemo *)meemo;
@end
