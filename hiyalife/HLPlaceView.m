//
//  HLPlaceView.m
//  hiyalife
//
//  Created by Oswaldo on 08/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLPlaceView.h"
@interface HLPlaceView()
@property (nonatomic,strong) UILabel *placeCaptionLabel;

@end
@implementation HLPlaceView


-(UILabel *)placeCaptionLabel{
    if(!_placeCaptionLabel){
        _placeCaptionLabel= [[UILabel alloc]initWithFrame:CGRectMake(2, 2, 200, 18)];
        [_placeCaptionLabel setFont:[UIFont fontWithName:@"Tauri" size:10.0]];
        _placeCaptionLabel.textColor = [UIColor whiteColor];
        _placeCaptionLabel.backgroundColor = [UIColor clearColor];
    }
    return _placeCaptionLabel;
}
- (id)init
{
    self = [super initWithFrame:CGRectZero];
    if(self){
        [self setup];
    }
    
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

-(id)initWithPlace:(HLPlace *)place {
    self = [super init];
    if(self){
        self.place = place;
        [self setup];
    }
    return self;
}
- (void)setup {
    
    [self setUserInteractionEnabled:YES];
 
    [self.placeCaptionLabel setText:self.place.caption];
    
    [self addSubview:self.placeCaptionLabel];
 
    
    CGSize labelSize = [self.place.caption sizeWithFont:[UIFont fontWithName:@"Tauri" size:14.0]];
    
    CGRect totalFrame = CGRectMake(0, 0, labelSize.width  , 20);
    
    
    [self setFrame:totalFrame];
}

@end
