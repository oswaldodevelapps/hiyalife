//
//  HLEditingMeemo.m
//  hiyalife
//
//  Created by Oswaldo on 15/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLEditingMeemo.h"
#import "NSDate+HLDateFormatter.h"
#import "HLDataStore.h"

@implementation HLEditingMeemo

@synthesize editingPhotos=_editingPhotos, editingCaptions=_editingCaptions;

-(NSString *)description{
    return [NSString stringWithFormat:@"Meemo Draft (tile=%@, text=%@, place=%@\r\r\r",self.title,self.text,self.editingPlace];
}

-(NSArray *)editingPhotos{
    if(!_editingPhotos)_editingPhotos = [[NSArray alloc]init];
    return _editingPhotos;
}
-(NSArray *)editingCaptions{
    if(!_editingCaptions)_editingCaptions = [[NSArray alloc]init];
    return _editingCaptions;
}

-(NSArray *)daysForCurrentDateFrom{
    if(!_daysForCurrentDateFrom)
        _daysForCurrentDateFrom =  [NSDate getDaysForMonth:self.fromDateMonth.intValue andYear:self.fromDateYear.intValue];
    return _daysForCurrentDateFrom;
 
} 

+(HLEditingMeemo *)editingMeemoWithDictionary:(NSDictionary *)dict{
    HLEditingMeemo *memo = [[HLEditingMeemo alloc]init];
    memo.text = [dict valueForKey:@"text"];
    memo.title = [dict valueForKey:@"title"];
    memo.fromDateYear = [dict valueForKey:@"fromDateYear"];
    memo.fromDateMonth = [dict valueForKey:@"fromDateMonth"];
    memo.fromDateDay = [dict valueForKey:@"fromDateDay"];
    memo.toDateYear = [dict valueForKey:@"toDateYear"];
    memo.toDateMonth = [dict valueForKey:@"toDateMonth"];
    memo.toDateDay = [dict valueForKey:@"toDateDay"];
    
    NSMutableArray *mutablePhotos = [[NSMutableArray alloc]init];
    for(NSString *photoPath in [dict valueForKey:@"editingPhotos"]){
 
        [mutablePhotos addObject:[UIImage imageWithContentsOfFile:photoPath]];
        
    }
    memo.editingPhotos = mutablePhotos;
  
    memo.editingCaptions = [dict valueForKey:@"editingCaptions"];
    
    if(memo.json){
        [memo.json addEntriesFromDictionary:dict];

    }else{
        
        memo.json = [dict mutableCopy];
    }
    
    return memo;
}

-(BOOL)inDraftState{
    return [[self.json allKeys] count] > 0;
}
-(NSMutableDictionary *)json{
    if(!_json) _json = [[NSMutableDictionary alloc]init];
    
    return _json;
}

#pragma mark - Setters
-(void)setText:(NSString *)text{
    _text = text;
    [self.json setValue:text forKey:@"text"];
    
}

-(void)setTitle:(NSString *)title
{
    _title = title;
    [self.json setValue:title forKey:@"title"];
}

-(void)setFromDateYear:(NSString *)fromDateYear{
    _fromDateYear = fromDateYear;
    [self.json setValue:fromDateYear forKey:@"fromDateYear"];
}

-(void)setFromDateMonth:(NSString *)fromDateMonth{
    _fromDateMonth = fromDateMonth;
    [self.json setValue:fromDateMonth forKey:@"fromDateMonth"];
}
-(void)setFromDateDay:(NSString *)fromDateDay{
    _fromDateDay = fromDateDay;
    [self.json setValue:fromDateDay forKey:@"fromDateDay"];
}

-(void)setToDateDay:(NSString *)toDateDay{
    _toDateDay = toDateDay;
    [self.json setValue:toDateDay forKey:@"toDateDay"];
}
-(void)setToDateMonth:(NSString *)toDateMonth{
    _toDateMonth = toDateMonth;
    [self.json setValue:toDateMonth forKey:@"toDateMonth"];
}
-(void)setToDateYear:(NSString *)toDateYear{
    _toDateYear = toDateYear;
    [self.json setValue:toDateYear forKey:@"toDateYear"];
}
-(void)setEditingCaptions:(NSArray *)editingCaptions
{
    _editingCaptions = editingCaptions;
    [self.json setValue:editingCaptions forKey:@"editingCaptions"];
}
-(void)setEditingPhotos:(NSArray *)editingPhotos{
    _editingPhotos = editingPhotos;

    
    NSArray *photoPaths = [[HLDataStore sharedInstance]saveDraftPhotos:editingPhotos];
    
    
    
    [self.json setValue:photoPaths forKey:@"editingPhotos"];
}


#pragma mark - KVO Validation Methods
/*
-(BOOL)validatefromDateYear:(id *)ioValue error:(NSError * __autoreleasing *)outError{
    
    if(*ioValue != nil ){
        NSString *value = *ioValue;
        NSUInteger yearValue = value.intValue;
        if(yearValue >0 ) return YES;
        else return NO;
    }
    
    NSLog(@"Is valid email? %@",*ioValue);
    return YES;
}
*/
@end
