//
//  HLProfileViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 05/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLProfileViewController.h"
#import "HLDataStore.h"
#import <QuartzCore/QuartzCore.h>
#import "NSDate+HLDateFormatter.h"

@interface HLProfileViewController () 
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *surnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;

@end

@implementation HLProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    HLMeUser *user= [HLDataStore sharedInstance].currentUser;
    
    NSString *avatarPath = [user userImageArchivePath];
    
    
    self.avatarView.image = [UIImage imageWithContentsOfFile:avatarPath];
    self.nameTextField.text = user.name;
    self.surnameTextField.text = user.surname;
    self.birthdayTextField.text = [user.birthday dateString];
    
}
 


-(void)dealloc
{
    NSLog(@"Deallocating %@",NSStringFromClass([self class]));
}

  
 
  

@end
