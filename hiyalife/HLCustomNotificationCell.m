//
//  HLCustomNotificationCell.m
//  hiyalife
//
//  Created by Oswaldo on 03/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLCustomNotificationCell.h"

@implementation HLCustomNotificationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setTypeNotification:(int)typeNotification{
    
    // Initialization code
    self.textNotificationLabel.font= [UIFont fontWithName:@"Tauri" size:10.0];
    self.textNotificationLabel.textColor     = [UIColor blackColor];
    CGRect rect =  self.textNotificationLabel.frame;
    rect.size.width = 222;
    self.textNotificationLabel.frame = rect;
    self.textNotificationLabel.numberOfLines = 0;
    [self.textNotificationLabel sizeToFit];
    
    self.userNameLabel.font= [UIFont fontWithName:@"Tauri" size:12.0];
    self.userNameLabel.textColor  = [UIColor lightGrayColor];
    
    self.dateLabel.font= [UIFont fontWithName:@"Tauri" size:12.0];
    self.dateLabel.textColor  = [UIColor lightGrayColor];
    
    
    _typeNotification = typeNotification;
    
    switch (typeNotification) {
            /*
            [02/07/13 10:44:35] Alfonso Fernandez Roca: 0 -> share. Alguien te ha compartido una meemo
            [02/07/13 10:45:06] Alfonso Fernandez Roca: 1 -> user2user. Mensaje de usuario a usuario( no se usan de momento )
            [02/07/13 10:45:27] Alfonso Fernandez Roca: 2 -> meemochange. Alguien ha modificado o añadido contenido en una memo tuya
            [02/07/13 10:45:44] Alfonso Fernandez Roca: 3. acceptmeemo. Un usuario ha aceptado una meemo tuya
            [02/07/13 10:46:05] Alfonso Fernandez Roca: 4. server2user. Notificación del sistema al usuario( no se usan de momento )
             */
        case 0:
            self.notificationTypeLabel.textColor = [UIColor colorWithRed:121/255.0f green:217/255.0f blue:166/255.0f alpha:1.0];
            break;
        case 1:
            self.notificationTypeLabel.textColor =  [UIColor whiteColor];
            break;
        case 2:
            self.notificationTypeLabel.textColor = [UIColor colorWithRed:116/255.0f green:206/255.0f blue:207/255.0f alpha:1.0]; 
            break;
        case 3:
            self.notificationTypeLabel.textColor = [UIColor colorWithRed:116/255.0f green:206/255.0f blue:207/255.0f alpha:1.0] ;
            break;
        case 4:
            self.notificationTypeLabel.textColor = [UIColor whiteColor]; ;
            break;
            
        default:
            break;
    }
}

@end
