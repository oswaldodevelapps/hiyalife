//
//  HLTakeSelectPhotoHelper.m
//  hiyalife
//
//  Created by Oswaldo on 12/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLTakeSelectPhotoHelper.h"
#import "UIImage+ImageResizer.h"


@interface HLTakeSelectPhotoHelper()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong,nonatomic) UIImagePickerController *imagePicker;

@property(strong,nonatomic) UIViewController *currentViewController;

@property(nonatomic) BOOL mustResizePhoto;
@property (nonatomic) CGFloat maxWidthSize;
@end

@implementation HLTakeSelectPhotoHelper

-(UIImagePickerController *)imagePicker{
    if(!_imagePicker){
        _imagePicker = [[UIImagePickerController alloc] init];
    }
    return _imagePicker;
}
-(void)takeOrSelectPhotoFromViewController:(UIViewController *)viewController withMaxWidthSize:(CGFloat)maxWidthSize{
    self.mustResizePhoto = YES;
    self.maxWidthSize = maxWidthSize;
    [self takeOrSelectPhotoFromViewController:viewController];
    
}
-(void)takeOrSelectPhotoFromViewController:(UIViewController *)viewController{
    self.mustResizePhoto = NO;
    self.currentViewController = viewController;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take a photo",@"Select a photo",nil];
    
    
    [actionSheet showInView:self.currentViewController.view];
}


#pragma mark - UIActionSheetDelegate

#define TAKE_PHOTO_INDEX 0
#define SELECT_PHOTO_INDEX 1
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex != actionSheet.cancelButtonIndex){
        switch (buttonIndex) {
            case TAKE_PHOTO_INDEX:
                if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                    [self.imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                }else{
                    [self.imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                }
                break;
            case SELECT_PHOTO_INDEX:
                
                [self.imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                
                break;
            default:
                break;
        }
        [self.imagePicker setDelegate:self];
        self.imagePicker.allowsEditing = YES;
        
        
        [self.currentViewController presentViewController:self.imagePicker animated:YES completion:nil];
        
    }
    
}

#pragma mark - UIImagePickerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    if(self.mustResizePhoto){
       image = [UIImage imageWithImage:image scaledProportionalToMaxWidth:self.maxWidthSize];
    }
    
    
    if([self.delegate respondsToSelector:@selector(imageWasTaken:)]){
        [self.delegate imageWasTaken:image];
    }
         
    [self.currentViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
