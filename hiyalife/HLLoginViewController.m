//
//  HLLoginViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLLoginViewController.h"
#import "HLDataStore.h"
#import "HLFacebookHelper.h"
#import "SVProgressHUD.h"
#import "HLHomeViewController.h"

@interface HLLoginViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;

@end

@implementation HLLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
 
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Loading %@",NSStringFromClass([self class]));
 
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSingleTap:)];
    
    [self.view addGestureRecognizer:tap];
    
    self.navigationItem.titleView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
 
    
    self.usernameTextField.text = @"ullemopp-328@yopmail.com";
    //self.usernameTextField.text = @"oswaldo@develapps.es";
    //self.usernameTextField.text = @"pablo@mensamatic.com";
    self.passwordTextField.text = @"1234";
     
 
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

 


#pragma mark - Actions

- (IBAction)loginAction:(UIButton *)sender {
    BOOL loginOk = YES;
    
    if([self.usernameTextField.text length] ==0){
        [self.usernameTextField setPlaceholder:@"Set a email"];
        loginOk = NO;
    }
    else{
        if(![HLDataStore  validateEmail:self.usernameTextField.text]){
            self.usernameTextField.text = @"";
           [self.usernameTextField setPlaceholder:@"Invalid email"];
            loginOk = NO;
        }
        
    }
    if([self.passwordTextField.text length] == 0){
        [self.passwordTextField setPlaceholder:@"Input password"];
        loginOk = NO;
    }
    
    
    if(loginOk){
    
        
        
        [SVProgressHUD showWithStatus:@"Authenticating..." maskType:SVProgressHUDMaskTypeGradient];
        
        [[HLDataStore sharedInstance] getLoginOkWithUser:self.usernameTextField.text andPassword:self.passwordTextField.text returnBlock:^(HLMeUser *user, NSError *error) {
            if(user){
                self.usernameTextField.text = @"";
                self.passwordTextField.text = @"";
                
                //update user info with Profile 
                [[HLDataStore sharedInstance] updateMeProfileDataForUser:user returnBlock:^(HLMeUser *user, NSError *error) {
                    if(user && !error){
                        [SVProgressHUD showSuccessWithStatus:@"Welcome to Hiyalife!"];
                        
                        [self dismissViewControllerAnimated:NO completion:^{
                            
                        }];
                    }else{
                        [SVProgressHUD showErrorWithStatus:@"Server Error. Login again."];
                    }
                  
                }];
                
         
                
            }else{
                [SVProgressHUD showErrorWithStatus:@"Invalid email or password"];
            }
            
            
        } ];
    }

    
}
- (IBAction)loginWithFacebookAction:(UIButton *)sender {
    
    self.facebookButton.enabled = NO;
    [[HLFacebookHelper sharedInstance] getFacebookToken:^(NSString *token) {
        
        if(token){
            
            [SVProgressHUD showWithStatus:@"Authenticating..."  maskType:SVProgressHUDMaskTypeGradient];
            
            [[HLDataStore sharedInstance] getLoginWithFacebookToken:token returnBlock:^(HLMeUser *user, NSError *error) {
                 [SVProgressHUD dismiss];
                 self.facebookButton.enabled = YES;
                if(user){
                    
                  
                    /*
                     //TEST PROFILE FACEBOOK
                    [[HLDataStore sharedInstance] updateMeProfileDataForUser:user returnBlock:^(HLMeUser *user, NSError *error) {
                        
                        NSLog(@"Recovered profile from Facebook user %@",user);
                        
                        
                            [self performSegueWithIdentifier:@"completeProfileFacebookSegue" sender:self];
                    }];
                    // TEST PROFILE FACEBOOK
                     */
               
                    [SVProgressHUD showSuccessWithStatus:@"Welcome to Hiyalife!"];
                    
                    [self dismissViewControllerAnimated:NO completion:^{
                        
                    }]; 
                   
                    
                }else{
                    
                    [[HLDataStore sharedInstance] joinUserWithFacebookToken:token returnBlock:^(HLMeUser *user, NSError *error) {
                       
                        if(user){
                            //update user info with Profile
                            [[HLDataStore sharedInstance] updateMeProfileDataForUser:user returnBlock:^(HLMeUser *user, NSError *error) {
                                [SVProgressHUD showSuccessWithStatus:@"Just complete your profile"];
                                [self performSegueWithIdentifier:@"completeProfileFacebookSegue" sender:self];
                                
                            }];
                            
                        }
                        else{
                            [SVProgressHUD showErrorWithStatus:@"Facebook Access Error"];
                        }

                        
                    }];
                    
                    
          
                }
            }];
            
            
        }else{
            [SVProgressHUD showErrorWithStatus:@"Facebook Access Error. Setup your account in Settings/Facebook"];
            self.facebookButton.enabled = YES;
        }
 
        
    }];
}
 

-(void)dealloc
{
    NSLog(@"Deallocating %@",NSStringFromClass([self class]));
}

@end
