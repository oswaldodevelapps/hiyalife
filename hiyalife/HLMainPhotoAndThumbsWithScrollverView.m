//
//  HLMainPhotoAndThumbsWithScrollverView.m
//  hiyalife
//
//  Created by Juan Hontanilla on 11/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLMainPhotoAndThumbsWithScrollverView.h"
#import "RBHorizontalImageScrollerView.h"
#import "UIImageView+AFNetworking.h"
#import "UIButton+HLOverlay.h"

@interface HLMainPhotoAndThumbsWithScrollverView()<RBHorizontalImageScrollerViewDelegate>

@property (strong, nonatomic)  UIImageView *mainPhotoView;
@property (strong, nonatomic)  RBHorizontalImageScrollerView *thumbView;
 
@end

@implementation HLMainPhotoAndThumbsWithScrollverView


-(UIImageView *)mainPhotoView
{
    if(!_mainPhotoView){
        _mainPhotoView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height*0.6f)];
   
        _mainPhotoView.clipsToBounds = YES;
        
        _mainPhotoView.backgroundColor = [UIColor yellowColor];
    
        [_mainPhotoView setContentMode:UIViewContentModeScaleAspectFill];
 
         
        
       
    }
    
    return _mainPhotoView;
}


-(RBHorizontalImageScrollerView *)thumbView
{
    if(!_thumbView){
        _thumbView = [[RBHorizontalImageScrollerView alloc]initWithFrame:CGRectMake(0, self.frame.size.height*0.6f, self.frame.size.width, self.frame.size.height*0.4f)];
        _thumbView.delegate = self;
    }
 
    return  _thumbView;
    
}

-(void)setActivePage:(int)pageIndex{
    [self.thumbView setActivePage:pageIndex];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
-(void)setup{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.mainPhotoView];
    [self addSubview:self.thumbView];


    [UIButton overlayButtonWithAction:@selector(showPhoto:) onTarget:self onTopOfView:_mainPhotoView];
}

-(void)setStepIndicator:(UIPageControl *)stepControl{
    self.thumbView.stepIndicator = stepControl;
}



- (void)setImagesThumnails:(NSArray *)images{
    
    NSLog(@"Set %d photos",[images count]);
    
    //full scroller
    if([images count] < 5 ){
        [self.thumbView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self.mainPhotoView setFrame:CGRectZero];
        [self.thumbView setImagesURL:images withGap:2.0f withVerticalGap:4.0f inGroupsOf:1];
       
        
    }
    //half size scroller
    else if([images count]>0){
        [self.mainPhotoView setImageWithURL:[images objectAtIndex:0]];
        NSMutableArray * nextPhotos = [images mutableCopy] ;
        [nextPhotos removeObjectAtIndex:0];
        if([nextPhotos count] > 0 ){
            [self.thumbView setImagesURL:nextPhotos withGap:2.0f withVerticalGap:4.0f inGroupsOf:4];
        }
    }
 
    
}

 
- (void)showPhoto:(UIButton *)sender{
    
    if(self.delegate){
        [self.delegate photoWasSelected:0];
    }
    
}


#pragma mark - RBHorizontalImageScrollerViewDelegate
-(void)imageWasSelected:(NSUInteger)selectedIndex
{
     
    if(self.delegate){
        [self.delegate photoWasSelected:selectedIndex + (CGRectIsEmpty(self.mainPhotoView.frame) ? 0:1)];
    }
}

@end
