//
//  RBImageHorizontalScroller.h
//  TutorialWizard
//
//  Created by Rafa Barberá Córdoba on 04/06/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RBHorizontalImageScrollerViewDelegate  <NSObject>

-(void)imageWasSelected:(NSUInteger)
selectedIndex;

@end

@interface RBHorizontalImageScrollerView : UIView
@property (nonatomic)BOOL allowsInteraction;
@property (nonatomic,assign) id<RBHorizontalImageScrollerViewDelegate> delegate;
@property (nonatomic, strong) UIPageControl *stepIndicator;
- (void)setImages:(NSArray *)images withGap:(CGFloat)gapWidth withVerticalGap:(CGFloat)gapHeight inGroupsOf:(int)count;
- (void)setImagesURL:(NSArray *)URLs withGap:(CGFloat)gapWidth withVerticalGap:(CGFloat)gapHeight inGroupsOf:(int)count;
-(void)setImagesURL:(NSArray *)URLs withGap:(CGFloat)gapWidth withVerticalGap:(CGFloat)gapHeight inGroupsOf:(int)count withContentMode:(UIViewContentMode)contentMode; 

- (void)setImages:(NSArray *)images withGap:(CGFloat)gapWidth;
- (void)setImagesURL:(NSArray *)URLs placeholder:(UIImage *)placeholder withGap:(CGFloat)gapWidth;
-(void)setActivePage:(int)index;

-(void)navigateToImageIndex:(NSUInteger)index animated:(BOOL)animated;
@end
