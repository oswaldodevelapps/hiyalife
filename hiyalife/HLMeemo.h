//
//  HLMeemo.h
//  hiyalife
//
//  Created by Juan Hontanilla on 10/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HLPlace.h"
 
@interface HLMeemo : NSObject
+(HLMeemo *)makeMeemoWithDictionary:(NSDictionary *)dict;
@property (nonatomic,strong) NSDictionary *json;
@property (nonatomic, strong) NSString *idMeemo;
@property (nonatomic,strong) NSDate *date;
@property (nonatomic,strong) NSString *dateString;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic,strong)NSMutableDictionary *owner;
@property (nonatomic, strong) NSString *place;
@property (nonatomic, assign) HLMeemoPrivacyType privacy;
@property (nonatomic, strong) UIColor *backgroundColor;
@property(nonatomic) BOOL isInMyTimeLine;
@property (nonatomic,strong) NSString *chapterName;
@property (nonatomic,strong) NSMutableDictionary *photosUImages;
@property(nonatomic,strong) NSArray *shareList;


//edit propertiess
@property (nonatomic,strong) NSMutableArray *editingPhotos;
@property (nonatomic,strong) HLPlace *editingPlace;


-(NSURL *)getPhotoThumbUrl;
-(NSArray *)getNextFivePhotoThumbUrl;
-(NSArray *)getAllPhotosUIImage;
-(NSArray *)getAllPhotosNSURL;

-(NSArray *)getAllPhotosOriginalSizeNSURL;
-(NSString *)dateDescription;
-(NSString *)dateAndWhereDescription;
 
-(HLMeemoPreferredLayout)preferredLayout;

-(BOOL)isOwnedByMe;
 

 
@end
