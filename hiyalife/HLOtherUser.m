//
//  HLOtherUser.m
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLOtherUser.h"

@implementation HLOtherUser
+(HLOtherUser *)makeUserWithDictionary:(NSDictionary *)dict{
    HLOtherUser *user = [[HLOtherUser alloc] init];
    
    user.hiyaUserID = [[dict valueForKey:@"id"] intValue];
    user.name = [dict valueForKey:@"name"];
    user.photoURL = [dict valueForKey:@"photo"];
    
    if([dict valueForKey:@"added"]){
        user.isAdded = [[dict valueForKey:@"added"] intValue];
    }
    
    return user;
}

-(NSString *)description{
    return [NSString stringWithFormat:@"Other User (name=%@,id=%d,photo=%@",self.name,self.hiyaUserID,self.photoURL];
}

@end
