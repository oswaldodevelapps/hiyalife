//
//  NSString+HLHTMLContent.m
//  hiyalife
//
//  Created by Juan Hontanilla on 24/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "NSString+HLHTMLContent.h"

@implementation NSString (HLHTMLContent)
-(NSString *)embedStringInHTMLBody{
    
    if(self.length > 140){
        return [NSString stringWithFormat:@"<HTML><head><style type=\"text/css\"> \n"
                "body {font-family: \"%@\"; font-size: %@;}\n"
                "</style> \n</head><body>%@</body></HTML>",@"Tauri", @"12" ,self];
    }
    else{
        return [NSString stringWithFormat:@"<HTML><head><style type=\"text/css\"> \n"
                "body {font-family: \"%@\"; font-size: %@;}\n"
                "</style> \n</head><body><center>%@</center></body></HTML>",@"Tauri",  @"18",self];
    }
    

}
@end
