//
//  HLShareCustomCell.h
//  hiyalife
//
//  Created by Juan Hontanilla on 25/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLShareCustomCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
