//
//  RBBasePickerConnector.h
//  MeetQute
//
//  Created by Rafa Barberá Córdoba on 10/05/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RBBasePickerConnector <NSObject>

- (id)getPicker;
- (void)updateWithModel;
- (void)changeModelObject:(NSObject *)newModelObject;

@end
