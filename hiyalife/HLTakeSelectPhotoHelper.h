//
//  HLTakeSelectPhotoHelper.h
//  hiyalife
//
//  Created by Oswaldo on 12/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol HLTakeSelectPhotoHelperDelegate <NSObject>

-(void)imageWasTaken:(UIImage *)image;

@end

@interface HLTakeSelectPhotoHelper : NSObject

 

@property (nonatomic, weak) id<HLTakeSelectPhotoHelperDelegate> delegate;


-(void)takeOrSelectPhotoFromViewController:(UIViewController *)viewController;
-(void)takeOrSelectPhotoFromViewController:(UIViewController *)viewController withMaxWidthSize:(CGFloat)maxWidthSize;
@end
