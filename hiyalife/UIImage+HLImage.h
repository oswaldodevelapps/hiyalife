//
//  UIImage+HLPrivacy.h
//  hiyalife
//
//  Created by Juan Hontanilla on 29/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (HLImage)
+(UIImage *)imageForPrivacy:(HLMeemoPrivacyType)type blackOrWhite:(BOOL)black;
+(UIImage *)paintImage:(UIImage *)image withColor:(UIColor *)color;
@end
