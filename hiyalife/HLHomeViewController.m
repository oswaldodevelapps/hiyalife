//
//  HLHomeViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 10/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLHomeViewController.h"
#import "HLDataStore.h"

@interface HLHomeViewController ()

@end

@implementation HLHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tabBar setBackgroundImage:[UIImage imageNamed:@"footer_menu" ]];
    
 
}


-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
     NSLog(@"Home View controller did Appear");
    
    //any draft of memo present?
    if([[HLDataStore sharedInstance].editingMeemo inDraftState]){
        NSLog(@"Editing memo in draft");
        [[self.tabBar.items objectAtIndex:CREATE_MEMO_TAB_BAR_INDEX] setBadgeValue:@"1"];
    }
}

 
#pragma mark - Actions





-(void)dealloc
{
    NSLog(@"Deallocating %@",NSStringFromClass([self class]));
}

@end
