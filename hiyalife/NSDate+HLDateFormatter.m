//
//  NSDate+HLDateFormatter.m
//  hiyalife
//
//  Created by Juan Hontanilla on 31/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "NSDate+HLDateFormatter.h"



@implementation NSDate (HLDateFormatter)
+(NSArray *)getDaysForMonth:(NSUInteger)month andYear:(NSUInteger)year{
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comps = [[NSDateComponents alloc] init];
    [comps setMonth:month];
    [comps setYear:year];
    
    NSRange range = [cal rangeOfUnit:NSDayCalendarUnit
                              inUnit:NSMonthCalendarUnit
                             forDate:[cal dateFromComponents:comps]];
    
    NSMutableArray * days = [[NSMutableArray alloc] init];
    for(int i = 1;i<range.length;i++){
        [days addObject:[NSString stringWithFormat:@"%02d",i]];
    }
    return days;

}
+(NSUInteger)getIndexForMonthName:(NSString *)monthName{
    NSArray *arrayMonths = [self getMonthNames];
    return [arrayMonths indexOfObject:monthName];
}
+(NSArray *)getMonthNames{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSMutableArray * months = [[NSMutableArray alloc] init];
    
    for(int i=0;i<12;i++){
        [months addObject:[[df monthSymbols] objectAtIndex:(i)]];
    }
    return months;
}

+(NSArray *)getYearsArrayFrom:(NSUInteger)fromYear{
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int yearToday  = [[formatter stringFromDate:[NSDate date]] intValue];

    NSMutableArray * years = [[NSMutableArray alloc] init];
    for (int i=fromYear; i<=yearToday; i++) {
        [years addObject:[NSString stringWithFormat:@"%d",i]];
    }
    return years;
}
-(NSString *)dateString{
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:K_FORMATER_DATE];
    return [formatter stringFromDate:self];
}

-(NSString *)dateStringSecondFormat{
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:K_FORMATER_TEXT_DATE;
    return [formatter stringFromDate:self];
}

+(NSDate *)dateFromString:(NSString *)dateString{
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:K_FORMATER_DATE];
    return [formatter dateFromString:dateString];
}

+(NSDate *)getDateForSomeYearsAgo:(NSUInteger)years{
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDate * currentDate = [NSDate date];
    NSDateComponents * comps = [[NSDateComponents alloc] init];
    [comps setYear: -18];
    return [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
}
@end
