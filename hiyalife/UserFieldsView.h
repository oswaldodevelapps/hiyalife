//
//  UserFieldsView.h
//  hiyalife
//
//  Created by Juan Hontanilla on 20/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserFieldsView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *avatarPhoto;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *surnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UITextField *genderTextField;

@end
