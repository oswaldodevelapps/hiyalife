//
//  HLFacebookHelper.m
//  hiyalife
//
//  Created by Juan Hontanilla on 08/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//
#import <Accounts/Accounts.h> 
#import <Social/Social.h>
#import "HLFacebookHelper.h"

@interface HLFacebookHelper()
@property (strong, nonatomic) ACAccountStore *accountStore;
@property (strong, nonatomic) ACAccount *facebookAccount;
@end
@implementation HLFacebookHelper
+(HLFacebookHelper *)sharedInstance{
    static HLFacebookHelper *_instance;
    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[HLFacebookHelper alloc] init];
        }
        return _instance;
    }
}

-(ACAccountStore *)accountStore{
    if(!_accountStore)
        _accountStore = [[ACAccountStore alloc] init];
    return _accountStore;
}



-(void)getFacebookToken:(getTokenBlock)block;

{
 
    ACAccountType *facebookAccountType = [self.accountStore
                                          accountTypeWithAccountTypeIdentifier:
                                          ACAccountTypeIdentifierFacebook];
    
 
    dispatch_async(dispatch_get_global_queue(
                                             DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     
        NSDictionary *facebookOptions = @{
                                          ACFacebookAppIdKey : FACEBOOK_APP_ID,
                                          ACFacebookPermissionsKey : @[@"email",@"user_events",@"read_stream",@"user_photos"],
                                          ACFacebookAudienceKey : ACFacebookAudienceEveryone};
   
        [self.accountStore
         requestAccessToAccountsWithType:facebookAccountType
         options:facebookOptions completion:^(BOOL granted,
                                              NSError *error) {
             
             
          
             if (granted)
             {
                 self.facebookAccount = [[self.accountStore accountsWithAccountType:facebookAccountType]
                                         lastObject];
                 
                 ACAccountCredential *fbCredential = [self.facebookAccount credential];
                 NSString *accessToken = [fbCredential oauthToken];
                 NSLog(@"Facebook Access Token: %@", accessToken);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if(block)
                         block(accessToken);
                 });
              
             }
           
             else
             {
              
                 if (error)
                 {
                     
                     NSLog(@"Facebook Error %@",error);
                     dispatch_async(dispatch_get_main_queue(), ^{
                         if(block)
                             block(nil);
                     });
                 }
                
                 else //el usuario canceló la acción
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         if(block)
                             block(nil);
                     });
                 }
             }
         }];
    });
}


@end
