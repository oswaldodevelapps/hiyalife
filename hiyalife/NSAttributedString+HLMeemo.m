//
//  NSAttributedString+HLMeemo.m
//  hiyalife
//
//  Created by Juan Hontanilla on 11/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "NSAttributedString+HLMeemo.h"


@implementation NSAttributedString (HLMeemo)

+(NSAttributedString *)dateAndWhereAttributedDescriptionWithMeemo:(HLMeemo *)meemo
{
    
 
    
    NSString *dateString = [meemo dateDescription];
    NSString *symbolPlace = meemo.place ? @" \ue724 ":@"";
    
    NSString *placeString = meemo.place ? meemo.place: @"";
    
    NSString *string = [NSString stringWithFormat:@"%@%@%@",dateString,symbolPlace,placeString];
    
    
    NSMutableAttributedString * attrString = [[NSMutableAttributedString alloc] initWithString:string];
    
    // setting the font below makes line spacing become ignored
    [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Tauri" size:10.0] range:NSMakeRange(0, [dateString length])];
    
    if(meemo.place){
        [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"-_icons" size:10.0] range:NSMakeRange([dateString length], [symbolPlace length])];
        
        [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Tauri" size:10.0] range:NSMakeRange([dateString length]+[symbolPlace length] , [placeString length])];
    }
    

    
    return attrString;
    
    
}
@end
