//
//  HLJoin3ViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 13/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLJoinCompleteDataNormalAccountViewController.h"
#import "HLDataStore.h"
#import "SVProgressHUD.h"
#import "NSDate+HLDateFormatter.h"

#import "HLTakeSelectPhotoHelper.h"


@interface HLJoinCompleteDataNormalAccountViewController ()<UITextFieldDelegate,HLTakeSelectPhotoHelperDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *surnameField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayField;
@property (weak, nonatomic) IBOutlet UITextField *genderField;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;



@property (weak, nonatomic) IBOutlet UIButton *finishButton;

@property (strong,nonatomic) HLTakeSelectPhotoHelper *photoHelper;
@property (nonatomic,strong) HLMeUser *currentUser;
@end

@implementation HLJoinCompleteDataNormalAccountViewController

-(HLTakeSelectPhotoHelper *)photoHelper
{
    if(!_photoHelper){
      _photoHelper = [[HLTakeSelectPhotoHelper alloc]init];
        _photoHelper.delegate = self;
    }
    
    return _photoHelper;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentUser = [HLDataStore sharedInstance].currentUser;
    
        self.navigationItem.titleView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSingleTap:)];
    
    [self.view addGestureRecognizer:tap];
    
    
    self.modelObject = [HLDataStore sharedInstance].currentUser;
    
    [self connectTextField:self.nameField forKey:@"name"];
    [self connectTextField:self.surnameField forKey:@"surname"];
 
    
    [self connectDatePickerWithFormatandMaxDate:@"dd/MM/yyyy" withMaxDate:[NSDate getDateForSomeYearsAgo:8] toTextField:self.birthdayField forKey:@"birthday"];
    
    
    [self connectSimplePickerWithOptions:@[@"",@"Male",@"Female"] toTextField:self.genderField forKey:@"gender"];
    
    
    
    [SVProgressHUD showSuccessWithStatus:@"Account validated. Complete your profile to finish." ];
     
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    
    [self cancelInput];
    
}
 

- (CGPoint)computeBottomPointForView:(UIView *)view
{
    if(view == self.genderField || view == self.birthdayField){
        CGPoint bottomPoint = self.finishButton.frame.origin;
        bottomPoint.y += self.finishButton.bounds.size.height+20;
        
        return bottomPoint;
    }
    else return [super computeBottomPointForView:view];
}

#pragma mark - Action

#define MAX_WIDTH_PHOTO 640
- (IBAction)changeAvatarPhotoAction:(id)sender {
    
    [self.photoHelper takeOrSelectPhotoFromViewController:self withMaxWidthSize:MAX_WIDTH_PHOTO];
 
}

#pragma mark - HLTakeSelectPhotoHelperDelegate
-(void)imageWasTaken:(UIImage *)image
{
    NSLog(@"We have the new image %@",image);
    
    
    
    self.currentUser.editingPhoto = image;
    [self.currentUser setAvatarImage:image];
    [self.avatarImageView setImage:image];
 
}


#define MAX_SIZE_FOR_IMAGE 4*1024*1024

#pragma mark - Functionality
- (IBAction)finishAction:(UIButton *)sender {
 
#warning resize the avatar photo!
    
    
    
    [SVProgressHUD showWithStatus:@"Updating profile..." maskType:SVProgressHUDMaskTypeClear];
    
    [[HLDataStore sharedInstance] setMeProfileDataForUser:self.currentUser succesBlock:^(BOOL success) {
        
        if(success){
 
            [SVProgressHUD showSuccessWithStatus:@"Profile Completed. Welcome!"];
 
            [self performSegueWithIdentifier:@"unwindFromLoginID" sender:self];
        }
        else{
           [SVProgressHUD showErrorWithStatus:@"Error setting Profile User Data"];
        }

    }];
 
    
}
 

@end
