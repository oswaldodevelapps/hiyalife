//
//  HLCreateMeemoViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 17/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLCreateMeemoViewController.h"
#import "HLHeaderView.h"
#import "RBHorizontalImageScrollerView.h"
#import "HLMeemo.h"
#import "HLDataStore.h"
#import "SVProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "HLLocationHelper.h"
#import "ORSelectableViewScroller.h"
#import "HLPlace.h"
#import "HLPlaceView.h"
#import "NSDate+HLDateFormatter.h"
#import "HLViewMeemoViewController.h"
#import "UIImage+ImageResizer.h"


@interface HLCreateMeemoViewController ()<UIImagePickerControllerDelegate, UIAlertViewDelegate, UINavigationControllerDelegate,UIActionSheetDelegate, UITextFieldDelegate, CLLocationManagerDelegate,ORSelectableViewScrollerDelegate, UITextViewDelegate>{
    int currentPhotoIndex;
    MKPointAnnotation *annotationPoint ;
}
 

@property (weak, nonatomic) IBOutlet HLHeaderView *headerView;
@property (weak, nonatomic) IBOutlet UIScrollView *containerViewScroller;
 


@property (weak, nonatomic) IBOutlet RBHorizontalImageScrollerView *photosThumbnailScrollView;

@property (strong,nonatomic) UIImagePickerController *imagePicker;
 
 

//current Edit Memo
@property(strong,nonatomic) HLEditingMeemo *editingMeemo;

@property (strong,nonatomic) NSArray *userListOfPlaces;


//container Views
@property (strong,nonatomic) UIView *editPhotoView;
@property (strong,nonatomic) UIView *editTitleView;
@property (strong,nonatomic) UIView *editPlaceView;
@property (strong,nonatomic) UIView *editDateView;
@property (strong,nonatomic) UIView *selectedView;


//image view buttons icons

@property (weak, nonatomic) IBOutlet UIImageView *photoIconView;
@property (weak, nonatomic) IBOutlet UIImageView *titleIconView;
@property (weak, nonatomic) IBOutlet UIImageView *locationIconView;
@property (weak, nonatomic) IBOutlet UIImageView *dateIconView;


//action buttons
@property (weak, nonatomic) IBOutlet UIButton *publishButtonBig;
@property (weak, nonatomic) IBOutlet UIButton *publishButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

//content outlets


@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;
@property (weak, nonatomic) IBOutlet UITextField *titleEditText;
@property (weak, nonatomic) IBOutlet UITextView *textTextView;
 
@property (weak, nonatomic) IBOutlet UITextField *chapterTextField;

@property (weak, nonatomic) IBOutlet UITextField *fromDateYear;
@property (weak, nonatomic) IBOutlet UITextField *fromDateMonth;
@property (weak, nonatomic) IBOutlet UITextField *fromDateDay;

@property (weak, nonatomic) IBOutlet UITextField *toDateYear;
@property (weak, nonatomic) IBOutlet UITextField *toDateMonth;
@property (weak, nonatomic) IBOutlet UITextField *toDateDay;

@property (weak, nonatomic) IBOutlet UILabel *photoCaptionLabel;

@property(strong,nonatomic)    UIActionSheet *takePhotoactionSheet;
@property(strong,nonatomic)  UIActionSheet *photoActionsactionSheet;
@property (strong,nonatomic) UIAlertView *setCaptionAlertView;
@property (strong,nonatomic) UIAlertView *deleteMemoAlertView;

@property (weak, nonatomic) IBOutlet UITextField *searchLocationTextField;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic) CLLocationCoordinate2D selectedCoordinate;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property(strong,nonatomic) ORSelectableViewScroller* selectLocationScroller;
@property(strong,nonatomic) NSArray *resultPlacesArray;

@property(strong,nonatomic) HLPlace *editingPlace;




@end

@implementation HLCreateMeemoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
 

-(void)setEditingPlace:(HLPlace *)editingPlace
{
    _editingPlace = editingPlace;
    _editingMeemo.editingPlace = _editingPlace;
    
    
    if(_editingPlace){
    self.searchLocationTextField.text = _editingPlace.caption;
 
    
    [self.mapView removeAnnotation:annotationPoint];
    
     annotationPoint = [[MKPointAnnotation alloc] init]; annotationPoint.coordinate = CLLocationCoordinate2DMake(_editingPlace.latitude, _editingPlace.longitude);
    
    annotationPoint.title = _editingPlace.caption;
    
    [self.mapView addAnnotation:annotationPoint];
    [self.mapView setCenterCoordinate:annotationPoint.coordinate];
    
    }
    else{
         [self.mapView removeAnnotation:annotationPoint];
    }
    
}
-(void)setResultPlacesArray:(NSArray *)resultPlacesArray
{
    _resultPlacesArray = resultPlacesArray;
    
    NSMutableArray *placesItemArray = [[NSMutableArray alloc]init];
    for(HLPlace *place in _resultPlacesArray){
        HLPlaceView *placeView = [[HLPlaceView alloc] initWithPlace:place];
    
        [placesItemArray addObject:placeView];
    }

    [self.selectLocationScroller setViewElements:placesItemArray];
}

#define HEIGHT_SCROLLER 30
-(ORSelectableViewScroller *)selectLocationScroller{
    if(!_selectLocationScroller){
        _selectLocationScroller = [[ORSelectableViewScroller alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_SCROLLER)];
        _selectLocationScroller.delegate = self;

    }
    return _selectLocationScroller;
}
-(CLLocationManager *)locationManager
{
    if(!_locationManager) {
        _locationManager = [[CLLocationManager alloc]init];
        _locationManager.delegate = self;
    }
    return _locationManager;
}
-(UIAlertView *)deleteMemoAlertView{
    if(!_deleteMemoAlertView){
      _deleteMemoAlertView =    [[UIAlertView alloc]initWithTitle:@"Cancel memo" message:@"Are you sure you want to cancel? Your changes will be lost." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    }
    return _deleteMemoAlertView;
}
-(UIAlertView *)setCaptionAlertView{
    if(!_setCaptionAlertView){
        _setCaptionAlertView =    [[UIAlertView alloc]initWithTitle:@"Photo Caption" message:@"Please enter a photo Caption" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        _setCaptionAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [_setCaptionAlertView textFieldAtIndex:0].delegate = self;
    }
    return _setCaptionAlertView;
}
-(UIActionSheet *)takePhotoactionSheet{
    if(!_takePhotoactionSheet){
        _takePhotoactionSheet =  [[UIActionSheet alloc] initWithTitle:nil
                                                                                            delegate:self
                                                                                   cancelButtonTitle:@"Cancel"
                                                                              destructiveButtonTitle:nil
                                                                                   otherButtonTitles:@"Take a photo",@"Select a photo",nil];
    }
    return _takePhotoactionSheet;
}

-(UIActionSheet *)photoActionsactionSheet{
    if(!_photoActionsactionSheet){
        _photoActionsactionSheet =   [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:@"Delete"
                                                        otherButtonTitles:@"Set Caption",@"Add new photo",nil];
    }
    return _photoActionsactionSheet;
}
-(HLEditingMeemo *)editingMeemo
{
    if(!_editingMeemo){
        _editingMeemo = [HLDataStore sharedInstance].editingMeemo;
        
        
        
    }
    
    return  _editingMeemo;
}

 
 

-(UIImagePickerController *)imagePicker{
    if(!_imagePicker){
        _imagePicker = [[UIImagePickerController alloc] init];
    }
    return _imagePicker;
}

-(UIView *)editPhotoView
{
    if(!_editPhotoView){
        _editPhotoView = [[[NSBundle mainBundle] loadNibNamed:@"EditMeemoPhoto" owner:self options:nil] objectAtIndex:0];
    }
    return _editPhotoView;
}
-(UIView *)editTitleView{
    if(!_editTitleView){
        _editTitleView = [[[NSBundle mainBundle] loadNibNamed:@"EditMeemoTitle" owner:self options:nil] objectAtIndex:0];
    }
    return _editTitleView;
}
-(UIView *)editPlaceView
{
    if(!_editPlaceView){
        _editPlaceView = [[[NSBundle mainBundle] loadNibNamed:@"EditMeemoPlace" owner:self options:nil] objectAtIndex:0];
        
    }
    return _editPlaceView;
}
-(UIView *)editDateView{
    if(!_editDateView){
        _editDateView = [[[NSBundle mainBundle] loadNibNamed:@"EditMeemoDates" owner:self options:nil] objectAtIndex:0];
    }
    return _editDateView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.photosThumbnailScrollView.allowsInteraction = NO;
    //[self updateViewPhoto];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if([self isValidForm]){
        [[HLDataStore sharedInstance] saveDraftMeemo];
        [[self.tabBarController.tabBar.items objectAtIndex:CREATE_MEMO_TAB_BAR_INDEX] setBadgeValue:@"1"];
    }
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
   
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSingleTap:)];
    
    [self.view addGestureRecognizer:tap];
 
    [self.mapView setUserInteractionEnabled:YES];
    self.selectedCoordinate = kCLLocationCoordinate2DInvalid;
    [self.locationManager startUpdatingLocation];
 
    [self setCurrentEditView:self.editPhotoView];
 
    
    [self connectViewOutletsWithModel];
    
    self.headerView.label.text = @"Create new memo";
 
    
    
    
    [[HLDataStore sharedInstance]getPlacesInBlock:^(NSArray *placesArray) {
        self.userListOfPlaces = placesArray;
        NSLog(@"Places: %@",self.userListOfPlaces);
    }];
     self.searchLocationTextField.inputAccessoryView = self.selectLocationScroller;
}

-(void)connectViewOutletsWithModel{
  
    self.modelObject =  self.editingMeemo;
    
    
    if([self.editingMeemo inDraftState]){
        
        self.cancelButton.hidden = NO;
        self.publishButton.hidden = NO;
        self.publishButtonBig.hidden = YES;
    }else{
        self.cancelButton.hidden = YES;
        self.publishButton.hidden = YES;
        self.publishButtonBig.hidden = NO;
    }
    
 
    
    [self connectTextField:self.titleEditText forKey:@"title"];
    
    [self connectTextView:self.textTextView forKey:@"text"];
    
    [self connectSimplePickerWithOptions:[[HLDataStore sharedInstance].currentUser getChaptersNames] toTextField:self.chapterTextField forKey:@"chapterName"];
    
    [self connectSimplePickerWithOptions:[@[@""] arrayByAddingObjectsFromArray:[NSDate getYearsArrayFrom:1900]] toTextField:self.fromDateYear  forKey:@"fromDateYear"];
 
    
    [self connectSimplePickerWithOptions:[@[@""] arrayByAddingObjectsFromArray:[NSDate getMonthNames]]  toTextField:self.fromDateMonth  forKey:@"fromDateMonth"];
    
    [self connectSimplePickerWithOptions:[@[@""] arrayByAddingObjectsFromArray:self.editingMeemo.daysForCurrentDateFrom] toTextField:self.fromDateDay  forKey:@"fromDateDay"];
   

    [self connectSimplePickerWithOptions:[@[@""] arrayByAddingObjectsFromArray:[NSDate getYearsArrayFrom:1900]] toTextField:self.toDateYear  forKey:@"toDateYear"];

    [self connectSimplePickerWithOptions:[@[@""] arrayByAddingObjectsFromArray:[NSDate getMonthNames]]  toTextField:self.toDateMonth  forKey:@"toDateMonth"];
    
    [self connectSimplePickerWithOptions:[@[@""] arrayByAddingObjectsFromArray:self.editingMeemo.daysForCurrentDateFrom] toTextField:self.toDateDay  forKey:@"toDateDay"];
    
    
    [self updateViewPhoto];
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    
    [self cancelInput];
    
}


-(void)disableIcons{
        self.photoIconView.image = [UIImage imageNamed:@"photo_grey.png"];
    self.titleIconView.image = [UIImage imageNamed:@"txt_grey.png"];
    self.locationIconView.image = [UIImage imageNamed:@"local_grey.png"];
    self.dateIconView.image = [UIImage imageNamed:@"date_grey.png"];
}
-(void)emptyContainerView{
    [self.editPhotoView removeFromSuperview];
    [self.editTitleView removeFromSuperview];
    [self.editPlaceView removeFromSuperview];
    [self.editDateView removeFromSuperview];
    
}
-(void)setCurrentEditView:(UIView *)view{
    
    [self cancelInput];
    
    if(self.selectedView == view)
        return;
    
    [self disableIcons];
    [self emptyContainerView];
    
    
    if(view == self.editPhotoView){
 
        
        
        self.photoIconView.image = [UIImage imageNamed:@"photo_green.png"];
  
    }
    
    if(view == self.editTitleView){
 
         self.titleIconView.image = [UIImage imageNamed:@"txt_green.png"];
       
    }
    if(view == self.editPlaceView){
        self.locationIconView.image = [UIImage imageNamed:@"local_green.png"];
    }
    if(view == self.editDateView){
        self.dateIconView.image = [UIImage imageNamed:@"date_green.png"];
    }
    
    [self.containerViewScroller setContentSize:CGSizeMake(view.bounds.size.width, view.bounds.size.height)];
    [self.containerViewScroller addSubview:view];
    
    self.selectedView = view;
}


#pragma mark - RBForms
- (CGPoint)computeBottomPointForView:(UIView *)view
{
    NSLog(@"Compute bottom for view %@",view);
    if([view isDescendantOfView:self.editDateView]){
        CGPoint bottomPoint = view.frame.origin;
        bottomPoint.y += self.editDateView.frame.origin.y;
        return bottomPoint;
    }
    else{
        return [super computeBottomPointForView:view];
    }
    
}
- (void)changedActiveView:(UIView *)view
{
    NSLog(@"Changed view %@",view);
    if(self.selectedView == self.editDateView){
        [self updateViewDate];
    }
    
}



-(void)updateViewDate{
    
     //self.fromDateDay.hidden = !(self.editingMeemo.fromDateYear && self.editingMeemo.fromDateMonth);
    
}
-(void)updateViewPlace{
    
}
-(void)updateViewTitle{
    
}
-(void)updateViewPhoto{
    if([self.editingMeemo.editingPhotos count]>0){
        self.photoCaptionLabel.alpha = 1.0;
        
    }
    else{
         self.photoCaptionLabel.alpha = 0.0;
    }
    [self updatePhotoThumbView];
}

#pragma mark - Actions
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showMemoAfterCreateSegue"]){
        HLViewMeemoViewController *vmVC =   segue.destinationViewController;
        vmVC.meemo = sender;
    }
}

- (IBAction)tapPhoto:(id)sender {
    [self setCurrentEditView:self.editPhotoView];
    [self updateViewPhoto];
    
}
- (IBAction)tapTitle:(id)sender {
    [self setCurrentEditView:self.editTitleView];
    [self updateViewTitle];
}
- (IBAction)tapPlaces:(id)sender {
    [self setCurrentEditView:self.editPlaceView];
    [self updateViewPlace];
}


- (IBAction)tapDate:(id)sender {
    [self setCurrentEditView:self.editDateView];
    
    [self updateViewDate];
  
}


- (IBAction)getLocationAction:(id)sender {
      NSLog(@"Get Location action %@",sender);
}
- (IBAction)showPhotoActions:(id)sender {
    
    [self cancelInput];
    
    NSLog(@"Current Photo selected %d ",   self.photosThumbnailScrollView.stepIndicator.currentPage );
          
    currentPhotoIndex = self.photosThumbnailScrollView.stepIndicator.currentPage;
    
    BOOL photoArrayEmpty = [self.editingMeemo.editingPhotos count] == 0;
    
    if(currentPhotoIndex>-1 && !photoArrayEmpty){
        NSLog(@"Current Photo index %d",currentPhotoIndex);
       
        [self.photoActionsactionSheet showInView:self.self.photosThumbnailScrollView];
    }else{
        //add 1st photo
      [self.takePhotoactionSheet showInView:self.photosThumbnailScrollView];
        
    }
    
    
}

 
 
 
- (IBAction)dateChapterChanged:(UISegmentedControl *)sender {
    
    NSLog(@"date chapter changed %d",sender.selectedSegmentIndex);
    
}
-(BOOL)isValidForm{
    BOOL retValue = YES;
    
    if(!self.editingMeemo.title || [self.editingMeemo.title length]==0){
        retValue = NO;
    }
    
    return retValue;
}
- (IBAction)cancelMemoAction:(id)sender {
    
    [self.deleteMemoAlertView show];
}

- (IBAction)publishMemo:(id)sender {
    
    NSLog(@"Publish");
    if([self isValidForm]){
        
        [[HLDataStore sharedInstance] saveDraftMeemo];
        
        [SVProgressHUD showWithStatus:@"Uploading.."];
        
        [[HLDataStore sharedInstance] publishMemo:self.editingMeemo returnBlock:^(NSString *meemoId) {
            if(!meemoId){
                [SVProgressHUD showErrorWithStatus:@"Error publishing Memo"];
                
                [[self.tabBarController.tabBar.items objectAtIndex:CREATE_MEMO_TAB_BAR_INDEX] setBadgeValue:@"1"];
            
            }
            else{
                 [[self.tabBarController.tabBar.items objectAtIndex:CREATE_MEMO_TAB_BAR_INDEX] setBadgeValue:nil];
              
                
                [SVProgressHUD showSuccessWithStatus:@"Memo published"];
                
              
                [[HLDataStore sharedInstance] deleteDraftMemo];
                
                self.editingMeemo = [HLDataStore sharedInstance].editingMeemo;
                
   
                [self connectViewOutletsWithModel];
        
                
                [[HLDataStore sharedInstance] getMeemoFromID:meemoId forUser:[HLDataStore sharedInstance].currentUser withBlock:^(HLMeemo *meemo) {
                    
                    if(meemo){
                      // [self performSegueWithIdentifier:@"showMemoAfterCreateSegue" sender:meemo];
                    }
                    else{
                        [SVProgressHUD showErrorWithStatus:@"Error showing memo."];
                    }
                }];
          
            }
            
        }];
        
    }
    else{
        [SVProgressHUD showErrorWithStatus:@"Complete the mandatory fields"];
    }
    /*
    if([self isValidForm]){
        
        //self.editingMeemo.date = [[NSDate alloc]init];
        
        self.editingMeemo.editingPhotos = self.photoArray;
        
        if(self.editingPlace){
        if(![self.userListOfPlaces containsObject:self.editingPlace]){
            
                [[HLDataStore sharedInstance] createPlace:self.editingPlace successblock:^(BOOL success) {
                    
                }];
            
        }
      }
     
        
   
        
        [SVProgressHUD showWithStatus:@"Uploading.."];
        
        [[HLDataStore sharedInstance] publishMemo:self.editingMeemo returnBlock:^(HLMeemo *meemo) {
            if(!meemo){
               [SVProgressHUD showErrorWithStatus:@"Error publishing Memo"];
            }
            
            
        }];
    }
    else{
        [SVProgressHUD showErrorWithStatus:@"Complete the mandatory fields"];
    }
    */
  
}


-(void)updatePhotoThumbView{
       [self.photosThumbnailScrollView setImages:self.editingMeemo.editingPhotos withGap:10.0f withVerticalGap:0.0f inGroupsOf:1];
}


- (IBAction)searchLocationTextFieldChanged:(id)sender {
    
    self.editingPlace = nil;
    
    NSLog(@"New Search for Place: %@",self.searchLocationTextField.text);
    NSString *queryLocationString = self.searchLocationTextField.text;
    if([queryLocationString length] > 3){
      [[HLLocationHelper sharedInstance] searchPlaceWithQuery:queryLocationString inLocation:self.selectedCoordinate aroundDistanceInKms:500 additionalPlacesArray:self.userListOfPlaces returnBlock:^(NSArray *placesArray) {
          NSLog(@"Found places %@",placesArray);
          self.resultPlacesArray = placesArray;
      }];
    }
   
   
}
 


#pragma mark - UIActionSheetDelegate

#define TAKE_PHOTO_INDEX 0
#define SELECT_PHOTO_INDEX 1
#define MAX_PHOTOS_ALLOWED 10
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(actionSheet == self.takePhotoactionSheet){
        if(buttonIndex != actionSheet.cancelButtonIndex){
            switch (buttonIndex) {
                case TAKE_PHOTO_INDEX:
                    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                        [self.imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                    }else{
                        [self.imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                    }
                    break;
                case SELECT_PHOTO_INDEX:
                    
                    [self.imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                    
                    break;
                default:
                    break;
            }
            [self.imagePicker setDelegate:self];
            self.imagePicker.allowsEditing = YES;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
            
        }
    }
    
    if(actionSheet == self.photoActionsactionSheet){
            if(buttonIndex != actionSheet.cancelButtonIndex){
                if(buttonIndex == actionSheet.destructiveButtonIndex){
                    NSLog(@"Delete Photo %d",currentPhotoIndex);
                    NSMutableArray *mutablePhotos = [self.editingMeemo.editingPhotos mutableCopy];
                    [mutablePhotos removeObjectAtIndex:currentPhotoIndex];
                    self.editingMeemo.editingPhotos = mutablePhotos;
                    
                    NSMutableArray *mutableCaptions= [self.editingMeemo.editingCaptions mutableCopy];
                    [mutableCaptions removeObjectAtIndex:currentPhotoIndex];
                    self.editingMeemo.editingCaptions = mutableCaptions;
                    
                    
                    [self updatePhotoThumbView];
                    
                }
                if(buttonIndex == 1){
                    NSLog(@"Set caption");
                 
                    NSString *currentCaption = [self.editingMeemo.editingCaptions objectAtIndex:currentPhotoIndex];
                
                    [self.setCaptionAlertView textFieldAtIndex:0].text = currentCaption;
                    [self.setCaptionAlertView show];
     
                }
                else if (buttonIndex == 2){
                    
                    if([self.editingMeemo.editingPhotos count]< MAX_PHOTOS_ALLOWED){
                        [self.takePhotoactionSheet showInView:self.photosThumbnailScrollView]; 
                    }else{
                        [SVProgressHUD showErrorWithStatus:@"Photo limit!"];
                    }
               
                }
            }
    }
    
    
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView == self.setCaptionAlertView){
        if(buttonIndex == alertView.firstOtherButtonIndex){
  
            NSString *captionValue =[self.setCaptionAlertView textFieldAtIndex:0].text ;
            
            NSMutableArray *mutableCaptions= [self.editingMeemo.editingCaptions mutableCopy];
            [mutableCaptions replaceObjectAtIndex:currentPhotoIndex withObject:captionValue];
            self.editingMeemo.editingCaptions = mutableCaptions;
            
           
        }
    }
    
    if(alertView == self.deleteMemoAlertView){
        if(buttonIndex == alertView.firstOtherButtonIndex){
            [[HLDataStore sharedInstance] deleteDraftMemo];
            
            self.editingMeemo = [HLDataStore sharedInstance].editingMeemo;
     
            [self connectViewOutletsWithModel];

        }
    }
}

#define MAX_IMAGE_SIZE_FOR_MEMO_PHOTOS 800
 
#pragma mark - UIImagePickerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
     image = [UIImage imageWithImage:image scaledProportionalToMaxWidth:MAX_IMAGE_SIZE_FOR_MEMO_PHOTOS];
    
    
    NSMutableArray *mutablePhotos = [self.editingMeemo.editingPhotos mutableCopy];
    [mutablePhotos addObject:image];
    self.editingMeemo.editingPhotos = mutablePhotos;
    
    NSMutableArray *mutableCaptions= [self.editingMeemo.editingCaptions mutableCopy];
    [mutableCaptions addObject:@""];    
    self.editingMeemo.editingCaptions = mutableCaptions;
    
    
    [self updatePhotoThumbView];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
 

#pragma mark - CLLocationManagerDelegate


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // if the location is older than 30s ignore
    if (fabs([newLocation.timestamp timeIntervalSinceDate:[NSDate date]]) > 30)
    {
        return;
    }
    
    self.selectedCoordinate = [newLocation coordinate];
    
    [self.mapView setCenterCoordinate:self.selectedCoordinate animated:YES];
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.selectedCoordinate, 100000, 100000) animated:YES];
 
    NSLog(@"New updated Location %f x %f",  self.selectedCoordinate.longitude,self.selectedCoordinate.latitude);
    [self.locationManager stopUpdatingLocation];
}

#pragma  mark - ORSelectableViewScrollerDelegate
-(void)selectableViewScroller:(ORViewScroller *)scroller viewWasSelected:(UIView *)view{
    HLPlace *selectedPlace = ((HLPlaceView *)view).place;
    if(selectedPlace ){
        self.editingPlace = selectedPlace;
        self.editingMeemo.editingPlace = selectedPlace;
        [self.searchLocationTextField resignFirstResponder];
    }
  
}


#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"type it here..."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"type it here...";
        textView.textColor = [UIColor lightTextColor]; //optional
    }
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
@end
