//
//  HLHeaderJoinView.m
//  hiyalife
//
//  Created by Juan Hontanilla on 23/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLHeaderJoinView.h"

@implementation HLHeaderJoinView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}


-(HLLabelMediumSizeDark *)label{
    if(!_label)
        _label = [[HLLabelMediumSizeDark alloc]initWithFrame:CGRectMake(68,6, 140, 34)];
    return _label;
}


- (void)setup {
    [self setBackgroundColor:[UIColor clearColor]];
    UIImageView *sepizq = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"grey_line"]];
    UIImageView *sepder = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"grey_line"]];
    UIImageView *logo = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logo_mini"]];
    
    sepizq.frame = CGRectMake(20,16, 42, 15);
    sepizq.contentMode = UIViewContentModeScaleAspectFit;
    sepder.frame = CGRectMake(258,16,42,15);
    sepder.contentMode = UIViewContentModeScaleAspectFit;
    
    logo.frame = CGRectMake(207, 16, 44, 14);
    logo.contentMode = UIViewContentModeScaleAspectFit;
    
 
    self.label.backgroundColor = [UIColor clearColor];
    self.label.text = @"Register through";
    
    
    [self addSubview:sepizq];
    [self addSubview:sepder];
    [self addSubview:logo];
    [self addSubview:self.label];
    
    
}


@end
