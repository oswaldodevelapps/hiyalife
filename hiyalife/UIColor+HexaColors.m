//
//  UIColor+HexaColors.m
//  hiyalife
//
//  Created by Juan Hontanilla on 23/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "UIColor+HexaColors.h"

@implementation UIColor (HexaColors)
+ (UIColor *)colorWithHexString:(NSString *)str{
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr+1, NULL, 16);
    return [UIColor colorWithHex:x];
}
+ (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}
@end
