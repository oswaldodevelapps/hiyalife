//
//  HLGreenLabel.m
//  hiyalife
//
//  Created by Juan Hontanilla on 13/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLGreenLabel.h"

@implementation HLGreenLabel

 

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
- (void)setup {
 
    self.font= [UIFont fontWithName:@"Tauri" size:14.0];
   
    self.textColor  = [UIColor colorWithRed:116/255.0f green:206/255.0f blue:207/255.0f alpha:1.0];
}
@end
