//
//  HLTestSearchFriendsViewController.m
//  hiyalife
//
//  Created by Oswaldo on 04/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLTestSearchFriendsViewController.h"
#import "HLDataStore.h"
 
#import "HLOtherUser.h"
#import "ORSelectableViewScroller.h"
#import "HLUserButtonTinySize.h"
#import "HLUserView.h"


@interface HLTestSearchFriendsViewController ()<ORSelectableViewScrollerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *searchFriendTextField;
 

@property(strong,nonatomic) NSArray *resultFriendsArray;
@property(strong,nonatomic) NSArray *selectedFriendsArray;

@property(strong,nonatomic) ORSelectableViewScroller* userListToolBar;
@property (weak, nonatomic) IBOutlet ORSelectableViewScroller *destinationView;

@end

@implementation HLTestSearchFriendsViewController

@synthesize selectedFriendsArray = _selectedFriendsArray;

-(void)setResultFriendsArray:(NSArray *)resultFriendsArray 
{
    _resultFriendsArray = resultFriendsArray;
 
    [self setTheseUsers:_resultFriendsArray inThatSelectableView:self.userListToolBar];
}


-(void)setSelectedFriendsArray:(NSArray *)selectedFriendsArray
{
    _selectedFriendsArray = selectedFriendsArray;
    [self setTheseUsers:_selectedFriendsArray inThatSelectableView:self.destinationView];
}
-(NSArray *)selectedFriendsArray{
    if(!_selectedFriendsArray){
        _selectedFriendsArray = [[NSArray alloc]init];
    }
return _selectedFriendsArray;
}


#define HEIGHT_SCROLLER 30
-(ORSelectableViewScroller *)userListToolBar{
    if(!_userListToolBar){
        _userListToolBar = [[ORSelectableViewScroller alloc]initWithFrame:CGRectMake(0, 0, 320, HEIGHT_SCROLLER)];
        _userListToolBar.delegate = self;
    }
    return _userListToolBar;
}


-(void)setTheseUsers:(NSArray *)users inThatSelectableView:(ORViewScroller *)view{
    NSMutableArray *userItemArray = [[NSMutableArray alloc]init];
    
    NSLog(@"Send %d users to view %@",[users count],view);
    
    for (HLOtherUser * user in users) {
 
        HLUserView *userView = [[HLUserView alloc]initWithUser:user ];
        
        [userItemArray addObject:userView];
    }
  
    
    [view setViewElements:userItemArray];
}

 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.searchFriendTextField.inputAccessoryView = self.userListToolBar;
    self.destinationView.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    [[HLDataStore sharedInstance] findUserListByKey:substring
                                        returnBlock:^(NSArray *friends) {
                                            NSLog(@"Found Friends %@", friends);
                                            self.resultFriendsArray = friends;
                                            
    }];
}


#pragma  mark - ORSelectableViewScrollerDelegate
-(void)selectableViewScroller:(ORViewScroller *)scroller viewWasSelected:(UIView *)view{
    
    if(scroller == self.userListToolBar){
        HLUserView *userView = (HLUserView *)view;
        [UIView animateWithDuration:0.3 animations:^{
            userView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            NSLog(@"user %@ was selected",userView.user);
            
            NSMutableArray *mutableUsersArray = [self.selectedFriendsArray mutableCopy];
            [mutableUsersArray addObject:userView.user];
            
            NSMutableArray *mutableSearchArray = [self.resultFriendsArray mutableCopy];
            [mutableSearchArray removeObject:userView.user];
            
            self.resultFriendsArray = mutableSearchArray;
            
            self.selectedFriendsArray = mutableUsersArray;
        }];
    }
    

    if(scroller == self.destinationView){
        
        
         HLUserView *userView = (HLUserView *)view;
       
        [UIView animateWithDuration:0.3 animations:^{
            userView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            NSLog(@"Remove from list user %@",userView.user);
            NSMutableArray *mutableUsersArray = [self.selectedFriendsArray mutableCopy];
            [mutableUsersArray removeObject:userView.user];
            self.selectedFriendsArray = mutableUsersArray;
        }];
    }
}

#pragma mark UITextFieldDelegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring stringByReplacingCharactersInRange:range withString:string];
        
        if([substring length] > 1){
            [self searchAutocompleteEntriesWithSubstring:substring];
            //NSLog(@"Search friends with String: %@",substring);
        }else{
            self.resultFriendsArray = nil;
        }
    

    return YES;
}
@end
