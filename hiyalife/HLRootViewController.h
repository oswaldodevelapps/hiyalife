//
//  HLRootViewController.h
//  hiyalife
//
//  Created by Juan Hontanilla on 06/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLMeUser.h"


@interface HLRootViewController : UIViewController
- (IBAction)unwindFromLogout:(UIStoryboardSegue *)segue;
- (IBAction)unwindFromLogin:(UIStoryboardSegue *)segue;
- (IBAction)unwindFromFacebookJoin:(UIStoryboardSegue *)segue;
-(void)handleValidationForUser:(HLMeUser *)user;

@property (nonatomic) BOOL skipAutoValidation;

@end
