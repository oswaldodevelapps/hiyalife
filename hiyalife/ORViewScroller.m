//
//  ORButtonScrollerView.m
//  hiyalife
//
//  Created by Oswaldo on 04/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "ORViewScroller.h"

@interface ORViewScroller()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSArray *viewElements;

@end

@implementation ORViewScroller

- (void)awakeFromNib
{
    if (self.viewElements) {
        [self setupScroller];
    }
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _scrollView.delegate = self;
        _scrollView.scrollEnabled = YES;
        //_scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.directionalLockEnabled = YES;
        _scrollView.alwaysBounceVertical = NO;
        _scrollView.backgroundColor = [UIColor darkGrayColor];
        [self addSubview:_scrollView];
    }
    return _scrollView;
}
 
-(void)setViewElements:(NSArray *)elements
{
    _viewElements = elements;
    [self setupScroller];
}

#define HORIZONTAL_SPACE 6
- (void)setupScroller
{
    [[self.scrollView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    //NSLog(@"Total Width %d",[self.viewElements count]);
    
    int index =0;
    CGFloat totalSize = 0;
    
    
    for (UIView* viewItem in self.viewElements) {
       // NSLog(@"Button %f",viewItem.frame.size.width);
       
        
          viewItem.frame = CGRectOffset(viewItem.frame, totalSize , 4);
        
         viewItem.frame = CGRectInset(viewItem.frame, HORIZONTAL_SPACE, 0);
         totalSize +=viewItem.frame.size.width + HORIZONTAL_SPACE*2;
        
        
        [self.scrollView addSubview:viewItem];
        
        index++;
    }
    
    self.scrollView.contentSize = CGSizeMake(totalSize, self.scrollView.frame.size.height);
    
}

@end
