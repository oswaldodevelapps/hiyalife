//
//  Constants.h
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#ifndef hiyalife_Constants_h
#define hiyalife_Constants_h

#define AUTH_BASE_URL @"http://prepro.hiyalife.com/"
#define RES_BASE_URL @"http://api.hiyalife.com/"

//Social
 
#define FACEBOOK_APP_ID @"416416958424454"

//API
#define API_CLIENT_ID @"97da4711fa6a073b720d24a0c582f1e01a5ca530"
#define API_CLIENT_SECRET @"dd6e33bc16bb0189793ff871e97a6018dc92052d"



#define K_FORMATER_DATE @"yyyy-MM-dd"
#define K_FORMATER_TEXT_DATE @"MMM dd'th' yyyy"]

#define HL_BLUE_COLOR @"#9ac2c9"
#define HL_GREEN_COLOR @"#bbdeb5"
#define HL_PURPLE_COLOR @"#aa9ac6"
#define HL_YELLO_COLOR @"#c9bc00"
#define HL_DARL_GREY_COLOR @"#434343"



#define CREATE_MEMO_TAB_BAR_INDEX 2


//Errors
#define USER_ERROR_DOMAIN @"User Error"
#define USER_EMAIL_INVALID 100 
#define MEMO_ERROR_DOMAIN @"Memo Error"
 


typedef enum {
    HLprivacyPrivate,    
    HLprivacyLimited,
    HLprivacyPublic
} HLMeemoPrivacyType;

typedef enum
{
    HLPreferredWide,
    HLPreferredShort
}HLMeemoPreferredLayout;





typedef void (^TokenBlock)(NSString *access_token);

#endif
