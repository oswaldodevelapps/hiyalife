//
//  HLNotificationsViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLNotificationsViewController.h"
#import "HLNotification.h"
#import "HLMeemo.h"
#import "HLCustomNotificationCell.h"
#import "NSDate+HLDateFormatter.h"
#import "HLDataStore.h"
#import "UIImageView+AFNetworking.h"
#import "HLViewMeemoViewController.h"
#import "HLHeaderView.h"
#import "SVProgressHUD.h"
#import "HLButtonTinySize.h"


@interface HLNotificationsViewController (){
    
    float textoAlto;
}

@property (nonatomic,strong) IBOutlet UITableView      *tableView;
@property (weak, nonatomic)  IBOutlet HLHeaderView     *headerView;
@property (weak, nonatomic)  IBOutlet HLButtonTinySize *clearAllButton;

@property (nonatomic,strong) NSMutableArray   *notificationsArray;
@property (nonatomic,strong) UIRefreshControl *refreshControl;

@end

@implementation HLNotificationsViewController
static NSString *CellIdentifier = @"HLCustomNotificationCell";
#define FONT_SIZE 10.0f
#define CELL_DEFAULT_HEIGHT 55
#define CELL_CONTENT_WIDTH 290
#define CELL_CONTENT_MARGIN 10.0f
#define CELL_MINIUM_LABEL_HEIGHT 13.0f
#define CELL_FONT [UIFont fontWithName:@"Tauri" size:10.0]

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"View Did Load Notifications VC");
    
    [self.tableView setHidden:YES];
    
    self.headerView.label.text = @"Notifications";
    self.navigationItem.titleView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    
    [self configureRefreshControl];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self getNotifications];
};


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getNotifications{
    
    [self.clearAllButton setEnabled:NO];
    
    //---get the notifications array---
    [[HLDataStore sharedInstance] getNotificationsWithBlock:^(NSArray *arrayNotifications) {
        
        self.notificationsArray = [[NSMutableArray alloc]initWithArray:arrayNotifications];
        
        if ([self.notificationsArray count] == 0) {
            [self.tableView setHidden:YES];
        }
        else
            {[self.tableView setHidden:NO];
             [self.tableView reloadData];}
    }];
}

-(void)configureRefreshControl{
    
    self.refreshControl = [[UIRefreshControl alloc]
                           init];
    
    self.refreshControl.frame = CGRectMake(0.0f, 20.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height);
    
    self.refreshControl.tintColor = [UIColor grayColor];
    
    [self.refreshControl addTarget:self action:@selector(valueChanged) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
 
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    // Return the number of rows in the section.
    return [self.notificationsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    HLCustomNotificationCell *cell = (HLCustomNotificationCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    HLNotification *notif = self.notificationsArray[indexPath.row];
    
    cell.textNotificationLabel.text = notif.text;
    cell.typeNotification           = notif.typeNotification;
    cell.notificationTypeLabel.text = notif.typeString;
    cell.userNameLabel.text         = notif.userName;
    cell.dateLabel.text             = [notif.date dateStringSecondFormat];
    
    [cell.avatarImageView setImageWithURL:notif.userPhotoURL];
    
    if (!notif.isReaded)
        [self.clearAllButton setEnabled:YES];

    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
     HLNotification *notif = self.notificationsArray[indexPath.row];
    cell.backgroundColor = (notif.isReaded) ? [UIColor colorWithRed:218/255.0f green:228/255.0f blue:230/255.0f alpha:1.0] : [UIColor whiteColor];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor colorWithRed:121/255.0f green:217/255.0f blue:166/255.0f alpha:1.0]];
    [cell setSelectedBackgroundView:bgColorView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    NSString *text = [[self.notificationsArray objectAtIndex:[indexPath row]] text];
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat height = MAX(size.height, CELL_MINIUM_LABEL_HEIGHT);
    float heightCell = 0;
    
    if (height == CELL_MINIUM_LABEL_HEIGHT) 
        heightCell = CELL_DEFAULT_HEIGHT;
    else
        heightCell = height + (CELL_CONTENT_MARGIN * 2) + CELL_DEFAULT_HEIGHT;
        
    return heightCell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //---Change the status to read Notification---
    [[HLDataStore sharedInstance]setNotificationAsRead:[NSArray arrayWithObject:[self.notificationsArray objectAtIndex:indexPath.row]] returnBlock:^(BOOL success) {
        if (success) {

            //reload the cell
            [tableView reloadRowsAtIndexPaths: [NSArray arrayWithObject: indexPath]
                             withRowAnimation: UITableViewRowAnimationNone];
        }
    }];
    
    //---Segue to Show Meemo---
    HLNotification *tempNot =  [self.notificationsArray objectAtIndex:indexPath.row];
    
    if (tempNot.ideMeemo) {
        [[HLDataStore sharedInstance]getMeemoFromID:tempNot.ideMeemo forUser:nil withBlock:^(HLMeemo *meemo) {
            if (meemo) {
                [self performSegueWithIdentifier:@"showMemoFromNotificationSegue" sender:meemo];
            }
        }];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIRefreshControl  
- (void)valueChanged{
    
    [self getNotifications];
    
    //refresh your data here
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *updated = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:updated];

    [self.refreshControl endRefreshing];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showMemoFromNotificationSegue"]) {
        HLViewMeemoViewController *memoVC   = [segue destinationViewController];
        
        memoVC.meemo = sender;
    }
}

#pragma mark - Actions- 
- (IBAction)actionClearAll:(id)sender {
    
    [SVProgressHUD show];

    NSPredicate *sPredicate2  = [NSPredicate predicateWithFormat:@"isReaded == NO"];
    NSArray *noReads = [self.notificationsArray filteredArrayUsingPredicate:sPredicate2];

    
    [[HLDataStore sharedInstance]setNotificationAsRead:noReads returnBlock:^(BOOL success) {
        [SVProgressHUD dismiss];
        if (success) {
            [self getNotifications];
        }
    }];
}
@end
