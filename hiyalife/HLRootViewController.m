//
//  HLRootViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 06/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLRootViewController.h"

#import "HLDataStore.h"


@interface HLRootViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *logoView;

@end

@implementation HLRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.skipAutoValidation = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_screen"]]];
    
	
 
    
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
              self.logoView.alpha = 1.0; 
                
    } completion:^(BOOL finished) {
        if(!self.skipAutoValidation){
            [self handleUserNavigation];  
        }
        
        self.skipAutoValidation = NO;
    }];
 
}


-(void)handleUserNavigation{
    
    HLMeUser *user = [HLDataStore sharedInstance].currentUser;
    
    if(user){
        NSLog(@"Already exists user login in disk");
        [self performSegueWithIdentifier:@"welcomeToHiyaLifeSegue" sender:self];
        
    }else{
        NSLog(@"Not user Loged");
        
        [self performSegueWithIdentifier:@"AskLoginSegue" sender:self];
    }
    
    
}

-(void)dealloc
{
    NSLog(@"Deallocating %@",NSStringFromClass([self class]));
}

#pragma mark - Actions
-(void)handleValidationForUser:(HLMeUser *)user{
    [self performSegueWithIdentifier:@"completeValidationSegue" sender:self];
    
}
- (IBAction)unwindFromLogout:(UIStoryboardSegue *)segue {
    
    
    [[HLDataStore sharedInstance]logout];
    
    
    NSLog(@"User has Logout or is waiting Join validation");
    
    
}

-(IBAction)unwindFromFacebookJoin:(UIStoryboardSegue *)segue{
    NSLog(@"unwind from Facebook Login");
}

- (IBAction)unwindFromLogin:(UIStoryboardSegue *)segue {
 
}


@end
