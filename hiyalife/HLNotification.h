//
//  HLNotification.h
//  hiyalife
//
//  Created by Oswaldo on 02/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>


/*
 Tipos de notificación: 
 
 [02/07/13 10:44:35] Alfonso Fernandez Roca: 0 -> share. Alguien te ha compartido una meemo
 [02/07/13 10:45:06] Alfonso Fernandez Roca: 1 -> user2user. Mensaje de usuario a usuario( no se usan de momento )
 [02/07/13 10:45:27] Alfonso Fernandez Roca: 2 -> meemochange. Alguien ha modificado o añadido contenido en una memo tuya
 [02/07/13 10:45:44] Alfonso Fernandez Roca: 3. acceptmeemo. Un usuario ha aceptado una meemo tuya
 [02/07/13 10:46:05] Alfonso Fernandez Roca: 4. server2user. Notificación del sistema al usuario( no se usan de momento )
 
 */


@interface HLNotification : NSObject
@property(nonatomic) NSUInteger notificationID;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSURL    *userPhotoURL;
@property (nonatomic, strong) NSString *typeString;
@property (nonatomic,strong)  NSDate   *date;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *ideMeemo;

@property (nonatomic) BOOL    isReaded;
@property (nonatomic) int    typeNotification;



+(HLNotification *)makeNotificationWithDictionary:(NSDictionary *)dict;

@end
