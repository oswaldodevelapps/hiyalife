//
//  HLTestVCViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 24/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLTestVCViewController.h"
#import "HLDataStore.h"

@interface HLTestVCViewController ()
@property (weak, nonatomic) IBOutlet UITextField *memoIDTextField;




@end

@implementation HLTestVCViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
   
}
- (IBAction)getNotifications:(id)sender {
     [[HLDataStore sharedInstance] getNotificationsWithBlock:^(NSArray *arrayNotifications) {
         
     }];
}
- (IBAction)getUserList:(id)sender {
    
    [[HLDataStore sharedInstance] findUserListByKey:@"david" returnBlock:^(NSArray *friends) {
        NSLog(@"Received friends list: %@",friends);
    }];
}
- (IBAction)getUserListFB:(id)sender {
    [[HLDataStore sharedInstance]findUserListByFacebookID:@"anna" returnBlock:^(NSArray *friends) {
        
    }];
    
    
}
- (IBAction)getSharesList:(id)sender {
    
    NSString  *memoID = self.memoIDTextField.text;
    [[HLDataStore sharedInstance]getSharesListForMeemoID:memoID returnBlock:^(NSArray *arrayOfOtherUser) {
        
    }];
    
}
- (IBAction)addToMyTL:(id)sender {
  
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
