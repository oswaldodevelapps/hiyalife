//
//  HLEditingMeemo.h
//  hiyalife
//
//  Created by Oswaldo on 15/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HLPlace.h"

@interface HLEditingMeemo : NSObject
@property (nonatomic,strong) NSMutableDictionary *json;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *title;
@property (nonatomic,strong) NSArray *editingPhotos;
@property (nonatomic,strong) NSArray *editingCaptions;
@property (nonatomic,strong) HLPlace *editingPlace;

@property (nonatomic, strong) NSString * fromDateYear;
@property (nonatomic, strong) NSString * fromDateMonth;
@property (nonatomic, strong) NSString * fromDateDay;
@property(nonatomic,strong) NSArray *daysForCurrentDateFrom;

@property (nonatomic, strong) NSString * toDateYear;
@property (nonatomic, strong) NSString * toDateMonth;
@property (nonatomic, strong) NSString * toDateDay;

@property (nonatomic,strong) NSString *chapterName;

-(BOOL)inDraftState;
+(HLEditingMeemo *)editingMeemoWithDictionary:(NSDictionary *)dict;
@end
