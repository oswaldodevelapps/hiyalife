//
//  HLMainPhotoAndThumbsWithScrollverView.h
//  hiyalife
//
//  Created by Juan Hontanilla on 11/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol HLPhotoThumbsDelegate  <NSObject>

-(void)photoWasSelected:(NSUInteger)
                         selectedImageViewIndex;
 
@end

@interface HLMainPhotoAndThumbsWithScrollverView : UIView
@property (nonatomic,assign) id<HLPhotoThumbsDelegate> delegate;

-(void)setStepIndicator:(UIPageControl *)stepControl;
- (void)setImagesThumnails:(NSArray *)images;
-(void)setActivePage:(int)pageIndex;
@end
