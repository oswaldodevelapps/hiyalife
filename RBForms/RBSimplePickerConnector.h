//
//  RBSimplePickerConnector.h
//  MeetQute
//
//  Created by Rafa Barberá Córdoba on 09/05/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBBasePickerConnector.h"

@interface RBSimplePickerConnector : NSObject <RBBasePickerConnector>

@property (nonatomic, weak) UITextField *textField;

+ (RBSimplePickerConnector *) simplePickerConnectorWithOptions:(NSArray *)options
                                              targetTextField :(UITextField *)targetTextField
                                               observingObject:(NSObject *)object
                                                        forKey:(NSString *)key;

- (id)initWithOptions:(NSArray *)options
      targetTextField:(UITextField *)targetTextField
      observingObject:(NSObject *)object
               forKey:(NSString *)key;

@end
