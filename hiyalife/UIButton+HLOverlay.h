//
//  UIButton+HLOverlay.h
//  hiyalife
//
//  Created by Juan Hontanilla on 13/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (HLOverlay)
+ (UIButton *)overlayButtonWithAction:(SEL)action onTarget:(id)target onTopOfView:(UIView *)view;
@end
