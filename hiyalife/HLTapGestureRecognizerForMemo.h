//
//  HLTapGestureRecognizerForMemo.h
//  hiyalife
//
//  Created by Juan Hontanilla on 21/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLMeemo.h"

@interface HLTapGestureRecognizerForMemo : UITapGestureRecognizer
@property (nonatomic,strong) HLMeemo *memo;
@end
