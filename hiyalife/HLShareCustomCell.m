//
//  HLShareCustomCell.m
//  hiyalife
//
//  Created by Juan Hontanilla on 25/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLShareCustomCell.h"

@implementation HLShareCustomCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
