//
//  RBDatePickerConnector.m
//  MeetQute
//
//  Created by Rafa Barberá Córdoba on 10/05/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import "RBDatePickerConnector.h"

@interface RBDatePickerConnector()

@property (nonatomic, strong) NSObject *observedObject;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *format;
@property (nonatomic, strong) NSDate *maximumDate;
@end

@implementation RBDatePickerConnector

+ (RBDatePickerConnector *) datePickerConnectorWithFormat:(NSString *)format targetTextField:(UITextField *)targetTextField observingObject:(NSObject *)object forKey:(NSString *)key
{
    return [[RBDatePickerConnector alloc] initWithFormat:format targetTextField:targetTextField observingObject:object forKey:key];
}
+ (RBDatePickerConnector *) datePickerConnectorWithFormat:(NSString *)format withMaxDate:(NSDate *)maximumDate targetTextField:(UITextField *)targetTextField observingObject:(NSObject *)object forKey:(NSString *)key
{
    return [[RBDatePickerConnector alloc] initWithFormat:format withMaxDate:maximumDate  targetTextField:targetTextField observingObject:object forKey:key];
}
// TODO: KVO for object
- (id)initWithFormat:(NSString *)format targetTextField:(UITextField *)targetTextField observingObject:(NSObject *)object forKey:(NSString *)key
{
    self = [super init];
    if (self) {
        self.observedObject = object;
        self.key = key;
        self.format = format;
        self.textField = targetTextField;
        [self updateWithModel];
    }
    return self;
}

// TODO: KVO for object
- (id)initWithFormat:(NSString *)format withMaxDate:(NSDate *)maximumDate targetTextField:(UITextField *)targetTextField observingObject:(NSObject *)object forKey:(NSString *)key
{
    self = [super init];
    if (self) {
        self.observedObject = object;
        self.key = key;
        self.format = format;
        self.maximumDate = maximumDate;
        self.textField = targetTextField;
        [self updateWithModel];
    }
    return self;
}
- (UIDatePicker *)picker
{
    if (!_picker)
    {
        _picker = [[UIDatePicker alloc] init];
        _picker.datePickerMode = UIDatePickerModeDate;
        if(_maximumDate){
            _picker.maximumDate = _maximumDate;
        }
        
        [_picker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _picker;
}

- (void)changeModelObject:(NSObject *)newModelObject
{
    if (newModelObject == self.observedObject) return;
    self.observedObject = newModelObject;
    [self updateWithModel];
}


#pragma mark - Events listener

- (void)dateChanged:(UIDatePicker *)datePicker
{
    [self.observedObject setValue:datePicker.date forKey:self.key];
    self.textField.text = [self formatedDate:datePicker.date];
}

#pragma mark - RBBasePickerConnector

- (id)getPicker
{
    return self.picker;
}

- (void)updateWithModel
{
    NSDate *date = [self.observedObject valueForKey:self.key];
    
    //if (!date) date = [NSDate date];
    
    if(date){
      self.picker.date = date;
        self.textField.text = [self formatedDate:self.picker.date];
    }
    
 
    
}

#pragma mark - Format Helper

- (NSString *)formatedDate:(NSDate *)date
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:self.format];
    return [formater stringFromDate:date];
}


@end
