//
//  HLCreateMeemoViewController.h
//  hiyalife
//
//  Created by Juan Hontanilla on 17/06/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBFormViewController.h"

@interface HLCreateMeemoViewController : RBFormViewController

@end
