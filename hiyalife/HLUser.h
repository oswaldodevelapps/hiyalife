//
//  HLUser.h
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLUser : NSObject

@property (nonatomic,assign) int hiyaUserID;
@property (nonatomic,strong)  NSString *name;
@property (nonatomic,strong)  NSString *photoURL;
@property (nonatomic,strong)  NSString *email;
-(BOOL)isMe;
@end
