//
//  RBDatePickerConnector.h
//  MeetQute
//
//  Created by Rafa Barberá Córdoba on 10/05/13.
//  Copyright (c) 2013 Develapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBBasePickerConnector.h"

@interface RBDatePickerConnector : NSObject <RBBasePickerConnector>

@property (nonatomic, weak) UITextField *textField;
@property (nonatomic, strong) UIDatePicker *picker;

+ (RBDatePickerConnector *) datePickerConnectorWithFormat:(NSString *)format
                                         targetTextField :(UITextField *)targetTextField
                                          observingObject:(NSObject *)object
                                                   forKey:(NSString *)key;
+ (RBDatePickerConnector *) datePickerConnectorWithFormat:(NSString *)format withMaxDate:(NSDate *)maximumDate targetTextField:(UITextField *)targetTextField observingObject:(NSObject *)object forKey:(NSString *)key;

- (id)initWithFormat:(NSString *)format
     targetTextField:(UITextField *)targetTextField
     observingObject:(NSObject *)object
              forKey:(NSString *)key;



@end
