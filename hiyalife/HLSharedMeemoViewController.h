//
//  HLSharedMeemoViewController.h
//  hiyalife
//
//  Created by Pablo on 03/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLMeemo.h"

@interface HLSharedMeemoViewController : UIViewController
@property (nonatomic,strong) HLMeemo *meemo;

@end
