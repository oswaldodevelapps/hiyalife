//
//  HLUser.h
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HLUser.h"
 


@interface HLMeUser : HLUser
@property (nonatomic,strong) NSDictionary *json;
@property (nonatomic,strong) NSString *access_token;
@property (nonatomic,strong) NSDate *access_token_datetime;

@property (nonatomic,strong) NSString *token_type;
@property(nonatomic,assign) int expires_in;
@property (nonatomic,strong) NSString *refresh_token;
@property (nonatomic,assign) BOOL first_login;



@property (nonatomic,strong)NSString *surname;
@property (nonatomic, strong) NSDate *birthday;
@property (nonatomic, strong) NSString *gender;
 

@property (nonatomic,strong) NSArray *userChapters;


//login properties
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *password1;
@property(nonatomic,strong) NSString *password2;

//editing Avatar
@property(nonatomic,strong) UIImage *editingPhoto;
 

+(HLMeUser *)makeUserWithDictionary:(NSDictionary *)dict;
-(void)updateUserInfoWith:(NSDictionary *)dict;

-(BOOL)isTokenExpired;

-(void)accessTokenRefreshed:(TokenBlock) token;
- (NSString *)userImageArchivePath;
-(void)setAvatarImage:(UIImage *)avatarImage;
-(UIImage *)avatarUIImage;

-(NSArray * )getChaptersNames;
 
@end
