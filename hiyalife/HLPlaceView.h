//
//  HLPlaceView.h
//  hiyalife
//
//  Created by Oswaldo on 08/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLPlace.h"

@interface HLPlaceView : UIView
@property(nonatomic,strong) HLPlace *place;
-(id)initWithPlace:(HLPlace*)place  ;
@end
