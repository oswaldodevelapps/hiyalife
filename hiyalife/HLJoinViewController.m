//
//  HLJoinViewController.m
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLJoinViewController.h"
#import "HLDataStore.h"
#import "HLFacebookHelper.h"
 
#import "SVProgressHUD.h"
#import "HLJoinCompletedViewController.h"
#import "HLJoinCompleteDataNormalAccountViewController.h"
#import "ORPasswordIndicator.h"

@interface HLJoinViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *password1TextField;
@property (weak, nonatomic) IBOutlet UITextField *password2TextField;
 

@property (weak, nonatomic) IBOutlet UIButton *joinWichFacebookButton;

@property(strong,nonatomic) HLMeUser *newUser;
@property (weak, nonatomic) IBOutlet UIView *passwordIndicatorView;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;

 

@end

@implementation HLJoinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

 
-(HLMeUser *)newUser
{
    if(!_newUser){
        _newUser = [[HLMeUser alloc]init];
    }
    return _newUser;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.titleView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(handleSingleTap:)];
    
    [self.view addGestureRecognizer:tap];
    
    self.modelObject = self.newUser;
    
    
    [self connectTextField:self.emailTextField forKey:@"username"];
    [self connectTextField:self.password1TextField forKey:@"password1"];
    [self connectTextField:self.password1TextField forKey:@"password2"];
 
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (CGPoint)computeBottomPointForView:(UIView *)view
{
    if(view == self.password1TextField || view == self.password2TextField){
        CGPoint bottomPoint = self.joinButton.frame.origin;
        bottomPoint.y += self.joinButton.bounds.size.height;
        
        return bottomPoint;
    }
    else return [super computeBottomPointForView:view];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
     [textField resignFirstResponder];
    return YES;
}

 
- (IBAction)passwordTextChanged:(UITextField *)sender {
    
    [ORPasswordIndicator updatePasswordIndicatorView:self.passwordIndicatorView andPassword:sender.text];
   
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"JoinOkSegue"]) {
        HLJoinCompletedViewController *join2VC   = [segue destinationViewController];
        join2VC.currentUser = sender;
    }
    /*
    else if([[segue identifier] isEqualToString:@"ConfirmSegue"]){
        HLJoinCompleteDataNormalAccountViewController *join3VC = [segue destinationViewController];
        join3VC.currentUser = sender;
    }*/
}

- (void)textFieldConnector:(UITextField *)textField isValid:(BOOL)valid withError:(NSError *)error
{
    NSLog(@"Textfield %@ is valid %d",textField,valid);
    
   
    
    
    if(!valid){
     
        //[SVProgressHUD showErrorWithStatus:error.localizedDescription];
        
      
    }
    
}

#pragma mark - Actions

- (IBAction)registerAction:(id)sender {
    
    BOOL allFieldsAreOk = [self validateTextFields];
    NSLog(@"Form Validation Result %d",allFieldsAreOk);
 
    
        
    BOOL joinOk = YES;
 
    if([self.password1TextField.text length] == 0){
        [self.password1TextField setPlaceholder:@"Set a password"];
        joinOk = NO;
    }
    
    if([self.password2TextField.text length] == 0){
        [self.password2TextField setPlaceholder:@"Set a password"];
        joinOk = NO;
    }
    
    
    

    if(joinOk){
        if(![self.password1TextField.text isEqualToString:self.password2TextField.text]){
           joinOk = NO;
        
         [SVProgressHUD showErrorWithStatus:@"Passwords are not equal"];
        }
        else{
            if([self.password1TextField.text length] <4){
              [SVProgressHUD showErrorWithStatus:@"Passwords too short"];
                joinOk = NO;
            }
        }
        
        
    }
    
    
    
    if(joinOk){
        
   
        
             [SVProgressHUD showWithStatus:@"Joining..." maskType:SVProgressHUDMaskTypeGradient];
        
            [[HLDataStore sharedInstance] joinUserWith:self.emailTextField.text andPassword:self.password1TextField.text returnBlock:^(HLMeUser *user,NSError *error) {
                if(user){
                     [SVProgressHUD showSuccessWithStatus:@"Join is Ok"];
                    NSLog(@"User returned %@",user);
                    user.email = self.emailTextField.text;
                    [self performSegueWithIdentifier:@"JoinOkSegue" sender:user];
                    
                }else{
                    
          
                    [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Error: %@",[error localizedDescription]] ];
             
                }
               
                
            }];
    }
 
}

/*
 Test Facebook Accounts
 
 wilikjx_qinsky_1373622652@tfbnw.net
 
 bhxshoj_carrieroskybergmansonsteinwitzescusen_1370344038@tfbnw.net
 
 lcvhzzm_shepardson_1373622372@tfbnw.net
 
 ogadslv_thurnsky_1373622450@tfbnw.net
 
 
 Pasword: password
 
 */


- (IBAction)registerWithFacebookAction:(UIButton *)sender {
 
    
    
    
    
    self.joinWichFacebookButton.enabled = NO;
 
    
    [[HLFacebookHelper sharedInstance] getFacebookToken:^(NSString *token) {
      
        
        if(token){
            
            [SVProgressHUD showWithStatus:@"Joining..."  maskType:SVProgressHUDMaskTypeGradient];
            
            [[HLDataStore sharedInstance] joinUserWithFacebookToken:token returnBlock:^(HLMeUser *user, NSError *error) {
                self.joinWichFacebookButton.enabled = YES;
                NSLog(@"API Return user %@",user);
                
                if(user){
                    //update user info with Profile
                    [[HLDataStore sharedInstance] updateMeProfileDataForUser:user returnBlock:^(HLMeUser *user, NSError *error) {
                        [SVProgressHUD showSuccessWithStatus:@"Just complete your profile"];
                        [self performSegueWithIdentifier:@"completeProfileFacebookSegue" sender:self];
                        
                    }];
            
                }
                else{
                     [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Error: %@",[error localizedDescription]] ];
                }
                
            }];
            
        }else{
            self.joinWichFacebookButton.enabled = YES;
            [SVProgressHUD showErrorWithStatus:@"Error: Facebook Account not configured or not available." ];

        }
        
 
    }];
}


-(void)dealloc
{
    NSLog(@"Deallocating %@",NSStringFromClass([self class]));
}
@end
