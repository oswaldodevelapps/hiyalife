//
//  ORPasswordIndicator.m
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "ORPasswordIndicator.h"
#import "Complexify-ObjC.h"
#import "ORPasswordIndicatorView.h"


@implementation ORPasswordIndicator
+(void)updatePasswordIndicatorView:(UIView *)view andPassword:(NSString *)password{
    
    NSAssert([view class] == [ORPasswordIndicatorView class], @"ORPasswordIndicator: View Must be subclass of ORPasswordIndicatorView");
    
    [Complexify checkComplexityInBackgroundOfPassword:password completionHandler:^(BOOL valid, double complexity) {
       
        NSLog(@"Password Complexity %f",complexity);
        
        NSArray *segmentsViewArray = [view subviews];
        NSArray *sortedIndicators = [segmentsViewArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            if (((UIImageView *)obj1).tag < ((UIImageView *)obj2).tag)
                return (NSComparisonResult)NSOrderedAscending;
            else
                return (NSComparisonResult)NSOrderedDescending;
            
        }];

      
        
        for(int i=0;i<SEGMENT_CNT;i++){
            UIImageView * segmentView =[sortedIndicators objectAtIndex:i];
            segmentView.image = [UIImage imageNamed:SEGMENT_IMAGE_OFF];
            
        }
        int segmentValue = 100/SEGMENT_CNT;
        
        if([sortedIndicators count] == SEGMENT_CNT){
            
            int currentSegment = 0;
            
            if(complexity== 100)
                currentSegment = SEGMENT_CNT -1;
            
            else if(complexity == 0)
                currentSegment = -1;
            else
                currentSegment = complexity / segmentValue;
            
            if(currentSegment>-1){
                for(int i=0;i<=currentSegment;i++){
                    
                    UIImageView * segmentView =[sortedIndicators objectAtIndex:i];
                    segmentView.image = [UIImage imageNamed:SEGMENT_IMAGE_ON];
                    
                }
            }
            
            
            
        }
 
    }];
}
@end
