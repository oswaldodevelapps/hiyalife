//
//  HLTestGeolocalizerViewController.m
//  hiyalife
//
//  Created by Oswaldo on 05/07/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLTestGeolocalizerViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface HLTestGeolocalizerViewController ()<UITextFieldDelegate, CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager; // location manager for current location
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (nonatomic,strong) MKLocalSearch *localSearch;
@property (nonatomic,strong) MKLocalSearchResponse *results;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *coordinatesLabel;
@property (nonatomic,strong) MKMapView *ibMapView;

@property (nonatomic) CLLocationCoordinate2D selectedCoordinate;

@end

@implementation HLTestGeolocalizerViewController

-(MKLocalSearch *)localSearch{
    if(!_localSearch) _localSearch = [[MKLocalSearch alloc] init];
    return _localSearch;
}
-(MKMapView *)ibMapView{
    if(!_ibMapView){
        _ibMapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
        
        // Zoom the map to current location.
        [_ibMapView setShowsUserLocation:YES];
        [_ibMapView setUserInteractionEnabled:YES];
        [_ibMapView setUserTrackingMode:MKUserTrackingModeFollow];
    }
    
    return _ibMapView;
}
-(CLLocationManager *)locationManager
{
    if(!_locationManager) {
      _locationManager = [[CLLocationManager alloc]init];
        _locationManager.delegate = self;
    }
    return _locationManager;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.selectedCoordinate = kCLLocationCoordinate2DInvalid;
    [self.locationManager startUpdatingLocation];
    
    self.addressTextField.inputAccessoryView = self.ibMapView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)showMyLocation:(id)sender {
 
    // Cancel any previous searches.
    [self.localSearch cancel];
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
 
    request.naturalLanguageQuery = self.addressTextField.text;
    
    if(CLLocationCoordinate2DIsValid(self.selectedCoordinate)){
 
        request.region = MKCoordinateRegionMake(self.selectedCoordinate, MKCoordinateSpanMake(1.0, 1.0));
    }
 
    
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    
    [self.localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
           [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        
       
        for(MKMapItem *item in response.mapItems){
            NSLog(@"Item %@ %@",item.name,item.description);
            
           
             
            [self.ibMapView addAnnotation:item.placemark];
            [self.ibMapView selectAnnotation:item.placemark animated:YES];
            
            [self.ibMapView setCenterCoordinate:item.placemark.location.coordinate animated:YES];
           
            
        }
        //[self.ibMapView setUserTrackingMode:MKUserTrackingModeNone];
    }];
}
 
#pragma mark - CLLocationManagerDelegate


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // if the location is older than 30s ignore
    if (fabs([newLocation.timestamp timeIntervalSinceDate:[NSDate date]]) > 30)
    {
        return;
    }
    
    self.selectedCoordinate = [newLocation coordinate];
 
    NSLog(@"New updated Location %f x %f",  self.selectedCoordinate.longitude,self.selectedCoordinate.latitude);
    [self.locationManager stopUpdatingLocation];
}

@end
