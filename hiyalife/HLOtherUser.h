//
//  HLOtherUser.h
//  hiyalife
//
//  Created by Juan Hontanilla on 22/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import "HLUser.h"

@interface HLOtherUser : HLUser
@property (nonatomic,assign) BOOL isBranded;

@property(nonatomic) BOOL isAdded;
+(HLOtherUser *)makeUserWithDictionary:(NSDictionary *)dict;
@end
