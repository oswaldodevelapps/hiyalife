//
//  HLDataStore.h
//  hiyalife
//
//  Created by Juan Hontanilla on 07/05/13.
//  Copyright (c) 2013 Juan Hontanilla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HLMeUser.h"
#import "HLMeemo.h"
#import "AFHTTPClient.h"
#import "HLEditingMeemo.h"

typedef void(^loginReturnBlock)(HLMeUser *user, NSError *error);

typedef void (^feedReturnBlock)(NSArray *meemos,NSError *error);

typedef void (^refreshTokenBlock)(NSString *access_token,NSString *refresh_token);

typedef void(^meemoDetailsBlock)(HLMeemo *meemo);
typedef void(^meemoPublishBlock)(NSString *meemoID);

typedef void(^chapterReturnBlock)(NSArray *arrayOfChapters);

typedef void(^notificationsReturnBlock)(NSArray *arrayNotifications);

typedef void(^friendReturnBlock)(NSArray *friends);

typedef void(^successBlock)(BOOL success);

typedef void(^shareListBlock)(NSArray *arrayOfOtherUser);

typedef void(^placesBlock)(NSArray *placesArray);

@interface HLDataStore : NSObject

@property (nonatomic,strong) NSMutableArray *discoverFeedMeemos;
@property (nonatomic,strong) NSMutableArray *myLifeFeedMeemos;
@property (nonatomic,strong) NSMutableArray *anotherUserFeedMeemos;


@property (nonatomic,strong) HLMeUser *currentUser;
@property (nonatomic,strong) HLMeUser *validatingUser;

@property(nonatomic,strong) HLEditingMeemo *editingMeemo;

+(HLDataStore *)sharedInstance;

#pragma mark - API Auth
-(void)getLoginOkWithUser:(NSString *)user andPassword:(NSString *)pwd returnBlock:(loginReturnBlock) block;
-(void)validateJoin:(NSURL *)url returnBlock:(loginReturnBlock)block;
-(void)getLoginWithFacebookToken:(NSString *)token returnBlock:(loginReturnBlock)block;
-(void)joinUserWith:(NSString *)email andPassword:(NSString *)pwd returnBlock:(loginReturnBlock) block;
-(void)joinUserWithFacebookToken:(NSString *)token returnBlock:(loginReturnBlock)block;
-(void)getRefreshToken:(refreshTokenBlock) block;

#pragma mark - API Notifications
-(void)getNotificationsWithBlock:(notificationsReturnBlock) block;
-(void)setNotificationAsRead:(NSArray *)notificationsArray returnBlock:(successBlock)block;

#pragma mark - API Feed
-(void)addToTimeline:(HLMeemo *)memo returnBlock:(successBlock)block;
-(void)getFeedForUser:(HLUser *)user withBlock:(feedReturnBlock)block;

#pragma mark - API Meemo
-(void)publishMemo:(HLEditingMeemo *)newMeemo returnBlock:(meemoPublishBlock) block;
-(void)getMeemoDetails:(HLMeemo *)meemo forUser:(HLUser *)user withBlock:(meemoDetailsBlock)block;
-(void)getMeemoFromID:(NSString *)meemoID forUser:(HLUser *)user withBlock:(meemoDetailsBlock)block;
-(void)editMemo:(HLMeemo*)editedMeemo onlyForParameters:(NSDictionary *)changedParamsDict returnBlock:(successBlock)block;
-(void)editMemo:(HLMeemo *)editedMeemo returnBlock:(meemoDetailsBlock)block;


#pragma mark - Chapters
-(void)getChaptersforUser:(HLUser *)user returnBlock:(chapterReturnBlock) block;


#pragma mark - API ME
-(void)updateMeProfileDataForUser:(HLMeUser *)user returnBlock:(loginReturnBlock) block;
-(void)setMeProfileDataForUser:(HLMeUser *)user succesBlock:(successBlock) block;

#pragma mark - API User
-(void)findUserListByFacebookID:(NSString *)fb_id returnBlock:(friendReturnBlock)block;
-(void)findUserListByKey:(NSString *)keySearch returnBlock:(friendReturnBlock)block;

#pragma mark - API Share
-(void)getSharesListForMeemoID:(NSString *)memoID returnBlock:(shareListBlock) block;
-(void)shareMeemoID:(NSString*)memoID andText:(NSString*)text withArrayUsers: (NSArray*)users withArrayFB:(NSArray*)FBUsers withArrayOfEmails:(NSArray*)emails successBlock:(successBlock)block;


#pragma mark - API Places
-(void)getPlacesInBlock:(placesBlock) block;
-(void)createPlace:(HLPlace *)place successblock:(successBlock)block;

#pragma mark - Validation
+(BOOL) validateEmail: (NSString *) candidate;

#pragma mark - Utilities 
-(NSDate *)translateStringToDate:(NSString *)dateStr;
-(NSString *)translateDateToString:(NSDate *)date;

#pragma mark - Images
+(void)downloadImageWithURL:(NSString *)urlString success:(void (^)(UIImage *image))success;

#pragma mark - Archiving 
-(BOOL)saveChanges;
-(BOOL)saveDraftMeemo;
-(NSArray *)saveDraftPhotos:(NSArray *)imageArray;
-(BOOL)deleteDraftMemo;

#pragma mark - Session
-(void)logout;




@end
